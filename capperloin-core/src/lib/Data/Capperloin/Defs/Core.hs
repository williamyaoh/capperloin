{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}

module Data.Capperloin.Defs.Core
  ( Defs(..)
  , Label(..)
  , Namespace
  , Identifier
  , Literal(..)
  , DefForest(..)
  , DefTree(..)
  , DefM
  , defs
  , empty
  , node
  , leaf
  , defcapperloin
  , capperloinNS
  , capperloinNode
  )
where

import GHC.Stack ( HasCallStack, SrcLoc(srcLocPackage), callStack, popCallStack )
import Data.Sequence ( Seq )
import Data.Text ( Text )
import Control.Monad.Reader.Class ( MonadReader )
import Control.Monad.Trans.Reader ( Reader, runReader )
import Control.Monad.Trans.Writer.Strict ( WriterT, execWriterT )
import Control.Monad.Trans.Class ( lift )
import GHC.Exts ( toList )

import qualified Control.Monad.Writer.Strict as Writer
import qualified Control.Monad.Reader as Reader
import qualified Data.Sequence as Seq

data Label
  = Ident Namespace Identifier
  | Lit Literal
  deriving (Eq, Show)

type Namespace = Text

type Identifier = Text

data Literal
  = Unit
  | Boolean Bool
  | Number Double
  | String Text
  deriving (Eq, Show)

-- |
-- A bunch of named, rooted definition trees. Each is tagged with the
-- source location where it was defined.
newtype DefForest = DefForest
  { getDefForest :: Seq DefTree
  }
  deriving stock (Show)
  deriving newtype (Semigroup, Monoid)

data DefTree = DefTree
  { defTreeLabel    :: Label
  , defTreeChildren :: DefForest
  , defTreeLoc      :: SrcLoc
  }
  deriving (Show)

data DefsConfig = DefsConfig
  { defsPackage    :: String
  , defsCurrSrcLoc :: SrcLoc
  }

type DefM = WriterT DefForest (Reader DefsConfig) ()

data Defs = Defs
  { defRootChildren :: DefForest
  , defRootSrcLoc   :: SrcLoc
  }

empty :: DefM
empty = Writer.tell mempty

currSrcLoc :: HasCallStack => SrcLoc
currSrcLoc = case toList $ popCallStack callStack of
  ((_, srcLoc):_) -> srcLoc
  []              -> error "No current call stack. Did you forget a HasCallStack constraint somewhere?"

-- |
-- Ensure that the passed-in `SrcLoc' is inside the current package.
guardDefPackage :: MonadReader DefsConfig m => SrcLoc -> m (Maybe SrcLoc)
guardDefPackage loc = do
  currPkg <- Reader.asks defsPackage
  if srcLocPackage loc == currPkg
    then pure $! Just loc
    else pure $! Nothing

-- |
-- Toplevel function to convert the definitions into an actual tree with
-- source locations.
defs :: HasCallStack => DefM -> Defs
defs d =
  let
    here = currSrcLoc
    cfg = DefsConfig { defsPackage = srcLocPackage here, defsCurrSrcLoc = here }
  in (\defs -> Defs { defRootChildren = defs, defRootSrcLoc = here }) $
     flip runReader cfg $
     execWriterT $
     d

-- |
-- All exported functions that produce `DefM' must be wrapped in a call to
-- `defcapperloin'. This ensures that the source locations on errors will
-- all be linked to the correct place in the end-user's definitions.
defcapperloin :: HasCallStack => DefM -> DefM
defcapperloin body = do
  mhere <- guardDefPackage parentSrcLoc
  case mhere of
    Nothing -> body
    Just here -> Reader.local (\cfg -> cfg { defsCurrSrcLoc = here }) body
  where
    parentSrcLoc :: HasCallStack => SrcLoc
    parentSrcLoc = case toList $ popCallStack $ popCallStack callStack of
      ((_, srcLoc):_) -> srcLoc
      []              -> error "No current call stack. Did you forget a HasCallStack constraint somewhere?"

-- |
-- Create a tree node.
node :: HasCallStack => Namespace -> Identifier -> DefM -> DefM
node ns ident body = defcapperloin $ do
  here <- Reader.asks defsCurrSrcLoc
  children <- lift $ execWriterT body
  Writer.tell $ DefForest $ Seq.singleton $ DefTree
    { defTreeLabel = Ident ns ident
    , defTreeChildren = children
    , defTreeLoc = here
    }

-- |
-- Create a tree leaf.
leaf :: HasCallStack => Literal -> DefM
leaf lit = defcapperloin $ do
  here <- Reader.asks defsCurrSrcLoc
  Writer.tell $ DefForest $ Seq.singleton $ DefTree
    { defTreeLabel = Lit lit
    , defTreeChildren = mempty
    , defTreeLoc = here
    }

-- |
-- Namespace for Capperloin-specific attributes. Mainly for top-level
-- datatype names. Should not be used by any extensions or producers.
capperloinNS :: Namespace
capperloinNS = "capperloin"

capperloinNode :: HasCallStack => Identifier -> DefM -> DefM
capperloinNode ident = defcapperloin . node capperloinNS ident

{-# LANGUAGE OverloadedStrings #-}

module Data.Capperloin.Defs.Haskell
  ( define
  , product
  , productAnonymousFields
  , sum
  , variants
  , variant
  , fields
  , named
  , TypeInfo(..)
  , maybe
  , field
  , field_
  , haskellNS
  , haskellContentNS
  , haskellNode
  , haskellContentNode
  )
where

import Prelude hiding ( sum, product, maybe )

import GHC.Stack ( HasCallStack )
import Data.Capperloin.Defs.Core ( Namespace, DefM, Identifier, capperloinNode, node, defcapperloin )
import Data.Foldable ( for_ )
import Data.String ( IsString(..) )

import qualified Data.Text as Text

data TypeInfo
  = FlatType Identifier
  | MaybeType Identifier

instance IsString TypeInfo where
  fromString = FlatType . Text.pack

haskellNS :: Namespace
haskellNS = "haskell"

-- |
-- We essentially have two namespaces: the normal Haskell NS, for "structural"
-- parts of the tree, and this one, for "content," like actual type names,
-- constructor names, etc.
haskellContentNS :: Namespace
haskellContentNS = "haskell-content"

haskellNode :: HasCallStack => Identifier -> DefM -> DefM
haskellNode ident = defcapperloin . node haskellNS ident

haskellContentNode :: HasCallStack => Identifier -> DefM -> DefM
haskellContentNode ident = defcapperloin . node haskellContentNS ident

define :: HasCallStack => Identifier -> [Identifier] -> DefM -> DefM
define name vars body = defcapperloin $ do
  capperloinNode name $ do
    haskellNode "typevars" $
      for_ vars $ \var -> haskellNode var (pure ())
    body

product :: HasCallStack => Identifier -> DefM -> DefM
product constructor body = defcapperloin $
  haskellNode "definition" $
    haskellNode "product" $
      haskellNode constructor $
        body

productAnonymousFields :: HasCallStack => Identifier -> DefM -> DefM
productAnonymousFields constructor body = defcapperloin $
  haskellNode "definition" $
    haskellNode "anonymous-product" $
      haskellNode constructor $
        body

sum :: HasCallStack => DefM -> DefM
sum body = defcapperloin $
  haskellNode "definition" $
    haskellNode "sum" $
      body

-- |
-- Ensure that our variant definitions are wrapped so that other producers
-- can insert nodes without interfering with our forAll
variants :: HasCallStack => DefM -> DefM
variants body = defcapperloin $
  haskellNode "variants" $
    body

variant :: HasCallStack => Identifier -> DefM -> DefM
variant constructor body = defcapperloin $
  haskellNode "variant" $
    haskellNode constructor $
      body

-- |
-- Ensure that we have a wrapper for field definitions so that
-- other producers can insert nodes without interfering with our forAll
fields :: HasCallStack => DefM -> DefM
fields body = defcapperloin $
  haskellNode "fields" $
    body

named :: HasCallStack => Identifier -> DefM -> DefM
named name body = defcapperloin $
  haskellNode "named" $
    haskellNode name $
      body

maybe :: Identifier -> TypeInfo
maybe = MaybeType

field :: HasCallStack => TypeInfo -> DefM -> DefM
field ty body = defcapperloin $
  haskellNode "field" $
    case ty of
      FlatType x ->
        haskellContentNode x $
          body
      MaybeType x ->
        haskellNode "maybe" $
          haskellContentNode x $
            body

field_ :: HasCallStack => TypeInfo -> DefM
field_ ty = defcapperloin $ field ty $ pure ()

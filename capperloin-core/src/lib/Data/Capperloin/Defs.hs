-- |
-- Description: Core types and functions for writing definitions
--
-- Trees of definitions that also track their source code location.
--
-- As a convention, functions with an underscore designate variants that
-- don't take any further definitions as a body.

module Data.Capperloin.Defs
  ( module Data.Capperloin.Defs.Core
  , module Data.Capperloin.Defs.Haskell
  -- * Reexports
  , module GHC.Stack
  )
where

import Data.Capperloin.Defs.Core
import Data.Capperloin.Defs.Haskell
import GHC.Stack ( HasCallStack, SrcLoc )

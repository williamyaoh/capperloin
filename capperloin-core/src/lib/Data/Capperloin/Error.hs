{-# LANGUAGE DeriveGeneric #-}

module Data.Capperloin.Error
  ( CapperloinError(..) )
where

import Control.Exception ( Exception )
import GHC.Stack ( SrcLoc )
import GHC.Generics ( Generic )
import Data.Text ( Text )

-- |
-- A common error type for things that can occur while processing a Capperloin
-- definition tree.
data CapperloinError = CapperloinError
  { errLoc :: SrcLoc
  , errMsg :: Text
  }
  deriving (Show, Generic)

instance Exception CapperloinError

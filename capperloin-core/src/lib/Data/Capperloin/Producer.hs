{-# LANGUAGE DataKinds            #-}
{-# LANGUAGE ExplicitNamespaces   #-}
{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE GADTs                #-}
{-# LANGUAGE LambdaCase           #-}
{-# LANGUAGE NamedFieldPuns       #-}
{-# LANGUAGE OverloadedStrings    #-}
{-# LANGUAGE QuasiQuotes          #-}
{-# LANGUAGE RankNTypes           #-}
{-# LANGUAGE ScopedTypeVariables  #-}
{-# LANGUAGE TypeOperators        #-}
{-# LANGUAGE TypeSynonymInstances #-}

-- |
-- Description: Main monads for defining autogen executables.

module Data.Capperloin.Producer
  ( Producer(..)
  , ProducerM, FileM, withFile, produce, mark, errorAt
  , runProducerM, dryRunProducerM
  , Capperloin
  , runCapperloin
  , produceTo
  , module Reexport
  )
where

import Control.Monad ( when )
import Control.Monad.Catch ( SomeException, Handler(..), throwM, catches, displayException )
import Control.Monad.Freer ( type (~>), type Eff, send, translate, reinterpret, run, runM )
import Control.Monad.IO.Class ( liftIO )
import Control.Monad.Trans.Resource ( MonadResource, ResourceT, allocate )
import Data.ByteString ( ByteString )
import Data.Capperloin.Defs ( Defs )
import Data.Capperloin.Error ( CapperloinError(..) )
import Data.Capperloin.Matcher ( DefsMatcher, Result(..), runDefsMatcher )
import Data.Capperloin.Metadata ( capperloinID, capperloinVersion )
import Data.Conduit ( ConduitT, (.|) )
import Data.Foldable ( traverse_ )
import Data.SemVer ( Version )
import Data.Sequence ( Seq )
import Data.String.Interpolate ( i )
import Data.Text ( Text )
import Data.Text.Encoding ( encodeUtf8 )
import GHC.Stack ( SrcLoc(..) )
import Path ( Path, (</>) )
import System.Directory ( getTemporaryDirectory )
import System.IO ( stderr, hClose, openBinaryTempFile )

import qualified Control.Monad.Freer.Writer as Writer
import qualified Crypto.Hash.MD5 as MD5
import qualified Data.ByteString as BS
import qualified Data.ByteString.Base16 as Hex
import qualified Data.Conduit as Conduit
import qualified Data.Conduit.Combinators as Conduit
import qualified Data.SemVer as SemVer
import qualified Data.Sequence as Seq
import qualified Data.Text.IO as Text
import qualified Data.UUID as UUID
import qualified Path as P
import qualified Path.IO as P

import Data.SemVer as Reexport
  ( version )
import Path as Reexport
  ( Path, Rel, Abs, Dir
  , (</>)
  , absdir, reldir, absfile, relfile
  )

data Producer where
  Producer ::
    { prodName :: Text
    , prodVersion :: Version
    , prodMatcher :: DefsMatcher a
    , prodCommentF :: (ByteString -> ByteString)
    , prodBody :: (a -> ProducerM ())
    } -> Producer

type Capperloin a = Eff '[ Writer.Writer (Seq (Defs -> IO ())) ] a

-- |
-- Produce an IO action suitable for use as a @main@ from a `Capperloin'
-- and a tree of definitions to run the Capperloin producers on.
runCapperloin :: Defs -> Capperloin a -> IO a
runCapperloin defs body =
  let (x, producers) = run $ Writer.runWriter body
  in traverse_ ($ defs) producers *> pure x
  -- Currently we don't accept any CLI options. We almost certainly
  -- should in the future, though.

-- |
-- Given a `Producer' and a directory into which the output of the Producer
-- will be placed, convert it so it's acceptable to use inside a `Capperloin'
-- program.
--
-- The expectation is that the @Producer@ has full control over the contents
-- of the directory; you should not expect anything that's currently in that
-- directory to /stay/ in that directory.
produceTo :: Producer -> Path rel Dir -> Capperloin ()
produceTo producer@(Producer { prodMatcher, prodBody }) rootDir =
  Writer.tell $ Seq.singleton $ \defs ->
    withHandledErrors producer (runDefsMatcher prodMatcher defs) $ \matched ->
      runProducerM rootDir producer (prodBody matched)
        `catches`
          [ Handler $ \(e :: CapperloinError) ->
              prettyPrintError producer e
          , Handler $ \(e :: SomeException) ->
              prettyPrintSomeException producer e
          ]

withHandledErrors :: Producer -> Result CapperloinError a -> (a -> IO ()) -> IO ()
withHandledErrors producer result body = case result of
  Fatal err -> prettyPrintError producer err
  Recoverable errs x -> traverse_ (prettyPrintError producer) errs *> body x
  Success x -> body x

prettyPrintError :: Producer -> CapperloinError -> IO ()
prettyPrintError (Producer { prodName, prodVersion }) err =
  let loc = errLoc err
  in Text.hPutStrLn
       stderr
       [i|[#{prodName}-#{SemVer.toString prodVersion}] #{srcLocFile loc}:#{srcLocStartLine loc}-#{srcLocEndLine loc}: #{errMsg err}|]

-- TOOD: At least for FileM, we should make it so that arbitrary exceptions
-- give the name of the file that was being written.
prettyPrintSomeException :: Producer -> SomeException -> IO ()
prettyPrintSomeException (Producer { prodName, prodVersion }) err =
  Text.hPutStrLn
    stderr
    [i|[#{prodName}-#{SemVer.toString prodVersion}] #{displayException err}|]

type ProducerM = Eff '[ ProducerM' ]

data ProducerM' a where
  WithFile :: Path P.Rel P.File -> FileM () -> ProducerM' ()
  ProducerErrorAt :: forall b. SrcLoc -> Text -> ProducerM' b

type FileM = Eff '[ FileM' ]

data FileM' a where
  Produce :: ByteString -> FileM' ()
  Mark :: Text -> FileM' ()
  FileErrorAt :: forall b. SrcLoc -> Text -> FileM' b

-- | Input is lines from the existing file, if any.
-- | Output is chunks of the output file.
type FileConduit = ConduitT ByteString ByteString (ResourceT IO)

-- | A small helper typeclass to ensure that we can send errors in both
-- | `ProducerM' and `FileM'.
class ErrorAt m where
  errorAt :: forall b. SrcLoc -> Text -> m b

instance ErrorAt ProducerM where
  errorAt loc msg =
    send $ ProducerErrorAt loc msg

instance ErrorAt FileM where
  errorAt loc msg =
    send $ FileErrorAt loc msg

withFile :: Path P.Rel P.File -> FileM () -> ProducerM ()
withFile fname body =
  send $ WithFile fname body

produce :: ByteString -> FileM ()
produce =
  send . Produce

mark :: Text -> FileM ()
mark =
  send . Mark

runProducerM :: Path rel P.Dir -> Producer -> ProducerM a -> IO a
runProducerM root producer = runM . go
  where
    go :: ProducerM ~> Eff '[ IO ]
    go = translate $ \case
      WithFile fname body ->
        handleFile fname body
          `catches`
            [ Handler $ \(e :: CapperloinError) ->
                prettyPrintError producer e
            , Handler $ \(e :: SomeException) ->
                prettyPrintSomeException producer e
            ]
      ProducerErrorAt loc msg ->
        throwM $ CapperloinError { errLoc = loc, errMsg = msg }

    handleFile :: Path P.Rel P.File -> FileM () -> IO ()
    handleFile fname body = do
      path <- Conduit.runConduitRes
         $ maybeSourceFile (root </> fname)
        .| Conduit.splitOnUnboundedE (== 0x0A)
        .| outputFile producer body
        .| sinkDurableSystemTempFile "capperloin.tmp"
      -- The filename may have leading path components. Ensuring the root dir
      -- is not enough to ensure that the file's dir exists.
      P.ensureDir (P.parent $ root </> fname)
      P.copyFile path (root </> fname)

outputFile :: Producer -> FileM a -> FileConduit a
outputFile (Producer { prodName, prodVersion, prodCommentF = commentF }) fileM = do
  validateHeader
  runM $ go fileM
  where
    validateHeader :: FileConduit ()
    validateHeader = do
      let capperHead :: Text = [i|capperloin-#{SemVer.toString capperloinVersion}|]
      let prodHead :: Text = [i|#{prodName}-#{SemVer.toString prodVersion}|]
      -- TODO: We don't currently emit any warnings for attempting to use
      -- incompatible producer or Capperloin versions.
      -- TODO: We don't have a way to override the overwrite behavior for
      -- non-Capperloin files.
      mline <- Conduit.await
      case mline of
        Nothing ->
          pure ()
        Just line | BS.isInfixOf (UUID.toASCIIBytes capperloinID) line -> do
          pure ()
        Just _line ->
          error "this does not appear to be a Capperloin-generated file"
      Conduit.yield (commentF [i|#{capperHead} | #{prodHead} | #{capperloinID}|]) *> Conduit.yield "\n"
      Conduit.yield (commentF "THIS FILE AUTOGENERATED BY CAPPERLOIN, DO NOT MODIFY") *> Conduit.yield "\n"

    go :: FileM ~> Eff '[ FileConduit ]
    go = translate $ \case
      Produce str ->
        Conduit.yield str
      Mark name -> do
        let hash = markHash name
        findHash hash
        Conduit.yield "\n" *> Conduit.yield (commentF [i|BEGIN MANUAL CODE SECTION | #{name} | #{hash}|]) *> Conduit.yield "\n"
        outputManualLines hash
        Conduit.yield (commentF [i|END MANUAL CODE SECTION | #{name} | #{hash}|]) *> Conduit.yield "\n"
      FileErrorAt loc msg ->
        throwM $ CapperloinError { errLoc = loc, errMsg = msg }

    markHash :: Text -> ByteString
    markHash name =
      let updates = \ctx -> MD5.updates ctx [ encodeUtf8 name, UUID.toASCIIBytes capperloinID ]
      in Hex.encode $ MD5.finalize $ updates MD5.init

    findHash :: ByteString -> FileConduit ()
    findHash hash = do
      mline <- Conduit.await
      case mline of
        Nothing -> pure ()
        Just line | BS.isInfixOf hash line
                 && BS.isInfixOf "BEGIN MANUAL CODE SECTION" line -> pure ()
        Just _ -> findHash hash

    outputManualLines :: ByteString -> FileConduit ()
    outputManualLines hash = do
      mline <- Conduit.await
      case mline of
        Nothing ->
          pure ()
        Just line | BS.isInfixOf hash line
                 && BS.isInfixOf "END MANUAL CODE SECTION" line ->
          pure ()
        Just line ->
          Conduit.yield line *> Conduit.yield "\n" *> outputManualLines hash

-- | Returns the sequence of files that would be created.
dryRunProducerM :: ProducerM a -> (a, Seq (Path P.Rel P.File))
dryRunProducerM = run . Writer.runWriter . go
  where
    -- We try to ignore calls to `errorAt' by having calls to it return an unforced bottom.
    go :: ProducerM ~> Eff '[ Writer.Writer (Seq (Path P.Rel P.File)) ]
    go = reinterpret $ \case
      WithFile fname body -> do
        Writer.tell $ Seq.singleton fname
        goBody body
      ProducerErrorAt _ _ ->
        pure $ error "something went wrong; did you try to use the result of a call to errorAt?"

    -- For now we can safely ignore any filesystem operations, since the producer
    -- cannot inspect the file contents. In the future, we may want to change
    -- this pure runner to use an in-memory filesystem if we need complicated
    -- functionality.
    goBody :: FileM ~> Eff '[ Writer.Writer (Seq (Path P.Rel P.File)) ]
    goBody = reinterpret $ \case
      Produce _ -> pure ()
      Mark _ -> pure ()
      FileErrorAt _ _ -> pure $ error "something went wrong; did you try to use the result of a call to errorAt?"

--------------------------------------------------------------------------------
-- Conduit utils
--------------------------------------------------------------------------------

-- | Like `Conduit.sourceFile', but checks whether the file exists first.
-- | If it doesn't exist, the conduit is empty.
maybeSourceFile :: MonadResource m => Path rel P.File -> ConduitT i ByteString m ()
maybeSourceFile file = do
  exists <- P.doesFileExist file
  when exists $ Conduit.sourceFile $ P.toFilePath file

-- | Like `Conduit.sinkSystemTempFile'. The file created is "durable" in the sense
-- | that it won't be deleted when the `ResourceT' block exists, so you can
-- | do what you want with it.
sinkDurableSystemTempFile :: MonadResource m => String -> ConduitT ByteString o m (Path P.Abs P.File)
sinkDurableSystemTempFile pat = do
  dir <- liftIO getTemporaryDirectory
  (_releaseKey, (fname, hdl)) <- allocate
    (openBinaryTempFile dir pat)
    (\(_fname, hdl) -> hClose hdl)
  Conduit.sinkHandle hdl
  liftIO $ hClose hdl
  case P.parseAbsFile fname of
    Nothing -> error "impossible"
    Just path -> pure path

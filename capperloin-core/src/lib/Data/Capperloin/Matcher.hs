{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE NamedFieldPuns #-}

module Data.Capperloin.Matcher
  ( DefsMatcher
  , ChildMatcher
  , Result(..)
  , runDefsMatcher
  , exists
  , forAll
  , choice
  , try
  , choiceChild
  , tryChild
  , named
  , namedNS
  , exact
  , srcLoc
  , mapResult
  , onError
  , (<?>)
  )
where

import Control.Monad ( ap )
import Data.Capperloin.Defs ( Defs(..), DefForest(..), DefTree(..), Label(..), Namespace, Identifier, SrcLoc )
import Data.Capperloin.Error ( CapperloinError(..) )
import Data.Foldable ( foldr' )
import Data.Function ( (&) )
import Data.Maybe ( mapMaybe, catMaybes )
import Data.Sequence ( Seq )
import Data.String.Interpolate ( i )
import Data.Text ( Text )
import GHC.Exts ( toList )

import qualified Data.Sequence as Seq
import qualified Data.Text as Text

-- |
-- A three-state error-handling type. Errors can either be @Fatal@, in which
-- case they short-circuit like `Either', or they can be @Recoverable@, which
-- means that there's still a result and errors accumulate.
data Result err a
  = Fatal err
  | Recoverable (Seq err) a
  | Success a
  deriving (Show, Functor)

instance Applicative (Result err) where
  pure = Success
  (<*>) = ap

-- TODO: Make sure this instance actually obeys the monad laws...
instance Monad (Result err) where
  return = pure
  (>>=) (Fatal err) _ = Fatal err
  (>>=) (Recoverable errs x) f = case f x of
    Fatal err -> Fatal err
    Recoverable errs' y -> Recoverable (errs <> errs') y
    Success y -> Recoverable errs y
  (>>=) (Success x) f = f x

newtype FirstRecoverable err a = FirstRecoverable { unFirst :: Result err a }
  deriving newtype Functor

instance Semigroup (FirstRecoverable err a) where
  (<>) l@(FirstRecoverable ll) r = case ll of
    Success _       -> l
    Recoverable _ _ -> l
    _other          -> r

-- |
-- A matcher to match against an entire forest of trees. The `SrcLoc' points
-- to the parent of the forest, if there was one, or the root definition.
newtype DefsMatcher a = DefsMatcher
  { unDefsMatcher :: DefForest -> SrcLoc -> Result CapperloinError a }
  deriving Functor

instance Applicative DefsMatcher where
  pure = DefsMatcher . const . const . pure
  (<*>) = ap

-- TODO: Make sure this instance actually obeys the monad laws...
instance Monad DefsMatcher where
  return = pure
  (>>=) (DefsMatcher f) g = DefsMatcher $ \defs loc ->
    f defs loc >>= \x -> unDefsMatcher (g x) defs loc

instance MonadFail DefsMatcher where
  fail msg = DefsMatcher $ \_ loc ->
    Fatal $ CapperloinError { errMsg = Text.pack msg, errLoc = loc }

srcLoc :: DefsMatcher SrcLoc
srcLoc = DefsMatcher $ \_ loc -> Success loc

runDefsMatcher :: DefsMatcher a -> Defs -> Result CapperloinError a
runDefsMatcher (DefsMatcher f) (Defs { defRootChildren, defRootSrcLoc }) =
  f defRootChildren defRootSrcLoc

mapResult
  :: (Result CapperloinError a -> Result CapperloinError b)
  -> DefsMatcher a
  -> DefsMatcher b
mapResult f (DefsMatcher g) = DefsMatcher $ \defs loc ->
  f $ g defs loc

-- |
-- Add a custom error message if the matcher fails with a fatal error.
onError :: Text -> DefsMatcher a -> DefsMatcher a
onError msg = mapResult $ \result -> case result of
  Fatal err -> Fatal $ err { errMsg = msg }
  other     -> other

infixr 8 <?>

-- |
-- Infix alias for `onError'.
(<?>) :: DefsMatcher a -> Text -> DefsMatcher a
(<?>) = flip onError

-- |
-- Matcher for a single tree. The returned matcher, if given, will match
-- on the children of the tree, and be given the location of the tree.
type ChildMatcher a = Label -> Maybe (DefsMatcher a)

-- |
-- Creates a matcher that takes a child matcher and tries to find a single
-- child of the current tree that matches.
--
-- Fails with a fatal error if nothing matches.
exists :: ChildMatcher a -> DefsMatcher a
exists mBody = DefsMatcher $ \(DefForest defs) loc ->
  toList defs
    & mapMaybe (\def -> ((,,) (defTreeChildren def) (defTreeLoc def)) <$> mBody (defTreeLabel def))
    & fmap (\(children, childLoc, DefsMatcher matcher) -> FirstRecoverable $ matcher children childLoc)
    & foldr' (<>) (FirstRecoverable $ Fatal $ noChildMatchError loc)
    & unFirst
  where
    noChildMatchError :: SrcLoc -> CapperloinError
    noChildMatchError loc = CapperloinError
      { errMsg = "No children of this node matched an `exists'."
      , errLoc = loc
      }

-- |
-- Creates a matcher that takes a child matcher and runs it on all of the
-- children of the current tree.
--
-- Note that this doesn't fail if a child doesn't match; it instead recovers
-- fatal errors and excises that child from the results.
forAll :: ChildMatcher a -> DefsMatcher [a]
forAll mBody = DefsMatcher $ \(DefForest defs) _ ->
  toList defs
    & traverse
        (\def ->
           let
             label = defTreeLabel def
             loc = defTreeLoc def
           in case mBody label of
                Nothing -> Recoverable (Seq.singleton $ matchFailedError label loc) Nothing
                Just (recoverFatals label -> DefsMatcher matcher) -> matcher (defTreeChildren def) loc)
    & fmap catMaybes
  where
    prettyPrintLabel :: Label -> Text
    prettyPrintLabel (Ident ns ident) = [i|#{ns}:#{ident}|]
    prettyPrintLabel (Lit lit) = [i|#{lit}|]

    matchFailedError :: Label -> SrcLoc -> CapperloinError
    matchFailedError label loc = CapperloinError
      { errMsg = [i|Found a node that doesn't match a `forAll': <#{prettyPrintLabel label}>|]
      , errLoc = loc
      }

    recoverFatals :: Label -> DefsMatcher a -> DefsMatcher (Maybe a)
    recoverFatals label = mapResult $ \result -> case result of
      Fatal err -> Recoverable
        (Seq.singleton $ err
          { errMsg = [i|Found a node that doesn't match a `forAll': <#{prettyPrintLabel label}>. Original error: #{errMsg err}|]
          })
        Nothing
      other -> fmap Just other

-- |
-- Try the given matchers in order. Return the result of the first one that
-- returns something recoverable.
choice :: [DefsMatcher a] -> DefsMatcher a
choice matchers = DefsMatcher $ \defs loc ->
  matchers
    & fmap (\matcher -> unDefsMatcher matcher defs loc)
    & fmap FirstRecoverable
    & foldr' (<>) (FirstRecoverable $ Fatal $ allFailedError loc)
    & unFirst
  where
    allFailedError :: SrcLoc -> CapperloinError
    allFailedError loc = CapperloinError
      { errMsg = "No matchers in a `choice' succeeded."
      , errLoc = loc
      }

-- |
-- Try the given matcher, but don't fail if it doesn't match.
try :: DefsMatcher a -> DefsMatcher (Maybe a)
try body = choice
  [ Just <$> body
  , pure Nothing
  ]

choiceChild :: [ChildMatcher a] -> ChildMatcher a
choiceChild matchers = \label ->
  matchers
    & mapMaybe ($ label)
    & (\ms -> case ms of
        [] -> Nothing
        _  -> Just $ choice ms)

tryChild :: ChildMatcher a -> ChildMatcher (Maybe a)
tryChild body = choiceChild
  [ (fmap . fmap . fmap) Just body
  , (pure . pure . pure) Nothing
  ]

named :: ((Namespace, Identifier) -> DefsMatcher a) -> ChildMatcher a
named body label = case label of
  Ident ns ident -> Just $ body (ns, ident)
  _              -> Nothing

namedNS :: Namespace -> (Identifier -> DefsMatcher a) -> ChildMatcher a
namedNS ns body label = case label of
  Ident ns' ident | ns == ns' -> Just $ body ident
  _                           -> Nothing

exact :: Namespace -> Identifier -> DefsMatcher a -> ChildMatcher a
exact ns ident body label = case label of
  Ident ns' ident' | ns == ns' && ident == ident' -> Just body
  _                                               -> Nothing

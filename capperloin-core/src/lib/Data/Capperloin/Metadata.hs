module Data.Capperloin.Metadata
  ( capperloinID, capperloinVersion )
where

import Data.SemVer ( Version, version )
import Data.UUID ( UUID )

import qualified Data.UUID as UUID

-- | 132f80a8-ac8c-4a90-9e7c-6ae00a16138c, used as magic bytes to figure
-- | out whether a file is a Capperloin-generated file or not. Stable
-- | across all Capperloin versions.
capperloinID :: UUID
capperloinID =
  UUID.fromWords 0x132F80A8 0xAC8C4A90 0x9E7C6AE0 0x0A16138C

capperloinVersion :: Version
capperloinVersion = version 0 1 0 [] []

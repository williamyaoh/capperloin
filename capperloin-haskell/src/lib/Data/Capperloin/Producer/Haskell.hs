{-# LANGUAGE OverloadedLabels  #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

module Data.Capperloin.Producer.Haskell where

import Control.Monad ( when )
import Control.Monad.Trans.Class ( lift )
import Control.Monad.Trans.Reader ( ReaderT, runReaderT )
import Data.ByteString ( ByteString )
import Data.Capperloin.Defs ( Identifier, SrcLoc, TypeInfo(..), capperloinNS, haskellNS, haskellContentNS )
import Data.Capperloin.Matcher
import Data.Capperloin.Producer ( Producer(..), ProducerM, FileM, relfile, version, withFile, produce, mark, errorAt )
import Data.Foldable ( for_ )
import Data.String.Interpolate ( i, __i'L )
import Data.Text ( Text )
import Path ( parseRelFile )

import qualified Control.Monad.Trans.Reader as Reader
import qualified Data.Char as Char
import qualified Data.Text as Text

data Config = Config
  { -- |
    -- Whether to output each type into a separate file. If disabled, a single
    -- file called @Types.hs@ containing all types will be produced.
    cfgOutputIndividualFiles :: Bool
  , -- |
    -- Whether to prefix named record fields with the name of the type.
    -- The intention when not using this is to enable @-XDuplicateRecordFields@
    -- and access fields using generic lenses, to make data access more succinct.
    -- Regardless of whether you're using this, don't prefix the field names in
    -- your Capperloin definitions.
    cfgOutputPrefixedFields :: Bool
  , -- |
    -- The prefix of your modules, like @Data.MyTypes@. Don't add a trailing
    -- period.
    cfgModulePrefix :: Maybe Text
  }
  deriving Show

defaultConfig :: Config
defaultConfig = Config
  { cfgOutputIndividualFiles = True
  , cfgOutputPrefixedFields = True
  , cfgModulePrefix = Nothing
  }

producer :: Config -> Producer
producer cfg = Producer
  { prodName = "capperloin-haskell"
  , prodVersion = version 0 1 0 [] []
  , prodMatcher = matchDataDefs
  , prodCommentF = ("-- " <>)
  , prodBody = flip runReaderT cfg . produceDataDefs
  }

data SumVariant = SumVariant
  { variantName :: Identifier
  , variantFields :: [TypeInfo]
  }

data SumDef = SumDef
  { sumName :: Identifier
  , sumTypevars :: [Identifier]
  , sumVariants :: [SumVariant]
  }

data AnonymousProductDef = AnonymousProductDef
  { anonProdName :: Identifier
  , anonProdTypevars :: [Identifier]
  , anonProdConstructor :: Identifier
  , anonProdFields :: [TypeInfo]
  }

data NamedField = NamedField
  { fieldName :: Identifier
  , fieldType :: TypeInfo
  , fieldDefLoc :: SrcLoc
  }

data NamedProductDef = NamedProductDef
  { namedProdName :: Identifier
  , namedProdTypevars :: [Identifier]
  , namedProdConstructor :: Identifier
  , namedProdFields :: [NamedField]
  }

data DataDef' = Sum SumDef | AnonymousProduct AnonymousProductDef | NamedProduct NamedProductDef

data DataDef = DataDef
  { defName :: Identifier
  , defBody :: DataDef'
  }

--------------------------------------------------------------------------------
-- matching the tree structure
--------------------------------------------------------------------------------

hs :: Identifier -> DefsMatcher a -> ChildMatcher a
hs = exact haskellNS

hsc :: Identifier -> DefsMatcher a -> ChildMatcher a
hsc = exact haskellContentNS

nm :: (Identifier -> DefsMatcher a) -> ChildMatcher a
nm = namedNS haskellNS

nmc :: (Identifier -> DefsMatcher a) -> ChildMatcher a
nmc = namedNS haskellContentNS

-- |
-- Match various types of structural types. Right now:
--
-- * Normal (non-nullable) types
-- * Maybe-wrapped types
type_ :: ChildMatcher TypeInfo
type_ = choiceChild
  [ hs "field" $ exists $ nmc $ \typename -> pure (FlatType typename)
  , hs "field" $ exists $ hs "maybe" $ exists $ nmc $ \typename -> pure (MaybeType typename)
  ]

matchSum :: (Identifier, [Identifier]) -> DefsMatcher SumDef
matchSum (name, typevars) = do
  variants <- exists $ hs "variants" $ do
    forAll $ hs "variant" $
      exists $ nm $ \varname ->
        exists $ hs "fields" $ do
          fields <- forAll type_
          pure $! SumVariant varname fields
  pure $! SumDef name typevars variants

matchAnonProd :: (Identifier, [Identifier]) -> DefsMatcher AnonymousProductDef
matchAnonProd (name, typevars) =
  exists $ nm $ \constructor ->
    exists $ hs "fields" $ do
      fieldTypes <- forAll type_
      pure $! AnonymousProductDef name typevars constructor fieldTypes

matchNamedProd :: (Identifier, [Identifier]) -> DefsMatcher NamedProductDef
matchNamedProd (name, typevars) =
  exists $ nm $ \constructor ->
    exists $ hs "fields" $ do
      fields <- forAll $ hs "named" $
        exists $ nm $ \fieldName -> do
          field <- exists type_
          loc <- srcLoc
          pure $! NamedField fieldName field loc
      pure $! NamedProductDef name typevars constructor fields

matchDataDef :: Identifier -> DefsMatcher DataDef
matchDataDef name = do
  typevars <- exists $ hs "typevars" $
    forAll $ nm $ \typevar -> pure typevar
  exists $ hs "definition" $ do
    body <- exists $ nm $ \deftype -> case deftype of
      "sum"               -> Sum <$> matchSum (name, typevars)
      "anonymous-product" -> AnonymousProduct <$> matchAnonProd (name, typevars)
      "product"           -> NamedProduct <$> matchNamedProd (name, typevars)
      _                   -> fail [i|unknown definition type: `#{deftype}'|]
    pure $! DataDef name body

matchDataDefs :: DefsMatcher [DataDef]
matchDataDefs = forAll $ namedNS capperloinNS $ \typename ->
  matchDataDef typename

--------------------------------------------------------------------------------
-- producing the file output
--------------------------------------------------------------------------------

-- |
-- Output the boring module header.
producePreamble :: Text -> ReaderT Config FileM ()
producePreamble fname = do
  whenAsks cfgOutputIndividualFiles $ p
    [__i'L|
      {-\# LANGUAGE DeriveGeneric         \#-}
      {-\# LANGUAGE DuplicateRecordFields \#-}
    |]
  lift $ mark "module pragmas"

  moduleName <- maybe fname (\prefix -> [i|#{prefix}.#{fname}|])
    <$> Reader.asks cfgModulePrefix
  p [__i'L|
      module #{moduleName} where
    |]

  whenAsks cfgOutputIndividualFiles $ p
    [__i'L|
      import GHC.Generics ( Generic )
    |]
  lift $ mark "imports"

produceSum :: SumDef -> ReaderT Config FileM ()
produceSum (SumDef name typevars variants) = do
  p [i|data #{name} #{Text.intercalate " " typevars}|]
  case variants of
    [] -> pure ()
    (SumVariant varname varfields:rest) -> do
      p [i|  = #{varname} #{Text.intercalate " " $ fmap outputType varfields}|]
      for_ rest $ \(SumVariant varname varfields) ->
        p [i|  | #{varname} #{Text.intercalate " " $ fmap outputType varfields}|]
  whenAsks cfgOutputIndividualFiles $
    p [i|  deriving Generic|]

produceAnonProd :: AnonymousProductDef -> ReaderT Config FileM ()
produceAnonProd (AnonymousProductDef name typevars constructor fields) = do
  p [i|data #{name} #{Text.intercalate " " typevars} =|]
  p [i|  #{constructor} #{Text.intercalate " " $ fmap outputType fields}|]
  whenAsks cfgOutputIndividualFiles $
    p [i|  deriving Generic|]

produceNamedProd :: NamedProductDef -> ReaderT Config FileM ()
produceNamedProd (NamedProductDef name typevars constructor fields) = do
  p [i|data #{name} #{Text.intercalate " " typevars} =|]
  p [i|  #{constructor}|]
  case fields of
    [] -> pure ()
    (field:fs) -> do
      fullname <- maybePrefixField name field
      p [i|    { #{fullname} :: #{outputType $ fieldType field}|]
      for_ fs $ \field -> do
        fullname <- maybePrefixField name field
        p [i|    , #{fullname} :: #{outputType $ fieldType field}|]
      p [i|    }|]
  whenAsks cfgOutputIndividualFiles $
    p [i|  deriving Generic|]
  where
    maybePrefixField :: Identifier -> NamedField -> ReaderT Config FileM Identifier
    maybePrefixField typename field = do
      shouldPrefix <- Reader.asks cfgOutputPrefixedFields
      if not shouldPrefix
        then pure $ fieldName field
        else
          let mprefixed = do
                (tyHead, tyRest) <- Text.uncons typename
                (fieldHead, fieldRest) <- Text.uncons $ fieldName field
                pure [i|#{Char.toLower tyHead}#{tyRest}#{Char.toUpper fieldHead}#{fieldRest}|]
          in case mprefixed of
               Just prefixed ->
                 pure prefixed
               Nothing ->
                 lift $ errorAt (fieldDefLoc field) "Bad field definition. Is the field name empty?"

produceDataDef :: DataDef' -> ReaderT Config FileM ()
produceDataDef def = do
  case def of
    Sum sum -> produceSum sum
    AnonymousProduct aprod -> produceAnonProd aprod
    NamedProduct nprod -> produceNamedProd nprod

produceDataDefs :: [DataDef] -> ReaderT Config ProducerM ()
produceDataDefs defs = do
  cfg <- Reader.ask
  if cfgOutputIndividualFiles cfg
    then for_ defs $ \(DataDef name body) ->
      lift $ case parseRelFile [i|#{name}.hs|] of
        Nothing -> error [i|invalid file name: #{name}|]
        Just filename -> withFile filename $ do
          runReaderT (producePreamble name) cfg
          runReaderT (produceDataDef body) cfg
          mark "manual section"
    else lift $ withFile [relfile|Types.hs|] $ do
      runReaderT (producePreamble "Types") cfg
      for_ defs $ \(DataDef _ body) -> do
        runReaderT (produceDataDef body) cfg
        mark "manual section"

outputType :: TypeInfo -> Text
outputType (FlatType t) = t
outputType (MaybeType t) = [i|(Maybe #{t})|]

p :: ByteString -> ReaderT Config FileM ()
p str = do
  lift (produce str)
  lift (produce "\n")

whenAsks :: Monad m => (cfg -> Bool) -> ReaderT cfg m () -> ReaderT cfg m ()
whenAsks pred body = do
  flag <- Reader.asks pred
  when flag body

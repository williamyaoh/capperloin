{-# LANGUAGE OverloadedStrings #-}

module Main where

import Hedgehog ( Property, property, forAll, (===) )
import Test.Tasty ( TestTree )
import Test.Tasty.Hedgehog ( testPropertyNamed )

import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range
import qualified Test.Tasty as Tasty
import qualified Test.Tasty.HUnit as HUnit

main :: IO ()
main = Tasty.defaultMain tests

tests :: TestTree
tests = Tasty.testGroup "Tests"
  [ HUnit.testCase "test passes" $
      True HUnit.@?= True
  , testPropertyNamed
      "property passes"
      "prop_test"
      prop_test
  ]

prop_test :: Property
prop_test = property $ do
  a <- forAll $ Gen.int (Range.exponential 0 100)
  b <- forAll $ Gen.int (Range.exponential 0 100)
  a + b === b + a

{-# LANGUAGE OverloadedStrings #-}

-- |
-- Description: Functions for defining Purescript information.

module Data.Capperloin.Defs.WebTyped.Purescript
  ( pstype
  , pstype_
  , purescriptNS
  , purescriptContentNS
  )
where

import Data.Capperloin.Defs ( Namespace, DefM, TypeInfo(..), node, defcapperloin )
import GHC.Stack ( HasCallStack )

purescriptNS :: Namespace
purescriptNS = "purescript"

purescriptContentNS :: Namespace
purescriptContentNS = "purescript-content"

pstype :: HasCallStack => TypeInfo -> DefM -> DefM
pstype ty body = defcapperloin $
  node purescriptNS "type" $
    case ty of
      FlatType x ->
        node purescriptContentNS x $
          body
      MaybeType x ->
        node purescriptNS "maybe" $
          node purescriptContentNS x $
            body

pstype_ :: HasCallStack => TypeInfo -> DefM
pstype_ ty = defcapperloin $
  pstype ty $ pure ()

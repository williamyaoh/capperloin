{-# LANGUAGE OverloadedLists     #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE QuasiQuotes         #-}
{-# LANGUAGE ScopedTypeVariables #-}

-- | Outputs codecs for web-typed data using @purescript-codec-argonaut@.
-- | The format is meant to match the format of the derived Aeson instances
-- | in @capperloin-webtyped-haskell@. This is meant to be used alongside the
-- | producer for web-typed Purescript types.
-- |
-- | Just like the producer for Purescript types, we rely on a hack of outputting
-- | our own Prelude into @Extra/Prelude.purs@ to prevent warnings from unused imports.
-- |
-- | Compiling the output requires the following libraries:
-- |
-- | * @purescript-argonaut-core@
-- | * @purescript-codec-argonaut@
-- | * @purescript-datetime@
-- | * @purescript-foreign-object@
-- | * @purescript-formatters@
-- | * @purescript-parsing@
-- | * @purescript-uuid@

module Data.Capperloin.Producer.WebTyped.CodecArgonaut
  ( Config(..)
  , defaultConfig
  , producer
  )
where

import Control.Applicative ( liftA2 )
import Control.Monad ( unless )
import Control.Monad.Trans.Class ( lift )
import Control.Monad.Trans.Reader ( ReaderT, runReaderT )
import Data.ByteString ( ByteString )
import Data.Capperloin.Defs ( Identifier, SrcLoc, TypeInfo(..) )
import Data.Capperloin.Matcher.WebTyped.Purescript
import Data.Capperloin.Producer ( Producer(..), ProducerM, FileM, version, relfile, withFile, produce, mark, errorAt )
import Data.Foldable ( foldl', for_ )
import Data.Sequence ( Seq((:<|)), (><) )
import Data.String.Interpolate ( i, __i, __i'L )
import Data.Text ( Text )
import Path ( parseRelFile )

import qualified Control.Monad.Trans.Reader as Reader
import qualified Data.Sequence as Seq
import qualified Data.Text as Text
import qualified Text.Casing as Casing

data Config = Config
  { -- |
    -- The prefix of your modules, like @Data.MyTypes@. Don't add a trailing
    -- period.
    cfgModulePrefix :: Maybe Text
  , -- |
    -- The prefix of your types module.
    cfgTypesModulePrefix :: Maybe Text
  , -- |
    -- Any extra stuff to stick in the outputted module imports.
    cfgExtraImports :: Maybe Text
  }
  deriving Show

defaultConfig :: Config
defaultConfig = Config
  { cfgModulePrefix = Nothing
  , cfgTypesModulePrefix = Nothing
  , cfgExtraImports = Nothing
  }

producer :: Config -> Producer
producer cfg =
  Producer
    { prodName = "capperloin-webtyped-purescript"
    , prodVersion = version 0 1 0 [] []
    , prodMatcher = matchDataDefs
    , prodCommentF = ("-- " <>)
    , prodBody = flip runReaderT cfg . produceDataDefs
    }

-- | Output the boring module header.
producePreamble :: Text -> DataDef -> ReaderT Config FileM ()
producePreamble fname def = do
  moduleName <- maybe fname (\prefix -> [i|#{prefix}.#{fname}|])
    <$> Reader.asks cfgModulePrefix
  preludeModuleName :: Text <- maybe id (\prefix name -> [i|#{prefix}.#{name}|])
    <$> Reader.asks cfgModulePrefix
    <*> pure "Extra.Prelude"
  p [__i'L|
      module #{moduleName} where
    |]

  typesModuleName <- maybe id (\prefix name -> [i|#{prefix}.#{name}|])
    <$> Reader.asks cfgTypesModulePrefix
    <*> pure (defName def)
  let typeImports :: Text = case defBody def of
        Product _ -> [i|#{defName def}, #{defName def}CU|]
        Sum sum -> intercalate ", " $
              (defName def)
          :<| [i|#{defName def}CU(..)|]
          :<| flip foldMap (sumVariants sum) (\variant ->
                if Seq.null (variantFields variant)
                  then Seq.empty
                  else Seq.singleton [i|#{variantName variant}Fields|])

  p [__i|
      import #{preludeModuleName}
      import #{typesModuleName} ( #{typeImports} )
    |]

  mextraImports <- Reader.asks cfgExtraImports
  case mextraImports of
    Nothing -> pure ()
    Just extraImports -> p [i|#{extraImports}|]
  lift $ mark "imports"

-- TODO: Having to include this is dumb. We should really replace at least
-- the parser with the parser from @purescript-datetime-parsing@, but that
-- requires us to upgrade to purs 0.15, and requires us to rewrite our code
-- against the latest version of @purescript-formatting@.
produceParser :: ReaderT Config FileM ()
produceParser = do
  moduleName :: Text <- maybe id (\prefix name -> [i|#{prefix}.#{name}|])
    <$> Reader.asks cfgModulePrefix
    <*> pure "Extra.ISO8601"
  p [__i|
      module #{moduleName}
        ( formatter, datetime )
      where

      import Prelude

      import Data.Foldable ( foldl )
      import Data.List as List
      import Data.List ( List(..) )
      import Data.List.Lazy ( replicateM )
      import Data.Maybe ( Maybe(..) )
      import Data.String.CodePoints ( codePointFromChar )
      import Data.CodePoint.Unicode ( isDecDigit )
      import Data.Enum ( class BoundedEnum, toEnum )
      import Data.Formatter.DateTime ( Formatter, FormatterCommand(..) )
      import Data.DateTime
        ( DateTime(..), Date, Time(..), Millisecond
        , canonicalDate
        )

      import Parsing as P
      import Parsing.Combinators ( choice )
      import Parsing ( ParserT )
      import Parsing.String as PS

      import Partial.Unsafe ( unsafePartial )

      -- |
      -- Format string for ISO8601 dates. Only use this format string when
      -- *outputting*; it's too strict to work correctly when parsing input
      -- strings.
      formatter :: Formatter
      formatter = List.fromFoldable
        [ YearFull
        , Placeholder "-"
        , MonthTwoDigits
        , Placeholder "-"
        , DayOfMonthTwoDigits
        , Placeholder "T"
        , Hours24
        , Placeholder ":"
        , MinutesTwoDigits
        , Placeholder ":"
        , SecondsTwoDigits
        , Placeholder "."
        , Milliseconds
        , Placeholder "Z"
        ]

      digit :: forall m. Monad m => ParserT String m Int
      digit = PS.satisfy (isDecDigit <<< codePointFromChar) <\#> \\c -> unsafePartial case c of
        -- This usage of `unsafePartial' is safe; the predicate ensures
        -- we won't see characters other than these 10.
        '0' -> 0
        '1' -> 1
        '2' -> 2
        '3' -> 3
        '4' -> 4
        '5' -> 5
        '6' -> 6
        '7' -> 7
        '8' -> 8
        '9' -> 9

      -- |
      -- Parse a number of length N digits.
      numberN :: forall m. Monad m => Int -> ParserT String m Int
      numberN n =
        replicateM n digit <\#> foldl (\\x d -> x * 10 + d) 0

      date :: forall m. Monad m => ParserT String m Date
      date = canonicalDate
        <$> component "year" 4
        <* PS.char '-'
        <*> component "month" 2
        <* PS.char '-'
        <*> component "day" 2
        where
          component :: forall a. Monad m
                    => BoundedEnum a
                    => String
                    -> Int
                    -> ParserT String m a
          component name numDigits = do
            raw <- numberN numDigits
            case toEnum raw of
              Nothing -> P.fail (name <> " out of range")
              Just x -> pure x

      -- |
      -- Parsing the subsecond component from an ISO8601 timestamp is a bit
      -- complicated. We have to generate a Millisecond datum, but that means
      -- a subsecond value of ".99" has to be turned into 990 milliseconds,
      -- and similarly, we have to truncate excessively long subsecond values
      -- to make them fit.
      millisecond :: forall m. Monad m => ParserT String m Millisecond
      millisecond = do
        rawDigits <- List.some digit
        let digits = truncext rawDigits
        case toEnum (foldl (\\x d -> x * 10 + d) 0 digits) of
          Nothing -> P.fail "milliseconds out of range"
          Just ms -> pure ms
        where
          truncext :: List Int -> List Int
          truncext = case _ of
            Nil -> List.fromFoldable [0, 0, 0]
            Cons d1 Nil -> List.fromFoldable [d1, 0, 0]
            Cons d1 (Cons d2 Nil) -> List.fromFoldable [d1, d2, 0]
            Cons d1 (Cons d2 (Cons d3 _)) -> List.fromFoldable [d1, d2, d3]

      time :: forall m. Monad m => ParserT String m Time
      time = Time
        <$> component "hour" 2
        <* PS.char ':'
        <*> component "minute" 2
        <* PS.char ':'
        <*> component "seconds" 2
        <*> choice
              [ PS.char '.' *> millisecond
              , pure bottom
              ]
        where
          component :: forall a. Monad m
                    => BoundedEnum a
                    => String
                    -> Int
                    -> ParserT String m a
          component name numDigits = do
            raw <- numberN numDigits
            case toEnum raw of
              Nothing -> P.fail (name <> " out of range")
              Just x -> pure x

      -- |
      -- Parse an ISO8601 date string in UTC, correctly handling optional subseconds
      -- and variable subsecond lengths.
      datetime :: forall m. Monad m => ParserT String m DateTime
      datetime = DateTime
        <$> date
        <* PS.char 'T'
        <*> time
        <* PS.char 'Z'
    |]

producePrelude :: ReaderT Config FileM ()
producePrelude = do
  moduleName :: Text <- maybe id (\prefix name -> [i|#{prefix}.#{name}|])
    <$> Reader.asks cfgModulePrefix
    <*> pure "Extra.Prelude"
  parserModuleName :: Text <- maybe id (\prefix name -> [i|#{prefix}.#{name}|])
    <$> Reader.asks cfgModulePrefix
    <*> pure "Extra.ISO8601"
  p [__i'L|
      module #{moduleName}
        ( flip23
        , uuid
        , datetime
        , mapCodec
        , module Reexport
        )
      where
    |]

  p [__i|
      import Prelude ( bind, pure, (\#), ($), (<$>) ) as Reexport
      import Data.Argonaut.Core ( caseJsonObject, caseJsonString, fromObject, fromString ) as Reexport
      import Data.Codec ( decode, encode ) as Reexport
      import Data.Codec.Argonaut
        ( JsonDecodeError(..), JsonCodec
        , json, null, boolean, number, int, string, codePoint, char
        ) as Reexport
      import Data.Codec.Argonaut.Compat ( maybe ) as Reexport
      import Data.Codec.Argonaut.Record ( object ) as Reexport
      import Data.Either ( Either(..), note ) as Reexport
      import Foreign.Object ( lookup, fromHomogeneous ) as Reexport
    |]

  lift $ mark "prelude exports"

  p ""
  p [__i|
      import Data.Argonaut.Core as Argonaut
      import Data.Codec.Argonaut ( JsonDecodeError(..), JsonCodec )
      import Data.DateTime ( DateTime )
      import Data.Either ( Either(..) )
      import Data.Formatter.DateTime ( format )
      import Data.Maybe ( Maybe(..) )
      import Data.UUID ( UUID )
      import Prelude ( class Bind, ($) )

      import Data.Codec as Codec
      import Data.Codec.Argonaut as CA
      import Data.UUID as UUID
      import Parsing as P
      import #{parserModuleName} as ISO8601
    |]

  p ""
  p [__i|
      -- | Like `flip', but for the second and third arguments of a 3-argument function.
      flip23 :: forall a b c d. (a -> b -> c -> d) -> a -> c -> b -> d
      flip23 f x z y = f x y z

      uuid :: JsonCodec UUID
      uuid = Codec.composeFlipped CA.string $ Codec.codec'
        (\\s -> case UUID.parseUUID s of
          Nothing -> Left $ UnexpectedValue $ Argonaut.fromString s
          Just u -> Right u)
        UUID.toString

      datetime :: JsonCodec DateTime
      datetime = Codec.composeFlipped CA.string $ Codec.codec'
        (\\s -> case P.runParser s ISO8601.datetime of
            Left _ -> Left $ UnexpectedValue $ Argonaut.fromString s
            Right ts -> Right ts)
        (format ISO8601.formatter)

      mapCodec
        :: forall m a b c d
         . Bind m
        => (a -> m b)
        -> (b -> a)
        -> Codec.Codec m c d a a
        -> Codec.Codec m c d b b
      mapCodec f g codec =
        Codec.composeFlipped codec $ Codec.codec' f g
    |]

  lift $ mark "manual section"

produceSum :: SumDef -> ReaderT Config FileM ()
produceSum def = do
  let cutyname :: Text = [i|#{sumName def}CU|]
  let cucodec :: Text = [i|codec#{cutyname}|]
  let toplevelFields :: Seq (Field, Text) =
        [ (Field "id" (FlatType "UUID") (FlatType "UUID") (sumLoc def), "uuid")
        , (Field "body" (FlatType cutyname) (FlatType cutyname) (sumLoc def), cucodec)
        ]
        >< if sumIncludeTimestamps def
             then [ (Field "createdAt" (FlatType "UTCTime") (FlatType "DateTime") (sumLoc def), "datetime")
                  , (Field "updatedAt" (FlatType "UTCTime") (FlatType "DateTime") (sumLoc def), "datetime")
                  ]
             else Seq.empty

  produceRecord (sumName def) toplevelFields

  -- Produce the CU codec
  p [i|#{cucodec} :: JsonCodec #{cutyname}|]
  p [i|#{cucodec} = json \# mapCodec|]

  -- Produce mappings from JSON blob to PS type
  p [i|  (\\json -> flip23 caseJsonObject (Left $ TypeMismatch "object") json \\obj -> do|]
  p [i|    tag <- note MissingValue $ lookup "tag" obj|]
  p [i|    flip23 caseJsonString (Left $ TypeMismatch "string") tag $ case _ of|]
  for_ (sumVariants def) $ \variant -> do
    let varCodecName :: Text = [i|codec#{variantName variant}Fields|]
    p [i|      "#{toKebab (variantName variant)}" -> do|]
    if Seq.null (variantFields variant)
      then p [i|        pure #{variantName variant}|]
      else do
        p [i|        body <- note MissingValue $ lookup "body" obj|]
        p [i|        #{variantName variant} <$> decode #{varCodecName} body|]
  p [i|      _other -> Left $ UnexpectedValue json|]
  p [i|  )|]

  -- Produce mappings from PS type to JSON blob
  p [i|  (case _ of|]
  for_ (sumVariants def) $ \variant -> do
    let varCodecName :: Text = [i|codec#{variantName variant}Fields|]
    if Seq.null (variantFields variant)
      then p [i|    #{variantName variant} ->|]
      else p [i|    #{variantName variant} args ->|]
    p [i|      fromObject $ fromHomogeneous|]
    p [i|        { "tag": fromString "#{toKebab (variantName variant)}"|]
    unless (Seq.null $ variantFields variant) $
      p [i|        , "body": encode #{varCodecName} args|]
    p [i|        }|]
  p [i|  )|]

  -- Produce the Fields records for any variant that needs it
  for_ (sumVariants def) $ \variant ->
    unless (Seq.null $ variantFields variant) $ do
      let varFieldsName :: Text = [i|#{variantName variant}Fields|]
      fields <- traverse
        (\f -> liftA2 (,) (pure f) (jsonCodec (fieldPSType f) (fieldLoc f)))
        (variantFields variant)
      produceRecord varFieldsName fields

produceProd :: ProductDef -> ReaderT Config FileM ()
produceProd def = do
  let cutyname :: Text = [i|#{productName def}CU|]

  cuFields <- traverse
    (\f -> liftA2 (,) (pure f) (jsonCodec (fieldPSType f) (fieldLoc f)))
    (productFields def)
  fullFields <- traverse
    (\f -> liftA2 (,) (pure f) (jsonCodec (fieldPSType f) (fieldLoc f)))
    ( Field "id" (FlatType "UUID") (FlatType "UUID") (productLoc def)
      :<| productFields def
      >< if productIncludeTimestamps def
           then [ Field "createdAt" (FlatType "UTCTime") (FlatType "DateTime") (productLoc def)
                , Field "updatedAt" (FlatType "UTCTime") (FlatType "DateTime") (productLoc def)
                ]
           else Seq.empty
    )

  produceRecord (productName def) fullFields
  produceRecord cutyname cuFields

produceDataDefBody :: DataDefBody -> ReaderT Config FileM ()
produceDataDefBody def = case def of
  Sum sum -> produceSum sum
  Product prod -> produceProd prod

produceDataDefs :: Seq DataDef -> ReaderT Config ProducerM ()
produceDataDefs defs = do
  cfg <- Reader.ask
  for_ defs $ \def@(DataDef name body loc) -> do
    lift $ withFile [relfile|Extra/ISO8601.purs|] $
      runReaderT produceParser cfg
    lift $ withFile [relfile|Extra/Prelude.purs|] $
      runReaderT producePrelude cfg
    lift $ case parseRelFile [i|#{name}.purs|] of
      Nothing -> errorAt loc [i|invalid file name: #{name}|]
      Just filename -> withFile filename $ do
        runReaderT (producePreamble name def) cfg
        runReaderT (produceDataDefBody body) cfg
        mark "manual section"

produceRecord
  :: Text               -- ^ Type name
  -> Seq (Field, Text)  -- ^ Fields + codec for field
  -> ReaderT Config FileM ()
produceRecord tyname fields = do
  p [i|codec#{tyname} :: JsonCodec #{tyname}|]
  p [i|codec#{tyname} =|]
  p [i|  let|]

  -- Base codec for the on-the-wire data
  p [i|    base =|]
  p [i|      object "#{tyname}"|]
  case fields of
    Seq.Empty -> p [i|        {}|]
    (field, codec) :<| fields -> do
      p [i|        { "#{toKebab $ fieldName field}": #{codec}|]
      for_ fields $ \(field', codec') ->
        p [i|        , "#{toKebab $ fieldName field'}": #{codec'}|]
      p [i|        }|]
  p [i|  in base \# mapCodec|]

  -- Conversion from on-the-wire to Purescript type
  p [i|       (\\raw -> Right|]
  case fields of
    Seq.Empty -> p [i|         {})|]
    (field, _) :<| fields -> do
      p [i|         { #{fieldName field}: raw."#{toKebab $ fieldName field}"|]
      for_ fields $ \(field', _) ->
        p [i|         , #{fieldName field'}: raw."#{toKebab $ fieldName field'}"|]
      p [i|         })|]

  -- Conversion from Purescript type to on-the-wire
  p [i|       (\\purs ->|]
  case fields of
    Seq.Empty -> p [i|         {})|]
    (field, _) :<| fields -> do
      p [i|         { "#{toKebab $ fieldName field}": purs.#{fieldName field}|]
      for_ fields $ \(field', _) ->
        p [i|         , "#{toKebab $ fieldName field'}": purs.#{fieldName field'}|]
      p [i|         })|]

jsonCodec :: TypeInfo -> SrcLoc -> ReaderT Config FileM Text
jsonCodec ty loc = case ty of
  FlatType t -> flatnameCodec t loc
  MaybeType t -> (\c -> [i|(maybe #{c})|]) <$> flatnameCodec t loc
  where
    flatnameCodec :: Text -> SrcLoc -> ReaderT Config FileM Text
    flatnameCodec ty loc = case ty of
      "Unit"      -> pure "null"
      "Boolean"   -> pure "boolean"
      "Number"    -> pure "number"
      "Int"       -> pure "int"
      "String"    -> pure "string"
      "CodePoint" -> pure "codePoint"
      "Char"      -> pure "char"
      "UUID"      -> pure "uuid"
      "DateTime"  -> pure "datetime"
      _other      ->
        lift $ errorAt loc [i|unknown PureScript type: `#{ty}'|]

toKebab :: Identifier -> Identifier
toKebab = Text.pack . Casing.kebab . Text.unpack

p :: ByteString -> ReaderT Config FileM ()
p str = do
  lift (produce str)
  lift (produce "\n")

intercalate :: Text -> Seq Text -> Text
intercalate sep elems = case elems of
  Seq.Empty -> ""
  x :<| xs -> foldl' (\l r -> l <> sep <> r) x xs

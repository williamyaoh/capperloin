{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE QuasiQuotes         #-}
{-# LANGUAGE ScopedTypeVariables #-}

-- | A producer for Purescript type definitions in the web-typed format.
-- | Sums get translated by wrapping variant fields in a record. Products
-- | get turned into type aliases for Purescript records, without any wrapping
-- | using @data@ declarations.
-- |
-- | Compiling the output requires the @purescript-uuid@ and @purescript-datetime@
-- | libraries installed.
-- |
-- | Because Purescript does not have a way to ignore warnings related to
-- | unused imports, we use a small hack to avoid flooding compilation with
-- | warnings from the generated files: we output an @Extra/Prelude.purs@ file
-- | in the output directory, which exports the functions we might need. That
-- | then gets imported into the type files instead of the normal Prelude.

module Data.Capperloin.Producer.WebTyped.Purescript
  ( Config(..)
  , defaultConfig
  , producer
  )
where

import Control.Monad ( when )
import Control.Monad.Trans.Class ( lift )
import Control.Monad.Trans.Reader ( ReaderT, runReaderT )
import Data.ByteString ( ByteString )
import Data.Capperloin.Defs ( TypeInfo(..) )
import Data.Capperloin.Matcher.WebTyped.Purescript
import Data.Capperloin.Producer ( Producer(..), ProducerM, FileM, version, relfile, withFile, produce, mark, errorAt )
import Data.Foldable ( foldl', for_ )
import Data.Sequence ( Seq((:<|)) )
import Data.String.Interpolate ( i, __i )
import Data.Text ( Text )
import Path ( parseRelFile )

import qualified Control.Monad.Trans.Reader as Reader
import qualified Data.Sequence as Seq

data Config = Config
  { -- |
    -- The prefix of your modules, like @Data.MyTypes@. Don't add a trailing
    -- period.
    cfgModulePrefix :: Maybe Text
  , -- |
    -- Any extra stuff to stick in the outputted module imports.
    cfgExtraImports :: Maybe Text
  }
  deriving Show

defaultConfig :: Config
defaultConfig = Config
  { cfgModulePrefix = Nothing
  , cfgExtraImports = Nothing
  }

producer :: Config -> Producer
producer cfg =
  Producer
    { prodName = "capperloin-purescript"
    , prodVersion = version 0 1 0 [] []
    , prodMatcher = matchDataDefs
    , prodCommentF = ("-- " <>)
    , prodBody = flip runReaderT cfg . produceDataDefs
    }

-- | Produce the @Extra/Prelude.purs@ file, for the reasons described above.
producePrelude :: ReaderT Config FileM ()
producePrelude = do
  moduleName :: Text <- maybe id (\prefix name -> [i|#{prefix}.#{name}|])
    <$> Reader.asks cfgModulePrefix
    <*> pure "Extra.Prelude"
  p [__i|
      module #{moduleName}
        ( module Reexport )
      where

      import Data.DateTime ( DateTime ) as Reexport
      import Data.Maybe ( Maybe ) as Reexport
      import Data.UUID ( UUID ) as Reexport
    |]
  lift $ mark "prelude exports"

-- | Output the boring module header.
producePreamble :: Text -> ReaderT Config FileM ()
producePreamble fname = do
  moduleName <- maybe fname (\prefix -> [i|#{prefix}.#{fname}|])
    <$> Reader.asks cfgModulePrefix
  preludeName :: Text <- maybe id (\prefix name -> [i|#{prefix}.#{name}|])
    <$> Reader.asks cfgModulePrefix
    <*> pure "Extra.Prelude"
  p [__i|
      module #{moduleName} where

      import #{preludeName}
    |]

  mextraImports <- Reader.asks cfgExtraImports
  case mextraImports of
    Nothing -> pure ()
    Just extraImports -> p [i|#{extraImports}|]
  lift $ mark "imports"

produceSum :: SumDef -> ReaderT Config FileM ()
produceSum def = do
  let typevars = intercalate " " $ sumTypevars def
  let cuTypeName :: Text = [i|#{sumName def}CU|]

  -- Output normal type with ID
  p [i|type #{sumName def} #{typevars} =|]
  p [i|  { id :: UUID|]
  p [i|  , body :: #{cuTypeName} #{typevars}|]
  when (sumIncludeTimestamps def) $ do
    p [i|  , createdAt :: DateTime|]
    p [i|  , updatedAt :: DateTime|]
  p [i|  }|]

  -- Output CU type, which doubles as the actual sum to case on
  p [i|data #{cuTypeName} #{typevars}|]
  case sumVariants def of
    Seq.Empty -> pure ()
    variant :<| rest -> do
      p [i|  = #{variantName variant} #{varBody variant typevars}|]
      for_ rest $ \variant' ->
        p [i|  | #{variantName variant'} #{varBody variant' typevars}|]

  -- Output Fields types for any variants which need it
  for_ (sumVariants def) $ \variant ->
    case variantFields variant of
      Seq.Empty -> pure ()
      field :<| rest -> do
        p [i|type #{varBodyName variant} #{typevars} =|]
        p [i|  { #{fieldName field} :: #{outputType $ fieldPSType field}|]
        for_ rest $ \field' ->
          p [i|  , #{fieldName field'} :: #{outputType $ fieldPSType field'}|]
        p [i|  }|]
  where
    varBodyName :: SumVariant -> Text
    varBodyName variant = [i|#{variantName variant}Fields|]

    varBody :: SumVariant -> Text -> Text
    varBody variant typevars = case variantFields variant of
      Seq.Empty -> ""
      _         -> [i|(#{varBodyName variant} #{typevars})|]

produceProd :: ProductDef -> ReaderT Config FileM ()
produceProd def = do
  let typevars = intercalate " " $ productTypevars def
  let cuTypeName :: Text = [i|#{productName def}CU|]
  let timestampFields :: [Text] = if productIncludeTimestamps def
        then ["createdAt :: DateTime", "updatedAt :: DateTime"]
        else []

  -- Output normal type with ID
  p [i|type #{productName def} #{typevars} =|]
  p [i|  { id :: UUID|]
  for_ (productFields def) $ \field ->
    p [i|  , #{fieldName field} :: #{outputType $ fieldPSType field}|]
  for_ timestampFields $ \field ->
    p [i|  , #{field}|]
  p [i|  }|]

  -- Output CU type
  p [i|type #{cuTypeName} #{typevars} =|]
  case (productFields def) of
    Seq.Empty -> pure ()
    field :<| fields -> do
      p [i|  { #{fieldName field} :: #{outputType $ fieldPSType field}|]
      for_ fields $ \field' ->
        p [i|  , #{fieldName field'} :: #{outputType $ fieldPSType field'}|]
      p [i|  }|]

produceDataDef :: DataDefBody -> ReaderT Config FileM ()
produceDataDef def = do
  case def of
    Sum sum -> produceSum sum
    Product nprod -> produceProd nprod

produceDataDefs :: Seq DataDef -> ReaderT Config ProducerM ()
produceDataDefs defs = do
  cfg <- Reader.ask
  for_ defs $ \(DataDef name body loc) -> do
    lift $ withFile [relfile|Extra/Prelude.purs|] $
      runReaderT producePrelude cfg
    lift $ case parseRelFile [i|#{name}.purs|] of
      Nothing -> errorAt loc [i|invalid file name: #{name}|]
      Just filename -> withFile filename $ do
        runReaderT (producePreamble name) cfg
        runReaderT (produceDataDef body) cfg
        mark "manual section"

outputType :: TypeInfo -> Text
outputType (FlatType t) = t
outputType (MaybeType t) = [i|(Maybe #{t})|]

p :: ByteString -> ReaderT Config FileM ()
p str = do
  lift (produce str)
  lift (produce "\n")

intercalate :: Text -> Seq Text -> Text
intercalate sep elems = case elems of
  Seq.Empty -> ""
  x :<| xs -> foldl' (\l r -> l <> sep <> r) x xs

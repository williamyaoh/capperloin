{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ScopedTypeVariables #-}

-- |
-- Output codecs for Capperloin-defined types using @purescript-codec-argonaut@.

module Data.Capperloin.Producer.Purescript.JSONCodec where

import Control.Monad ( when )
import Control.Monad.Trans.Class ( lift )
import Control.Monad.Trans.Reader ( ReaderT, runReaderT )
import Data.ByteString ( ByteString )
import Data.Capperloin.Defs ( Identifier, SrcLoc, TypeInfo(..) )
import Data.Capperloin.Producer ( Producer(..), ProducerM, FileM, version, relfile, withFile, produce, mark, errorAt )
import Data.Capperloin.Producer.Purescript.Matcher
  ( AnonymousField(..)
  , NamedField(..)
  , SumVariant(..)
  , SumDef(..)
  , AnonymousProductDef(..)
  , NamedProductDef(..)
  , DataDef'(..)
  , DataDef(..)
  , matchDataDefs
  )
import Data.Foldable ( for_ )
import Data.Functor ( (<&>) )
import Data.String.Interpolate ( i, iii, __i, __i'L )
import Data.Text ( Text )
import Data.Traversable ( for )
import Path ( parseRelFile )

import qualified Control.Monad.Trans.Reader as Reader
import qualified Data.Text as Text
import qualified Text.Casing as Casing

data Config = Config
  { -- |
    -- Whether to output each codec into a separate file. If disabled, a single
    -- file called @Codecs.purs@ containing all codecs will be produced.
    cfgOutputIndividualFiles :: Bool
  , -- |
    -- The prefix of your modules, like @Data.MyTypes@. Don't add a trailing
    -- period.
    cfgModulePrefix :: Maybe Text
  , -- |
    -- The prefix of your types module.
    cfgTypesModulePrefix :: Maybe Text
  , -- |
    -- Any extra stuff to stick in the outputted module imports.
    cfgExtraImports :: Maybe Text
  }
  deriving Show

defaultConfig :: Config
defaultConfig = Config
  { cfgOutputIndividualFiles = True
  , cfgModulePrefix = Nothing
  , cfgTypesModulePrefix = Nothing
  , cfgExtraImports = Nothing
  }

producer :: Config -> Producer
producer cfg =
  Producer
    { prodName = "capperloin-purescript"
    , prodVersion = version 0 1 0 [] []
    , prodMatcher = matchDataDefs
    , prodCommentF = ("-- " <>)
    , prodBody = flip runReaderT cfg . produceDataDefs
    }

-- |
-- Output the boring module header.
producePreamble :: Text -> Text -> ReaderT Config FileM ()
producePreamble fname tyname = do
  moduleName <- maybe fname (\prefix -> [i|#{prefix}.#{fname}|])
    <$> Reader.asks cfgModulePrefix
  p [__i'L|
      module #{moduleName} where
    |]

  typesModuleName <- maybe tyname (\prefix -> [i|#{prefix}.#{tyname}|])
    <$> Reader.asks cfgTypesModulePrefix
  p [__i|
      import Prelude

      import Data.Argonaut.Core as Argonaut
      import Data.Codec ( (~) )
      import Data.Codec as Codec
      import Data.Codec.Argonaut ( JsonDecodeError(..), JsonCodec )
      import Data.Codec.Argonaut as CA
      import Data.Codec.Argonaut.Compat as CAC
      import Data.Codec.Argonaut.Record ( object )
      import Data.Either ( Either(..) )
      import Data.Maybe ( Maybe(..) )
      import Foreign.Object as Object

      import #{typesModuleName}
    |]

  mextraImports <- Reader.asks cfgExtraImports
  case mextraImports of
    Nothing -> pure ()
    Just extraImports -> p [i|#{extraImports}|]
  lift $ mark "imports"

produceSum :: SumDef -> ReaderT Config FileM ()
produceSum def = do
  let cutyname :: Text = [i|#{sumName def}CU|]

  p [i|codec#{sumName def} :: JsonCodec #{sumName def}|]
  p [i|codec#{sumName def} =|]
  p [i|  object "#{sumName def}"|]
  p [i|    { tag: CA.string|]
  p [i|    , id: uuid|]
  p [i|    , body: codec#{cutyname}|]
  when (sumIncludeTimestamps def) $ do
    p [i|    , "created-at": datetime|]
    p [i|    , "updated-at": datetime|]
  p [i|    }|]
  p [i|    \# Codec.mapCodec|]
  p [i|         (\\raw -> if raw.tag /= "#{toKebab (sumName def)}"|]
  p [i|           then Left (TypeMismatch $ "expected tag #{toKebab (sumName def)} but got " <> raw.tag)|]
  p [i|           else Right $|]
  p [i|             { id: raw.id|]
  p [i|             , body: raw.body|]
  when (sumIncludeTimestamps def) $ do
    p [i|             , createdAt: raw."created-at"|]
    p [i|             , updatedAt: raw."updated-at"|]
  p [i|             })|]
  p [i|         (\\ps ->|]
  p [i|           { tag: "#{toKebab (sumName def)}"|]
  p [i|           , id: ps.id|]
  p [i|           , body: ps.body|]
  when (sumIncludeTimestamps def) $ do
    p [i|           , "created-at": ps.createdAt|]
    p [i|           , "updated-at": ps.updatedAt|]
  p [i|           })|]

  p [i|codec#{cutyname} :: JsonCodec #{cutyname}|]
  p [i|codec#{cutyname} = CA.json \# Codec.mapCodec|]

  -- Produce mappings from JSON blob to PS type
  p [i|  (\\json -> arg23 Argonaut.caseJsonObject (Left $ TypeMismatch "object") json \\obj -> do|]
  p [i|    tag <- note MissingValue $ Object.lookup "tag" obj|]
  p [i|    arg23 Argonaut.caseJsonString (Left $ TypeMismatch "string") tag $ case _ of|]
  for_ (sumVariants def) $ \variant -> do
    fields <- for (variantFields variant) $ \field ->
      jsonCodec (anonFieldPSType field) (anonFieldDefLoc field)
    p [i|      "#{toKebab (variantName variant)}" -> do|]
    p [i|        rec <- Codec.decode raw#{variantName variant} json|]
    p [i|        (#{indexedMapJSONToPS (variantName variant) fields}) rec|]
  p [i|      _other -> Left $ CA.UnexpectedValue json|]
  p [i|  )|]

  -- Produce mappings from PS type to JSON blob
  p [i|  (case _ of|]
  for_ (sumVariants def) $ \variant -> do
    p [i|    #{variantName variant} #{indexedFields (variantFields variant)} ->|]
    p [i|      Codec.encode raw#{variantName variant}|]
    p [i|        { "tag": "#{toKebab (variantName variant)}"|]
    when (not $ null $ variantFields variant) $
      p [i|        , "body": #{indexedRecordFn (variantFields variant)} #{indexedFields (variantFields variant)}|]
    p [i|        }|]
  p [i|  )|]

  -- Produce where clauses
  p [i|  where|]
  p [i|    note :: forall e a. e -> Maybe a -> Either e a|]
  p [i|    note e = case _ of|]
  p [i|      Nothing -> Left e|]
  p [i|      Just x -> Right x|]
  p ""
  p [i|    arg23 :: forall a b c d. (a -> b -> c -> d) -> a -> c -> b -> d|]
  p [i|    arg23 f x z y = f x y z|]
  p ""
  for_ (sumVariants def) $ \variant -> do
    fields <- for (variantFields variant) $ \field ->
      jsonCodec (anonFieldPSType field) (anonFieldDefLoc field)
    p [i|    raw#{variantName variant} =|]
    p [i|      object "#{variantName variant}"|]
    p [i|        { "tag": CA.string|]
    when (not $ null $ variantFields variant) $
      p [i|        , "body": #{indexedBaseCodec (variantName variant) fields}|]
    p [i|        }|]

produceAnonProd :: AnonymousProductDef -> ReaderT Config FileM ()
produceAnonProd def = do
  let cutyname :: Text = [i|#{anonProdName def}CU|]
      cuconstructor :: Text = [i|#{anonProdConstructor def}CU|]
  cufields <- for (anonProdFields def) $ \field ->
    jsonCodec (anonFieldPSType field) (anonFieldDefLoc field)

  let basefields =
        let
          withID = "uuid" : cufields
          timestampFields = if anonProdIncludeTimestamps def
            then ["datetime", "datetime"]
            else []
        in withID <> timestampFields

  -- Produce base type
  p [i|codec#{anonProdName def} :: JsonCodec #{anonProdName def}|]
  p [i|codec#{anonProdName def} =|]
  p [i|  let|]
  p [i|    base =|]
  p [i|      object "#{anonProdName def}"|]
  p [i|        { "tag": CA.string|]
  p [i|        , "body": #{indexedBaseCodec (anonProdName def) basefields}|]
  p [i|        }|]
  p [i|  in base \# Codec.mapCodec|]
  p [i|       (#{indexedMapJSONToPS (anonProdConstructor def) basefields})|]
  p [i|       (#{indexedMapPSToJSON (anonProdConstructor def) basefields})|]

  p ""

  -- Produce CU type
  p [i|codec#{cutyname} :: JsonCodec #{cutyname}|]
  p [i|codec#{cutyname} =|]
  p [i|  let|]
  p [i|    base =|]
  p [i|      object "#{cutyname}"|]
  p [i|        { "tag": CA.string|]
  when (not $ null $ anonProdFields def) $ do
    p [i|        , "body": #{indexedBaseCodec (cutyname) cufields}|]
  p [i|        }|]
  p [i|  in base \# Codec.mapCodec|]
  p [i|       (#{indexedMapJSONToPS cuconstructor cufields})|]
  p [i|       (#{indexedMapPSToJSON cuconstructor cufields})|]

produceNamedProd :: NamedProductDef -> ReaderT Config FileM ()
produceNamedProd def = do
  let cutyname :: Text = [i|#{namedProdName def}CU|]

  -- Produce base type
  p [i|codec#{namedProdName def} :: JsonCodec #{namedProdName def}|]
  p [i|codec#{namedProdName def} =|]
  p [i|  let|]

  -- Base codec for the on-the-wire data
  p [i|    base =|]
  p [i|      object "#{namedProdName def}"|]
  p [i|        { "id": uuid|]
  for_ (namedProdFields def) $ \field -> do
    codec <- jsonCodec (fieldPSType field) (fieldDefLoc field)
    p [i|        , "#{toKebab $ fieldName field}": #{codec}|]
  when (namedProdIncludeTimestamps def) $ do
    p [i|        , "created-at": datetime|]
    p [i|        , "updated-at": datetime|]
  p [i|        }|]
  p [i|  in base \# Codec.mapCodec|]

  -- Conversion from on-the-wire to Purescript type
  p [i|       (\\raw -> Right|]
  p [i|         { id: raw.id|]
  for_ (namedProdFields def) $ \field ->
    p [i|         , #{fieldName field}: raw."#{toKebab $ fieldName field}"|]
  when (namedProdIncludeTimestamps def) $ do
    p [i|         , createdAt: raw."created-at"|]
    p [i|         , updatedAt: raw."updated-at"|]
  p [i|         })|]

  -- Conversion from Purescript type to on-the-wire
  p [i|       (\\purs ->|]
  p [i|         { id: purs.id|]
  for_ (namedProdFields def) $ \field ->
    p [i|         , "#{toKebab $ fieldName field}": purs.#{fieldName field}|]
  when (namedProdIncludeTimestamps def) $ do
    p [i|         , "created-at": purs.createdAt|]
    p [i|         , "updated-at": purs.updatedAt|]
  p [i|         })|]

  p ""

  -- Produce CU type
  p [i|codec#{cutyname} :: JsonCodec #{cutyname}|]
  p [i|codec#{cutyname} =|]
  p [i|  let|]

  -- Base codec for the on-the-wire data
  p [i|    base =|]
  p [i|      object "#{cutyname}"|]
  case namedProdFields def of
    [] -> p [i|        {}|]
    (field:fields) -> do
      codec <- jsonCodec (fieldPSType field) (fieldDefLoc field)
      p [i|        { "#{toKebab $ fieldName field}": #{codec}|]
      for_ fields $ \field -> do
        codec <- jsonCodec (fieldPSType field) (fieldDefLoc field)
        p [i|        , "#{toKebab $ fieldName field}": #{codec}|]
      p [i|        }|]
  p [i|  in base \# Codec.mapCodec|]

  -- Conversion from on-the-wire to Purescript type
  p [i|       (\\raw -> Right|]
  case namedProdFields def of
    [] -> p [i|         {})|]
    (field:fields) -> do
      p [i|         { #{fieldName field}: raw."#{toKebab $ fieldName field}"|]
      for_ fields $ \field ->
        p [i|         , #{fieldName field}: raw."#{toKebab $ fieldName field}"|]
      p [i|         })|]

  -- Conversion from Purescript type to on-the-wire
  p [i|       (\\purs ->|]
  case namedProdFields def of
    [] -> p [i|         {})|]
    (field:fields) -> do
      p [i|         { "#{toKebab $ fieldName field}": purs.#{fieldName field}|]
      for_ fields $ \field ->
        p [i|         , "#{toKebab $ fieldName field}": purs.#{fieldName field}|]
      p [i|         })|]

produceDataDef' :: DataDef' -> ReaderT Config FileM ()
produceDataDef' def = do
  case def of
    Sum sum -> produceSum sum
    AnonymousProduct aprod -> produceAnonProd aprod
    NamedProduct nprod -> produceNamedProd nprod

produceDataDefs :: [DataDef] -> ReaderT Config ProducerM ()
produceDataDefs defs = do
  cfg <- Reader.ask
  if cfgOutputIndividualFiles cfg
    then for_ defs $ \(DataDef name body) ->
      lift $ case parseRelFile [i|#{name}.purs|] of
        Nothing -> error [i|invalid file name: #{name}|]
        Just filename -> withFile filename $ do
          runReaderT (producePreamble name name) cfg
          runReaderT (produceDataDef' body) cfg
          mark "manual section"
    else lift $ withFile [relfile|Codecs.purs|] $ do
      runReaderT (producePreamble "Codecs" "Types") cfg
      for_ defs $ \(DataDef _ body) -> do
        runReaderT (produceDataDef' body) cfg
        mark "manual section"

jsonCodec :: TypeInfo -> SrcLoc -> ReaderT Config FileM Text
jsonCodec ty loc = case ty of
  FlatType t -> flatnameCodec t loc
  MaybeType t -> (\c -> [i|(CAC.maybe #{c})|]) <$> flatnameCodec t loc
  where
    flatnameCodec :: Text -> SrcLoc -> ReaderT Config FileM Text
    flatnameCodec ty loc = case ty of
      "Unit"      -> pure "CA.null"
      "Boolean"   -> pure "CA.boolean"
      "Number"    -> pure "CA.number"
      "Int"       -> pure "CA.int"
      "String"    -> pure "CA.string"
      "CodePoint" -> pure "CA.codePoint"
      "Char"      -> pure "CA.char"
      "UUID"      -> pure "uuid"
      "DateTime"  -> pure "datetime"
      _other      ->
        lift $ errorAt loc [i|unknown PureScript type: `#{ty}'|]

indexedRecordFn :: [a] -> Text
indexedRecordFn fields =
  (\t -> [i|{ #{t} }|]) $ Text.intercalate ", " $ zip [0..] fields <&>
    \(idx :: Int, _) -> [i|"_#{idx}": _|]

indexedFields :: [a] -> Text
indexedFields fields =
  Text.intercalate " " $ zip [0..] fields <&>
    \(idx :: Int, _) -> [i|_#{idx}|]

indexedBaseCodec :: Text -> [Text] -> Text
indexedBaseCodec constructor codecs = case codecs of
  [codec] -> [i|_._0 ~ ({ "_0": _ } <$> #{codec})|]
  _       ->
    (\t -> [i|CA.indexedArray "#{constructor}" (pure #{indexedRecordFn codecs} #{t})|]) $
      Text.intercalate " " $ zip [0..] codecs <&> \(n :: Int, codec) ->
        [i|<*> _."_#{n}" ~ CA.index #{n} (#{codec})|]

indexedMapJSONToPS :: Text -> [a] -> Text
indexedMapJSONToPS constructor codecs =
  (\t -> [iii|
           \\raw -> if raw.tag /= "#{toKebab constructor}"
             then Left (TypeMismatch $ "expected tag #{toKebab constructor} but got " <> raw.tag)
             else Right (#{constructor} #{t})
         |]) $
    Text.intercalate " " $ zip [0..] codecs <&> \(n :: Int, _) ->
      [i|raw.body."_#{n}"|]

indexedMapPSToJSON :: Text -> [a] -> Text
indexedMapPSToJSON constructor codecs =
  (\t -> [i|\\(#{constructor} #{indexedFields codecs}) -> { "tag": "#{toKebab constructor}", "body": {#{t}} }|]) $
    Text.intercalate ", " $ zip [0..] codecs <&> \(n :: Int, _) -> [i|"_#{n}": _#{n}|]

outputType :: TypeInfo -> Text
outputType (FlatType t) = t
outputType (MaybeType t) = [i|(Maybe #{t})|]

toKebab :: Identifier -> Identifier
toKebab = Text.pack . Casing.kebab . Text.unpack

p :: ByteString -> ReaderT Config FileM ()
p str = do
  lift (produce str)
  lift (produce "\n")

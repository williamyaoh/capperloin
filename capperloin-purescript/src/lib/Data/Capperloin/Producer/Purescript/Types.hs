{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE QuasiQuotes         #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Data.Capperloin.Producer.Purescript.Types where

import Control.Monad ( when )
import Control.Monad.Trans.Class ( lift )
import Control.Monad.Trans.Reader ( ReaderT, runReaderT )
import Data.ByteString ( ByteString )
import Data.Capperloin.Defs ( TypeInfo(..) )
import Data.Capperloin.Producer ( Producer(..), ProducerM, FileM, relfile, version, withFile, produce, mark )
import Data.Capperloin.Producer.Purescript.Matcher
  ( AnonymousField(..)
  , NamedField(..)
  , SumVariant(..)
  , SumDef(..)
  , AnonymousProductDef(..)
  , NamedProductDef(..)
  , DataDef'(..)
  , DataDef(..)
  , matchDataDefs
  )
import Data.Foldable ( for_ )
import Data.String.Interpolate ( i, __i'L )
import Data.Text ( Text )
import Path ( parseRelFile )

import qualified Control.Monad.Trans.Reader as Reader
import qualified Data.Text as Text

data Config = Config
  { -- |
    -- Whether to output each type into a separate file. If disabled, a single
    -- file called @Types.hs@ containing all types will be produced.
    cfgOutputIndividualFiles :: Bool
  , -- |
    -- The prefix of your modules, like @Data.MyTypes@. Don't add a trailing
    -- period.
    cfgModulePrefix :: Maybe Text
  , -- |
    -- Any extra stuff to stick in the outputted module imports.
    cfgExtraImports :: Maybe Text
  }
  deriving Show

defaultConfig :: Config
defaultConfig = Config
  { cfgOutputIndividualFiles = True
  , cfgModulePrefix = Nothing
  , cfgExtraImports = Nothing
  }

producer :: Config -> Producer
producer cfg =
  Producer
    { prodName = "capperloin-purescript"
    , prodVersion = version 0 1 0 [] []
    , prodMatcher = matchDataDefs
    , prodCommentF = ("-- " <>)
    , prodBody = flip runReaderT cfg . produceDataDefs
    }

-- |
-- Output the boring module header.
producePreamble :: Text -> ReaderT Config FileM ()
producePreamble fname = do
  moduleName <- maybe fname (\prefix -> [i|#{prefix}.#{fname}|])
    <$> Reader.asks cfgModulePrefix
  p [__i'L|
      module #{moduleName} where
    |]

  p [__i'L|
      import Data.UUID ( UUID )
      import Data.Maybe ( Maybe )
    |]

  mextraImports <- Reader.asks cfgExtraImports
  case mextraImports of
    Nothing -> pure ()
    Just extraImports -> p [i|#{extraImports}|]
  lift $ mark "imports"

produceSum :: SumDef -> ReaderT Config FileM ()
produceSum def = do
  let cuTypeName :: Text = [i|#{sumName def}CU|]

  -- Output normal type with ID
  p [i|type #{sumName def} #{Text.intercalate " " $ sumTypevars def} =|]
  p [i|  { id :: UUID|]
  p [i|  , body :: #{cuTypeName} #{Text.intercalate " " $ sumTypevars def}|]
  when (sumIncludeTimestamps def) $ do
    p [i|  , createdAt :: DateTime|]
    p [i|  , updatedAt :: DateTime|]
  p [i|  }|]

  -- Output CU type, which doubles as the actual sum to case on
  p [i|data #{cuTypeName} #{Text.intercalate " " $ sumTypevars def}|]
  case sumVariants def of
    [] -> pure ()
    (SumVariant varname varfields:rest) -> do
      p [i|  = #{varname} #{Text.intercalate " " $ fmap (outputType . anonFieldPSType) varfields}|]
      for_ rest $ \(SumVariant varname varfields) ->
        p [i|  | #{varname} #{Text.intercalate " " $ fmap (outputType . anonFieldPSType) varfields}|]

produceAnonProd :: AnonymousProductDef -> ReaderT Config FileM ()
produceAnonProd def = do
  let
    cuTypeName :: Text = [i|#{anonProdName def}CU|]
    cuTypeConstructor :: Text = [i|#{anonProdConstructor def}CU|]
    timestampFields = if anonProdIncludeTimestamps def
      then ["DateTime", "DateTime"]
      else []

  -- Output normal type with ID
  p [i|data #{anonProdName def} #{Text.intercalate " " $ anonProdTypevars def} =|]
  p [i|  #{anonProdConstructor def} UUID #{Text.intercalate " " $ fmap (outputType . anonFieldPSType) $ anonProdFields def} #{Text.intercalate " " timestampFields}|]

  -- Output CU type
  p [i|data #{cuTypeName} #{Text.intercalate " " $ anonProdTypevars def} =|]
  p [i|  #{cuTypeConstructor} #{Text.intercalate " " $ fmap (outputType . anonFieldPSType) $ anonProdFields def}|]

produceNamedProd :: NamedProductDef -> ReaderT Config FileM ()
produceNamedProd def = do
  let
    cuTypeName :: Text = [i|#{namedProdName def}CU|]
    timestampFields :: [Text] = if namedProdIncludeTimestamps def
      then ["createdAt :: DateTime", "updatedAt :: DateTime"]
      else []

  -- Output normal type with ID
  p [i|type #{namedProdName def} #{Text.intercalate " " $ namedProdTypevars def} =|]
  p [i|  { id :: UUID|]
  for_ (namedProdFields def) $ \field ->
    p [i|  , #{fieldName field} :: #{outputType $ fieldPSType field}|]
  for_ timestampFields $ \field ->
    p [i|  , #{field}|]
  p [i|  }|]

  -- Output CU type
  p [i|type #{cuTypeName} #{Text.intercalate " " $ namedProdTypevars def} =|]
  case (namedProdFields def) of
    [] -> pure ()
    (field:fields) -> do
      p [i|  { #{fieldName field} :: #{outputType $ fieldPSType field}|]
      for_ fields $ \field ->
        p [i|  , #{fieldName field} :: #{outputType $ fieldPSType field}|]
      p [i|  }|]

produceDataDef :: DataDef' -> ReaderT Config FileM ()
produceDataDef def = do
  case def of
    Sum sum -> produceSum sum
    AnonymousProduct aprod -> produceAnonProd aprod
    NamedProduct nprod -> produceNamedProd nprod

produceDataDefs :: [DataDef] -> ReaderT Config ProducerM ()
produceDataDefs defs = do
  cfg <- Reader.ask
  if cfgOutputIndividualFiles cfg
    then for_ defs $ \(DataDef name body) ->
      lift $ case parseRelFile [i|#{name}.purs|] of
        Nothing -> error [i|invalid file name: #{name}|]
        Just filename -> withFile filename $ do
          runReaderT (producePreamble name) cfg
          runReaderT (produceDataDef body) cfg
          mark "manual section"
    else lift $ withFile [relfile|Types.purs|] $ do
      runReaderT (producePreamble "Types") cfg
      for_ defs $ \(DataDef _ body) -> do
        runReaderT (produceDataDef body) cfg
        mark "manual section"

outputType :: TypeInfo -> Text
outputType (FlatType t) = t
outputType (MaybeType t) = [i|(Maybe #{t})|]

p :: ByteString -> ReaderT Config FileM ()
p str = do
  lift (produce str)
  lift (produce "\n")

{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE OverloadedStrings #-}

module Data.Capperloin.Producer.Purescript.Matcher where

import Data.Maybe ( isJust )
import Data.Capperloin.Defs ( Identifier, SrcLoc, TypeInfo(..), capperloinNS, haskellNS, haskellContentNS )
import Data.Capperloin.Defs.Postgres ( postgresNS )
import Data.Capperloin.Defs.Purescript ( purescriptNS, purescriptContentNS )
import Data.Capperloin.Matcher
import Data.String.Interpolate ( i )

data AnonymousField = AnonymousField
  { anonFieldType :: TypeInfo
  , anonFieldPSType :: TypeInfo
  , anonFieldDefLoc :: SrcLoc
  }

data NamedField = NamedField
  { fieldName :: Identifier
  , fieldType :: TypeInfo
  , fieldPSType :: TypeInfo
  , fieldDefLoc :: SrcLoc
  }

data SumVariant = SumVariant
  { variantName :: Identifier
  , variantFields :: [AnonymousField]
  }

data SumDef = SumDef
  { sumName :: Identifier
  , sumTypevars :: [Identifier]
  , sumVariants :: [SumVariant]
  , sumIncludeTimestamps :: Bool
  , sumSrcLoc :: SrcLoc
  }

data AnonymousProductDef = AnonymousProductDef
  { anonProdName :: Identifier
  , anonProdTypevars :: [Identifier]
  , anonProdConstructor :: Identifier
  , anonProdFields :: [AnonymousField]
  , anonProdIncludeTimestamps :: Bool
  }

data NamedProductDef = NamedProductDef
  { namedProdName :: Identifier
  , namedProdTypevars :: [Identifier]
  , namedProdConstructor :: Identifier
  , namedProdFields :: [NamedField]
  , namedProdIncludeTimestamps :: Bool
  , namedProdSrcLoc :: SrcLoc
  }

data DataDef'
  = Sum SumDef
  | AnonymousProduct AnonymousProductDef
  | NamedProduct NamedProductDef

data DataDef = DataDef
  { defName :: Identifier
  , defBody :: DataDef'
  }

hs :: Identifier -> DefsMatcher a -> ChildMatcher a
hs = exact haskellNS

ps :: Identifier -> DefsMatcher a -> ChildMatcher a
ps = exact purescriptNS

psc :: Identifier -> DefsMatcher a -> ChildMatcher a
psc = exact purescriptContentNS

pg :: Identifier -> DefsMatcher a -> ChildMatcher a
pg = exact postgresNS

nmHS :: (Identifier -> DefsMatcher a) -> ChildMatcher a
nmHS = namedNS haskellNS

nmHSC :: (Identifier -> DefsMatcher a) -> ChildMatcher a
nmHSC = namedNS haskellContentNS

nmPS :: (Identifier -> DefsMatcher a) -> ChildMatcher a
nmPS = namedNS purescriptNS

nmPSC :: (Identifier -> DefsMatcher a) -> ChildMatcher a
nmPSC = namedNS purescriptContentNS

nmPG :: (Identifier -> DefsMatcher a) -> ChildMatcher a
nmPG = namedNS postgresNS

type_ :: (TypeInfo -> DefsMatcher a) -> ChildMatcher a
type_ body = choiceChild
  [ hs "field" $ exists $ nmHSC $ \typename ->
      body (FlatType typename)
  , hs "field" $ exists $ hs "maybe" $ exists $ nmHSC $ \typename ->
      body (MaybeType typename)
  ]

pstype_ :: ChildMatcher TypeInfo
pstype_ = choiceChild
  [ ps "type" $ exists $ nmPSC $ \typename -> pure (FlatType typename)
  , ps "type" $ exists $ ps "maybe" $ exists $ nmPSC $ \typename -> pure (MaybeType typename)
  ]

matchIncludeTimestamps :: DefsMatcher Bool
matchIncludeTimestamps = fmap isJust $ try $ exists $
  pg "timestamps" $ pure ()

matchSum :: (Identifier, [Identifier]) -> DefsMatcher SumDef
matchSum (name, typevars) = do
  sumLoc <- srcLoc
  includeTimestamps <- matchIncludeTimestamps
  variants <- exists $ hs "variants" $
    forAll $ hs "variant" $
      exists $ nmHS $ \varname -> do
        fields <- exists $ hs "fields" $
          forAll $ type_ $ \ty -> do
            loc <- srcLoc
            pstype <- exists pstype_
            pure $! AnonymousField ty pstype loc
        pure $! SumVariant varname fields
  pure $! SumDef name typevars variants includeTimestamps sumLoc

matchAnonProd :: (Identifier, [Identifier]) -> DefsMatcher AnonymousProductDef
matchAnonProd (name, typevars) =
  exists $ nmHS $ \constructor -> do
    includeTimestamps <- matchIncludeTimestamps
    exists $ hs "fields" $ do
      fields <- forAll $ type_ $ \ty -> do
        loc <- srcLoc
        pstype <- exists pstype_
        pure $! AnonymousField ty pstype loc
      pure $! AnonymousProductDef name typevars constructor fields includeTimestamps

matchNamedProd :: (Identifier, [Identifier]) -> DefsMatcher NamedProductDef
matchNamedProd (name, typevars) = do
  prodLoc <- srcLoc
  exists $ nmHS $ \constructor -> do
    includeTimestamps <- matchIncludeTimestamps
    exists $ hs "fields" $ do
      fields <- forAll $ hs "named" $
        exists $ nmHS $ \fieldName ->
          exists $ type_ $ \ty -> do
            loc <- srcLoc
            pstype <- exists pstype_
            pure $! NamedField fieldName ty pstype loc
      pure $! NamedProductDef name typevars constructor fields includeTimestamps prodLoc

matchDataDef :: Identifier -> DefsMatcher DataDef
matchDataDef name = do
  typevars <- exists $ hs "typevars" $
    forAll $ nmHS $ \typevar -> pure typevar
  exists $ hs "definition" $ do
    body <- exists $ nmHS $ \deftype -> case deftype of
      "sum"               -> Sum <$> matchSum (name, typevars)
      "anonymous-product" -> AnonymousProduct <$> matchAnonProd (name, typevars)
      "product"           -> NamedProduct <$> matchNamedProd (name, typevars)
      _                   -> fail [i|unknown definition type: `#{deftype}'|]
    pure $! DataDef name body

matchDataDefs :: DefsMatcher [DataDef]
matchDataDefs = forAll $ namedNS capperloinNS $ \typename ->
  matchDataDef typename

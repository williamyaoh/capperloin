{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE QuasiQuotes         #-}
{-# LANGUAGE ScopedTypeVariables #-}

-- |
-- In order to actually use the JSON codecs in this package, we have to ensure
-- that our backend is producing the JSON we expect. This producer produces
-- Haskell code to use the @deriving-aeson@ package to do exactly that.

module Data.Capperloin.Producer.Purescript.DerivingAeson where

import Control.Monad.Trans.Class ( lift )
import Control.Monad.Trans.Reader ( ReaderT, runReaderT )
import Data.ByteString ( ByteString )
import Data.Capperloin.Producer ( Producer(..), ProducerM, FileM, version, withFile, produce, mark )
import Data.Capperloin.Producer.Purescript.Matcher
  ( SumDef(..)
  , AnonymousProductDef(..)
  , NamedProductDef(..)
  , DataDef'(..)
  , DataDef(..)
  , matchDataDefs
  )
import Data.Foldable ( for_ )
import Data.String.Interpolate ( i, iii, __i'L )
import Data.Text ( Text )
import Path ( parseRelFile )

import qualified Control.Monad.Trans.Reader as Reader

data Config = Config
  { -- |
    -- The name of your module containing the ToJSON/FromJSON instances.
    cfgModuleName :: Text
  , -- |
    -- The prefix of your module, like @Data.MyTypes@. Don't add a trailing
    -- period.
    cfgModulePrefix :: Maybe Text
  , -- |
    -- The prefix of your types module.
    cfgTypesModulePrefix :: Maybe Text
  , -- |
    -- Any extra stuff to stick in the outputted module imports.
    cfgExtraImports :: Maybe Text
  }
  deriving Show

defaultConfig :: Config
defaultConfig = Config
  { cfgModuleName = "Serialize"
  , cfgModulePrefix = Nothing
  , cfgTypesModulePrefix = Nothing
  , cfgExtraImports = Nothing
  }

producer :: Config -> Producer
producer cfg =
  Producer
    { prodName = "capperloin-purescript"
    , prodVersion = version 0 1 0 [] []
    , prodMatcher = matchDataDefs
    , prodCommentF = ("-- " <>)
    , prodBody = flip runReaderT cfg . produceDataDefs
    }

-- |
-- Output the boring module header.
producePreamble :: Text -> ReaderT Config FileM ()
producePreamble fname = do
  moduleName <- maybe fname (\prefix -> [i|#{prefix}.#{fname}|])
    <$> Reader.asks cfgModulePrefix
  p [__i'L|
      {-\# OPTIONS -Wno-orphans             \#-}
      {-\# OPTIONS -fno-warn-unused-imports \#-}
      {-\# LANGUAGE DerivingStrategies      \#-}
      {-\# LANGUAGE DerivingVia             \#-}
      {-\# LANGUAGE StandaloneDeriving      \#-}
      {-\# LANGUAGE DataKinds               \#-}

      module #{moduleName} where

      import Deriving.Aeson
    |]

  mextraImports <- Reader.asks cfgExtraImports
  case mextraImports of
    Nothing -> pure ()
    Just extraImports -> p [i|#{extraImports}|]
  lift $ mark "imports"

produceSum :: SumDef -> ReaderT Config FileM ()
produceSum def = do
  let cutyname :: Text = [i|#{sumName def}CU|]
  let tags :: Text =
        [iii|
          FieldLabelModifier '[CamelToKebab],
          ConstructorTagModifier '[CamelToKebab],
          TagSingleConstructors,
          NoAllNullaryToStringTag,
          SumTaggedObject "tag" "body"
        |]
  p [i|deriving via (CustomJSON '[#{tags}] #{sumName def}) instance ToJSON #{sumName def}|]
  p [i|deriving via (CustomJSON '[#{tags}] #{sumName def}) instance FromJSON #{sumName def}|]
  p [i|deriving via (CustomJSON '[#{tags}] #{cutyname}) instance ToJSON #{cutyname}|]
  p [i|deriving via (CustomJSON '[#{tags}] #{cutyname}) instance FromJSON #{cutyname}|]

produceAnonProd :: AnonymousProductDef -> ReaderT Config FileM ()
produceAnonProd def = do
  let cutyname :: Text = [i|#{anonProdName def}CU|]
  let tags :: Text =
        [iii|
          FieldLabelModifier '[CamelToKebab],
          ConstructorTagModifier '[CamelToKebab],
          TagSingleConstructors,
          NoAllNullaryToStringTag,
          SumTaggedObject "tag" "body"
        |]
  p [i|deriving via (CustomJSON '[#{tags}] #{anonProdName def}) instance ToJSON #{anonProdName def}|]
  p [i|deriving via (CustomJSON '[#{tags}] #{anonProdName def}) instance FromJSON #{anonProdName def}|]
  p [i|deriving via (CustomJSON '[#{tags}] #{cutyname}) instance ToJSON #{cutyname}|]
  p [i|deriving via (CustomJSON '[#{tags}] #{cutyname}) instance FromJSON #{cutyname}|]

produceNamedProd :: NamedProductDef -> ReaderT Config FileM ()
produceNamedProd def = do
  let cutyname :: Text = [i|#{namedProdName def}CU|]
  let tags :: Text =
        [iii|
          FieldLabelModifier '[CamelToKebab],
          ConstructorTagModifier '[CamelToKebab]
        |]
  p [i|deriving via (CustomJSON '[#{tags}] #{namedProdName def}) instance ToJSON #{namedProdName def}|]
  p [i|deriving via (CustomJSON '[#{tags}] #{namedProdName def}) instance FromJSON #{namedProdName def}|]
  p [i|deriving via (CustomJSON '[#{tags}] #{cutyname}) instance ToJSON #{cutyname}|]
  p [i|deriving via (CustomJSON '[#{tags}] #{cutyname}) instance FromJSON #{cutyname}|]

produceDataDef' :: DataDef' -> ReaderT Config FileM ()
produceDataDef' def = do
  case def of
    Sum sum -> produceSum sum
    AnonymousProduct aprod -> produceAnonProd aprod
    NamedProduct nprod -> produceNamedProd nprod

produceDataDefs :: [DataDef] -> ReaderT Config ProducerM ()
produceDataDefs defs = do
  cfg <- Reader.ask
  let moduleName = cfgModuleName cfg
  case parseRelFile [i|#{moduleName}.hs|] of
    Nothing -> error [i|invalid file name: #{moduleName}|]
    Just filename -> lift $ withFile filename $ do
      runReaderT (producePreamble moduleName) cfg
      for_ defs $ \def -> do
        let tyname = defName def
        let typesModuleName = maybe
              tyname
              (\prefix -> [i|#{prefix}.#{tyname}|])
              (cfgTypesModulePrefix cfg)
        produce [i|import #{typesModuleName}\n|]
      for_ defs $ \def -> runReaderT (produceDataDef' (defBody def)) cfg
      mark "manual section"

p :: ByteString -> ReaderT Config FileM ()
p str = do
  lift (produce str)
  lift (produce "\n")

{-# LANGUAGE OverloadedStrings #-}

-- |
-- Description: Functions for defining Purescript information.

module Data.Capperloin.Defs.Purescript
  ( pstype
  , pstype_
  , purescriptNS
  , purescriptContentNS
  )
where

import Data.Capperloin.Defs ( Namespace, DefM, TypeInfo(..), node, defcapperloin )
import GHC.Stack ( HasCallStack )

purescriptNS :: Namespace
purescriptNS = "purescript"

purescriptContentNS :: Namespace
purescriptContentNS = "purescript-content"

pstype :: HasCallStack => TypeInfo -> DefM -> DefM
pstype ty body = defcapperloin $
  node purescriptNS "type" $
    case ty of
      FlatType x ->
        node purescriptContentNS x $
          body
      MaybeType x ->
        node purescriptNS "maybe" $
          node purescriptContentNS x $
            body

pstype_ :: HasCallStack => TypeInfo -> DefM
pstype_ ty = defcapperloin $
  pstype ty $ pure ()

-- So what do we actually need in order to generate our Purescript types?
-- We already have information about what the types are supposed to be
-- called, what their constructors are, what their fields are named, etc.
-- So really, all we'd need is the Purescript types for them.

-- It might be somewhat useful to turn Sums into anonymous records with
-- actual field names and stuff. And we could add information about that,
-- I suppose. But it's... probably not necessary? For now I guess we just don't
-- bother.

-- I wonder if something like this would actually be better handled through
-- Generics. But y'know, that's actually exactly what we're talking about,
-- with Generics having its limitations. It's hard to annotate individual
-- *fields* with extra information about metadata in a way that Generics
-- will pick up.

-- What if... we had Haskell code act as the tree structure? Like, normal
-- Haskell definitions. That would remove a lot of the necessity of even
-- matching on things.

-- Those datatypes would only be used for the purpose of providing structural
-- information. Then Generic-based typeclass instances defined on that
-- would give functions that produce output. Maybe they even output Generics-based
-- structural output, which then gets shoved into a file? Well, it wouldn't
-- be able to represent things like functions. So it's definitely... you'd
-- have to think about it, you'd have to decide whether the extra bit of
-- tightness would be worth the constant need to deal with limitations.

{-
data Something = Something
  { field1 :: WithMetadata '[PurescriptName "...", PGColumn "name" "type"] Int
  , field2 :: WithMetadata '[] Bool
  }

but then we can't keep the field-level metadata and the type-level metadata
in the same place! and we don't even have ways to do things like annotate
just one field in a single type, etc. it's nice for when it works, but it
seems like it often won't be flexible enough to do what we want?

in some sense, the Rust way of doing this, with user-defined metadata tags
allowed pretty much anywhere, is actually very nice. i mean, we assume that
you won't need metadata on metadata, so just allow metadata on each structural
element, and you get pretty damn close to whatever you need...

but in our case, we do actually allow that kind of metadata on metadata
if people need it, by simply adding more tags underneath existing tags. at
the cost of increasing the parsing necessity to get what you need.
-}

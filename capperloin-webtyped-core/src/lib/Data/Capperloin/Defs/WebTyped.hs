{-# LANGUAGE OverloadedStrings #-}

-- | @capperloin-webtyped@ encompasses a number of producers that build around
-- | the idea of "web-typed" data:
-- |
-- | * All types automatically get an ID field included.
-- | * Anonymous fields are not allowed; sum variants must contain a record type
-- |   with named fields, anonymous products are not supported.
-- | * Types can be marked as containing "timestamps," for @createdAt@ and
-- |   @updatedAt@.
-- | * We do not output types with prefixed field names; this is mostly only
-- |   relevant for the Haskell side of things. In any case, it makes the
-- |   interop with Javascript easier.
-- | * All types get an extra type defined for creation and updates. So if you
-- |   defined a type @Foo@, you'd also get a type called @FooCU@, which
-- |   would contain all the same fields as @Foo@, minus the ID field (and
-- |   timestamps, if applicable). Sum types have the @CU@ type contain the
-- |   branches, with the normal type name being a record containing the
-- |   ID/timestamps.
-- |
-- | Basically "web-typed" data handles a bunch of boilerplate that we would
-- | want anyways when building out a real web application.
-- |
-- | As another restriction, we always output individual files for each type.

module Data.Capperloin.Defs.WebTyped
  ( timestamps
  , timestamps_
  , webtypedNS
  )
where

import Data.Capperloin.Defs

webtypedNS :: Namespace
webtypedNS = "webtyped"

timestamps :: HasCallStack => DefM -> DefM
timestamps body = defcapperloin $
  node webtypedNS "timestamps" $ do
    body

-- |
-- Whether to include a @createdAt@ and @updatedAt@ field in the types.
timestamps_ :: HasCallStack => DefM
timestamps_ = defcapperloin $
  timestamps $ pure ()

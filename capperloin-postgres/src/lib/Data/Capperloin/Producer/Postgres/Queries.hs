{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE QuasiQuotes         #-}
{-# LANGUAGE ScopedTypeVariables #-}

-- |
-- Output Hasql queries for basic CRUD operations.
--
-- Note that with the way we encode sums, we're depending on Aeson version
-- >= 1.0 < 2.0.

module Data.Capperloin.Producer.Postgres.Queries where

import Control.Monad ( when )
import Control.Monad.Trans.Class ( lift )
import Control.Monad.Trans.Reader ( ReaderT, runReaderT )
import Data.ByteString ( ByteString )
import Data.Capperloin.Defs ( Identifier, SrcLoc, TypeInfo(..) )
import Data.Capperloin.Producer ( Producer(..), ProducerM, FileM, relfile, version, withFile, produce, mark, errorAt )
import Data.Capperloin.Producer.Postgres.Matcher
  ( AnonymousField(..)
  , NamedField(..)
  , SumVariant(..)
  , SumDef(..)
  , AnonymousProductDef(..)
  , NamedProductDef(..)
  , DataDef'(..)
  , DataDef(..)
  , matchDataDefs
  )
import Data.Foldable ( for_ )
import Data.Traversable ( for )
import Data.String.Interpolate ( i, __i, __i'L, iii )
import Data.Text ( Text )
import Path ( parseRelFile )

import qualified Control.Monad.Trans.Reader as Reader
import qualified Data.Char as Char
import qualified Data.Text as Text

-- TODO: This module doesn't support any kind of typevars on datatypes yet.
-- In order to fix that, we'll likely need to modify Capperloin such that
-- it's even possible to distinguish between normal types and type variables.

-- TODO: We're using an ugly hack where we assume that if our config is
-- output individually, our types are also output individually. We need to
-- change that so that we can be more flexible with our imports.

data Config = Config
  { -- |
    -- Whether to output each type's queries into a separate file. If
    -- disabled, a single file called @Queries.hs@ containing all queries
    -- will be produced.
    cfgOutputIndividualFiles :: Bool
  , -- |
    -- Whether to prefix named record fields with the name of the type.
    cfgOutputPrefixedFields :: Bool
  , -- |
    -- The prefix of your modules, like @Data.MyTypes.Queries@. Don't add a trailing
    -- period.
    cfgModulePrefix :: Maybe Text
  , -- |
    -- The prefix of your types modules.
    cfgTypesModulePrefix :: Maybe Text
  }
  deriving Show

defaultConfig :: Config
defaultConfig = Config
  { cfgOutputIndividualFiles = True
  , cfgOutputPrefixedFields = True
  , cfgModulePrefix = Nothing
  , cfgTypesModulePrefix = Nothing
  }

producer :: Config -> Producer
producer cfg =
  Producer
    { prodName = "capperloin-postgres"
    , prodVersion = version 0 1 0 [] []
    , prodMatcher = matchDataDefs
    , prodCommentF = ("-- " <>)
    , prodBody = flip runReaderT cfg . produceDataDefs
    }

-- |
-- Output the boring module header.
producePreamble :: Text -> Text -> ReaderT Config FileM ()
producePreamble fname tyname = do
  p [__i'L|
      {-\# OPTIONS -fno-warn-unused-imports \#-}
      {-\# LANGUAGE QuasiQuotes             \#-}
      {-\# LANGUAGE OverloadedStrings       \#-}
      {-\# LANGUAGE OverloadedLabels        \#-}
    |]
  lift $ mark "module pragmas"

  moduleName <- maybe fname (\prefix -> [i|#{prefix}.#{fname}|])
    <$> Reader.asks cfgModulePrefix
  p [__i'L|
      module #{moduleName} where
    |]

  typesModuleName <- maybe tyname (\prefix -> [i|#{prefix}.#{tyname}|])
    <$> Reader.asks cfgTypesModulePrefix
  p [__i'L|
      import Data.Aeson ( FromJSON, Value, withObject, (.:) )
      import Data.Aeson.Types ( parseEither )
      import Data.Text ( Text )
      import Data.Functor.Contravariant ( (>$<) )
      import Data.String ( fromString )
      import Data.String.Interpolate ( __i )
      import Data.UUID ( UUID )
      import Hasql.Session ( Session, statement )
      import Hasql.Statement ( Statement(..) )
      import Lens.Micro.Extras ( view )
      import Data.Generics.Labels ()
      import #{typesModuleName}
      import qualified Data.Bifunctor as Bifunctor
      import qualified Data.Text as Text
      import qualified Hasql.Decoders as Decode
      import qualified Hasql.Encoders as Encode
    |]
  lift $ mark "imports"

produceSum :: SumDef -> ReaderT Config FileM ()
produceSum def = do
  produceCreate
  produceCreateWithID
  produceRead
  produceUpdate
  produceDelete
  produceDecoder
  where
    produceCreate :: ReaderT Config FileM ()
    produceCreate = do
      cases <- sumCases
      p [__i|
          create#{sumName def} :: #{cutyname} -> Session #{tyname}
          create#{sumName def} create = do
            rowID <- case create of { #{cases} }
            read#{sumName def} rowID
        |]
      where
        sumCases :: ReaderT Config FileM Text
        sumCases = do
          cases <- for (sumVariants def) $ \variant -> do
            let
              tagCol = Text.toLower $ variantName variant
              numFields = length $ variantFields variant
              variantArgs = Text.intercalate " " $ fmap anonFieldColumnName $ variantFields variant
              variantArgsTuple :: Text = [i|(#{Text.intercalate ", " $ fmap anonFieldColumnName $ variantFields variant})|]
              sqlArgs = Text.intercalate ", " $ fmap (\(n, f) -> [i|$#{n :: Int} AS \\"#{anonFieldColumnName f}\\"|]) $ zip [1..] $ variantFields variant
              qry :: Text = if sumIncludeTimestamps def
                then
                  [iii|
                    "WITH base_entity AS
                      (INSERT INTO #{sumTableName def}
                       (id, \\"#{tagCol}\\", created_at, updated_at)
                       VALUES (DEFAULT, '()', DEFAULT, DEFAULT)
                       RETURNING id)
                     INSERT INTO #{variantTableName variant}
                     SELECT base_entity.id, '()' AS __tag__, #{sqlArgs} FROM base_entity
                     RETURNING #{variantTableName variant}.id;"
                  |]
                else
                  [iii|
                    "WITH base_entity AS
                      (INSERT INTO #{sumTableName def}
                       (id, \\"#{tagCol}\\")
                       VALUES (DEFAULT, '()')
                       RETURNING id)
                     INSERT INTO #{variantTableName variant}
                     SELECT base_entity.id, '()' AS __tag__, #{sqlArgs} FROM base_entity
                     RETURNING #{variantTableName variant}.id;"
                  |]
              decoder :: Text = "Decode.column (Decode.nonNullable Decode.uuid)"
            encoder <- fmap (Text.intercalate " <> ") $ do
              for (zip [1..] $ variantFields variant) $ \(n, field) -> do
                encoder <- hasqlEncoder (anonFieldColumnType field) (anonFieldDefLoc field)
                pure $! [i|((#{indexedTupleFn numFields n}) >$< Encode.param (#{encoder}))|]

            pure $!
              [iii|
                #{variantName variant} #{variantArgs} ->
                  statement #{variantArgsTuple} $ Statement
                    #{qry}
                    (#{encoder})
                    (Decode.singleRow $ #{decoder})
                    True
              |]

          pure $! Text.intercalate "; " cases

    produceCreateWithID :: ReaderT Config FileM ()
    produceCreateWithID = do
      cases <- sumCases
      p [__i|
          create#{sumName def}WithID :: UUID -> #{cutyname} -> Session #{tyname}
          create#{sumName def}WithID rowID create = do
            case create of { #{cases} }
            read#{sumName def} rowID
        |]
      where
        sumCases :: ReaderT Config FileM Text
        sumCases = do
          cases <- for (sumVariants def) $ \variant -> do
            let
              tagCol = Text.toLower $ variantName variant
              numFields = length $ variantFields variant
              variantArgs = Text.intercalate " " $ fmap anonFieldColumnName $ variantFields variant
              variantArgsTuple :: Text = [i|(#{Text.intercalate ", " $ fmap anonFieldColumnName $ variantFields variant})|]
              sqlArgs = Text.intercalate ", " $ fmap (\(n, f) -> [i|$#{n :: Int} AS \\"#{anonFieldColumnName f}\\"|]) $ zip [2..] $ variantFields variant
              qry :: Text = if sumIncludeTimestamps def
                then
                  [iii|
                    "WITH base_entity AS
                      (INSERT INTO #{sumTableName def}
                       (id, \\"#{tagCol}\\", created_at, updated_at)
                       VALUES ($1, '()', DEFAULT, DEFAULT)
                       RETURNING id)
                     INSERT INTO #{variantTableName variant}
                     SELECT base_entity.id, '()' AS __tag__, #{sqlArgs} FROM base_entity;"
                  |]
                else
                  [iii|
                    "WITH base_entity AS
                      (INSERT INTO #{sumTableName def}
                       (id, \\"#{tagCol}\\")
                       VALUES ($1, '()')
                       RETURNING id)
                     INSERT INTO #{variantTableName variant}
                     SELECT base_entity.id, '()' AS __tag__, #{sqlArgs} FROM base_entity;"
                  |]
            encoder <- fmap (Text.intercalate " <> ") $ do
              for (zip [1..] $ variantFields variant) $ \(n, field) -> do
                encoder <- hasqlEncoder (anonFieldColumnType field) (anonFieldDefLoc field)
                pure $! [i|((#{indexedTupleFn numFields n}) >$< Encode.param (#{encoder}))|]

            pure $!
              [iii|
                #{variantName variant} #{variantArgs} ->
                  statement (rowID, #{variantArgsTuple}) $ Statement
                    #{qry}
                    (((\\(x, _) -> x) >$< Encode.param (Encode.nonNullable Encode.uuid))
                       <>
                     ((\\(_, x) -> x) >$< (#{encoder})))
                    Decode.noResult
                    True
              |]

          pure $! Text.intercalate "; " cases

    produceRead :: ReaderT Config FileM ()
    produceRead = p
      [__i|
        read#{sumName def} :: UUID -> Session #{tyname}
        read#{sumName def} rowID =
          statement rowID $ Statement
            "SELECT #{sumTableName def}.id, #{sqlCase} AS \\"payload\\" #{timestampFields} FROM #{sumTableName def} #{sqlJoins} WHERE #{sumTableName def}.id = $1;"
            (Encode.param $ Encode.nonNullable Encode.uuid)
            (Decode.singleRow decode#{tyname})
            True
      |]
      where
        timestampFields :: Text
        timestampFields = if sumIncludeTimestamps def
          then [i|, #{sumTableName def}.created_at, #{sumTableName def}.updated_at|]
          else ""

        sqlCase :: Text
        sqlCase =
          let branches = Text.intercalate " " $ flip fmap (sumVariants def) $ \variant ->
                let tagCol = Text.toLower $ variantName variant
                in [i|WHEN #{sumTableName def}.\\"#{tagCol}\\" IS NOT NULL THEN row('#{variantTableName variant}', to_jsonb(row(#{variantTableName variant}.*) :: #{variantTableName variant}))|]
          in [i|CASE #{branches} END|]

        sqlJoins :: Text
        sqlJoins =
          Text.intercalate " " $ flip fmap (sumVariants def) $ \variant ->
            let tagCol = Text.toLower $ variantName variant
            in [i|LEFT JOIN #{variantTableName variant} ON #{sumTableName def}.id = #{variantTableName variant}.id AND #{sumTableName def}.\\"#{tagCol}\\" = #{variantTableName variant}.__tag__|]

    produceUpdate :: ReaderT Config FileM ()
    produceUpdate = do
      cases <- sumCases
      p [__i|
          update#{sumName def} :: UUID -> #{cutyname} -> Session #{tyname}
          update#{sumName def} rowID update = case update of
            { #{cases} }
        |]
      where
        sumCases :: ReaderT Config FileM Text
        sumCases = do
          cases <- for (sumVariants def) $ \variant -> do
            let
              numFields = length $ variantFields variant
              variantArgs = Text.intercalate " " $ fmap anonFieldColumnName $ variantFields variant
              variantArgsTuple :: Text = [i|(#{Text.intercalate ", " $ fmap anonFieldColumnName $ variantFields variant})|]
              sqlUpdates = Text.intercalate ", " $ flip fmap (zip [2..] $ variantFields variant) $ \(n, field) ->
                [i|\\"#{anonFieldColumnName field}\\" = $#{n :: Int}|]
              sqlReturnArgs = Text.intercalate ", " $ flip fmap (variantFields variant) $ \field ->
                [i|#{variantTableName variant}.\\"#{anonFieldColumnName field}\\"|]
              qry :: Text = if sumIncludeTimestamps def
                then
                  [iii|
                    "WITH timestamp AS
                      (UPDATE #{sumTableName def}
                          SET updated_at = DEFAULT
                        WHERE #{sumTableName def}.id = $1
                       RETURNING id, created_at, updated_at)
                     UPDATE #{variantTableName variant}
                        SET #{sqlUpdates}
                       FROM timestamp
                      WHERE #{variantTableName variant}.id = $1
                        AND timestamp.id = #{variantTableName variant}.id
                     RETURNING #{variantTableName variant}.id, #{sqlReturnArgs}, timestamp.created_at, timestamp.updated_at;"
                  |]
                else
                  [iii|
                    "UPDATE #{variantTableName variant}
                     SET #{sqlUpdates}
                     WHERE #{variantTableName variant}.id = $1
                     RETURNING #{variantTableName variant}.id, #{sqlReturnArgs};"
                  |]
            encoder <- fmap (Text.intercalate " <> ") $ do
              for (zip [1..] $ variantFields variant) $ \(n, field) -> do
                encoder <- hasqlEncoder (anonFieldColumnType field) (anonFieldDefLoc field)
                pure $! [i|((#{indexedTupleFn numFields n}) >$< Encode.param (#{encoder}))|]
            decoder :: Text <- do
              colDecoders <- fmap (Text.intercalate " <*> ") $
                for (variantFields variant) $ \field -> do
                  decoder <- hasqlDecoder (anonFieldColumnType field) (anonFieldDefLoc field)
                  pure $! [i|Decode.column (#{decoder})|]
              pure $!
                if sumIncludeTimestamps def
                  then
                    [iii|
                      #{sumName def}
                        <$> Decode.column (Decode.nonNullable Decode.uuid)
                        <*> (#{variantName variant} <$> #{colDecoders})
                        <*> Decode.column (Decode.nonNullable Decode.timestamptz)
                        <*> Decode.column (Decode.nonNullable Decode.timestamptz)
                    |]
                  else
                    [iii|
                      #{sumName def}
                        <$> Decode.column (Decode.nonNullable Decode.uuid)
                        <*> (#{variantName variant} <$> #{colDecoders})
                    |]

            pure $!
              [iii|
                #{variantName variant} #{variantArgs} ->
                  statement (rowID, #{variantArgsTuple}) $ Statement
                    #{qry}
                    ((fst >$< Encode.param (Encode.nonNullable Encode.uuid)) <> (snd >$< (#{encoder})))
                    (Decode.singleRow $ #{decoder})
                    True
              |]

          pure $! Text.intercalate "; " cases

    produceDelete :: ReaderT Config FileM ()
    produceDelete = p
      -- DELETE CASCADE handles deleting from the subtables
      [__i|
        delete#{sumName def} :: UUID -> Session ()
        delete#{sumName def} rowID =
          statement rowID $ Statement
            "DELETE FROM #{sumTableName def} WHERE id = $1;"
            (Encode.param $ Encode.nonNullable Encode.uuid)
            Decode.noResult
            True
      |]

    -- It's not possible (or at least, desirable) to produce a generalized
    -- encoder for a sum type, since it would produce a variable number of
    -- parameters based on the branch of the sum.
    produceDecoder :: ReaderT Config FileM ()
    produceDecoder = do
      p [__i|
          decode#{tyname} :: Decode.Row #{tyname}
          decode#{tyname} = do
            rowID <- Decode.column $ Decode.nonNullable Decode.uuid
            sumBody <- Decode.column $ Decode.nonNullable $ Decode.refine extractSum $
              Decode.composite $ (,)
                <$> Decode.field (Decode.nonNullable Decode.text)
                <*> Decode.field (Decode.nonNullable Decode.jsonb)
        |]
      when (sumIncludeTimestamps def) $ do
        p [i|  createdAt <- Decode.column (Decode.nonNullable Decode.timestamptz)|]
        p [i|  updatedAt <- Decode.column (Decode.nonNullable Decode.timestamptz)|]
      if sumIncludeTimestamps def
        then p [i|  pure $! #{tyname} rowID sumBody createdAt updatedAt|]
        else p [i|  pure $! #{tyname} rowID sumBody|]
      p [i|  where|]
      p [i|    extractSum :: (Text, Value) -> Either Text #{cutyname}|]
      p [i|    extractSum (tag, payload) = case tag of { #{cases}; other -> Left ("unknown sum variant: " <> other) }|]
      p ""
      p [i|    extract :: FromJSON a => Value -> Text -> Either Text a|]
      p [i|    extract json key = Bifunctor.first Text.pack $ flip parseEither json $|]
      p [i|      withObject "<object>" $ \\obj ->|]
      p [i|        obj .: key|]
      where
        cases :: Text
        cases = Text.intercalate "; " $ flip fmap (sumVariants def) $ \variant ->
          let
            extractions :: Text = flip foldMap (variantFields variant) $ \field ->
              [i| <*> extract payload "#{anonFieldColumnName field}"|]
          in [i|"#{variantTableName variant}" -> pure #{variantName variant}#{extractions}|]

    tyname :: Identifier
    tyname = sumName def

    cutyname :: Identifier
    cutyname = [i|#{sumName def}CU|]

produceAnonProd :: AnonymousProductDef -> ReaderT Config FileM ()
produceAnonProd def = do
  produceCreate
  produceCreateWithID
  produceRead
  produceUpdate
  produceDelete
  produceCodec
  where
    produceCreate :: ReaderT Config FileM ()
    produceCreate = do
      let
        fieldNames = Text.intercalate ", " $
          fmap (\f -> [i|\\"#{anonFieldColumnName f}\\"|]) (anonProdFields def)
            <>
          if anonProdIncludeTimestamps def
            then ["created_at", "updated_at"]
            else []
        fieldParams = Text.intercalate ", " $
          fmap (\(n, _) -> [i|$#{n :: Int}|]) (zip [1..] $ anonProdFields def)
            <>
          if anonProdIncludeTimestamps def
            then ["DEFAULT", "DEFAULT"]
            else []
      p [__i|
          create#{anonProdName def} :: #{cutyname} -> Session #{tyname}
          create#{anonProdName def} create =
            statement create $ Statement
              "INSERT INTO #{anonProdTableName def} (id, #{fieldNames}) VALUES (DEFAULT, #{fieldParams}) RETURNING id, #{fieldNames};"
              encode#{cutyname}
              (Decode.singleRow decode#{tyname})
              True
        |]

    produceCreateWithID :: ReaderT Config FileM ()
    produceCreateWithID = do
      let
        fieldNames = Text.intercalate ", " $
          fmap (\f -> [i|\\"#{anonFieldColumnName f}\\"|]) (anonProdFields def)
            <>
          if anonProdIncludeTimestamps def
            then ["created_at", "updated_at"]
            else []
        fieldParams = Text.intercalate ", " $
          fmap (\(n, _) -> [i|$#{n :: Int}|]) (zip [2..] $ anonProdFields def)
            <>
          if anonProdIncludeTimestamps def
            then ["DEFAULT", "DEFAULT"]
            else []
      p [__i|
          create#{anonProdName def}WithID :: UUID -> #{cutyname} -> Session #{tyname}
          create#{anonProdName def}WithID rowID create =
            statement (rowID, create) $ Statement
              "INSERT INTO #{anonProdTableName def} (id, #{fieldNames}) VALUES ($1, #{fieldParams}) RETURNING id, #{fieldNames};"
              (((\\(x, _) -> x) >$< Encode.param (Encode.nonNullable Encode.uuid))
                <>
               ((\\(_, x) -> x) >$< encode#{cutyname}))
              (Decode.singleRow decode#{tyname})
              True
        |]

    produceRead :: ReaderT Config FileM ()
    produceRead = do
      let fieldNames = Text.intercalate ", " $
            fmap (\f -> [i|\\"#{anonFieldColumnName f}\\"|]) (anonProdFields def)
              <>
            if anonProdIncludeTimestamps def
              then ["created_at", "updated_at"]
              else []
      p [__i|
          read#{anonProdName def} :: UUID -> Session #{tyname}
          read#{anonProdName def} rowID =
            statement rowID $ Statement
              "SELECT id, #{fieldNames} FROM #{anonProdTableName def} WHERE id = $1;"
              (Encode.param $ Encode.nonNullable Encode.uuid)
              (Decode.singleRow decode#{tyname})
              True
        |]

    produceUpdate :: ReaderT Config FileM ()
    produceUpdate = do
      let
        fieldNames = Text.intercalate ", " $
          fmap (\f -> [i|\\"#{anonFieldColumnName f}\\"|]) (anonProdFields def)
            <>
          if anonProdIncludeTimestamps def
            then ["created_at", "updated_at"]
            else []
        fieldUpdates = Text.intercalate ", " $
          fmap (\(n, f) -> [i|\\"#{anonFieldColumnName f}\\" = $#{n :: Int}|]) (zip [2..] $ anonProdFields def)
            <>
          if anonProdIncludeTimestamps def
            then ["updated_at = DEFAULT"]
            else []
      p [__i|
          update#{anonProdName def} :: UUID -> #{cutyname} -> Session #{tyname}
          update#{anonProdName def} rowID update =
            statement (rowID, update) $ Statement
              "UPDATE #{anonProdTableName def} SET #{fieldUpdates} WHERE id = $1 RETURNING id, #{fieldNames};"
              ((fst >$< Encode.param (Encode.nonNullable Encode.uuid)) <> (snd >$< encode#{cutyname}))
              (Decode.singleRow decode#{tyname})
              True
        |]

    produceDelete :: ReaderT Config FileM ()
    produceDelete = p
      [__i|
        delete#{anonProdName def} :: UUID -> Session ()
        delete#{anonProdName def} rowID =
          statement rowID $ Statement
            "DELETE FROM #{anonProdTableName def} WHERE id = $1;"
            (Encode.param $ Encode.nonNullable Encode.uuid)
            Decode.noResult
            True
      |]

    produceCodec :: ReaderT Config FileM ()
    produceCodec = do
      encoder <- encodeCUType
      decoder <- decodeType
      p [__i|
          encode#{cutyname} :: Encode.Params #{cutyname}
          encode#{cutyname} = #{encoder}

          decode#{tyname} :: Decode.Row #{tyname}
          decode#{tyname} = #{decoder}
        |]
      where
        encodeCUType :: ReaderT Config FileM Text
        encodeCUType = do
          let numFields = length (anonProdFields def)
          colEncoders <- for (zip [1..] $ anonProdFields def) $ \(n, field) -> do
            colEncoder <- hasqlEncoder (anonFieldColumnType field) (anonFieldDefLoc field)
            pure [i| <> ((#{indexedFn cutyname numFields n}) >$< Encode.param (#{colEncoder}))|]
          pure $! mconcat $ "mempty" : colEncoders

        decodeType :: ReaderT Config FileM Text
        decodeType = do
          let timestampFields =
                if anonProdIncludeTimestamps def
                  then [ " <*> Decode.column (Decode.nonNullable Decode.timestamptz)"
                       , " <*> Decode.column (Decode.nonNullable Decode.timestamptz)"
                       ]
                  else []
          colDecoders <- for (anonProdFields def) $ \field -> do
            colDecoder <- hasqlDecoder (anonFieldColumnType field) (anonFieldDefLoc field)
            pure [i| <*> Decode.column (#{colDecoder})|]
          pure $! mconcat $
            [i|pure #{tyname}|]
            : " <*> Decode.column (Decode.nonNullable Decode.uuid)"
            : colDecoders
            <> timestampFields

    tyname :: Identifier
    tyname = anonProdName def

    cutyname :: Identifier
    cutyname = [i|#{anonProdName def}CU|]

produceNamedProd :: NamedProductDef -> ReaderT Config FileM ()
produceNamedProd def = do
  produceCreate
  produceCreateWithID
  produceRead
  produceUpdate
  produceDelete
  produceCodec
  where
    produceCreate :: ReaderT Config FileM ()
    produceCreate = do
      let
        fieldNames = Text.intercalate ", " $
          fmap (\f -> [i|\\"#{fieldColumnName f}\\"|]) (namedProdFields def)
            <>
          if namedProdIncludeTimestamps def
            then ["created_at", "updated_at"]
            else []
        fieldParams = Text.intercalate ", " $
          fmap (\(n, _) -> [i|$#{n :: Int}|]) (zip [1..] $ namedProdFields def)
            <>
          if namedProdIncludeTimestamps def
            then ["DEFAULT", "DEFAULT"]
            else []
      p [__i|
          create#{namedProdName def} :: #{cutyname} -> Session #{tyname}
          create#{namedProdName def} create =
            statement create $ Statement
              "INSERT INTO #{namedProdTableName def} (id, #{fieldNames}) VALUES (DEFAULT, #{fieldParams}) RETURNING id, #{fieldNames};"
              encode#{cutyname}
              (Decode.singleRow decode#{tyname})
              True
        |]

    produceCreateWithID :: ReaderT Config FileM ()
    produceCreateWithID = do
      let
        fieldNames = Text.intercalate ", " $
          fmap (\f -> [i|\\"#{fieldColumnName f}\\"|]) (namedProdFields def)
            <>
          if namedProdIncludeTimestamps def
            then ["created_at", "updated_at"]
            else []
        fieldParams = Text.intercalate ", " $
          fmap (\(n, _) -> [i|$#{n :: Int}|]) (zip [2..] $ namedProdFields def)
            <>
          if namedProdIncludeTimestamps def
            then ["DEFAULT", "DEFAULT"]
            else []
      p [__i|
          create#{namedProdName def}WithID :: UUID -> #{cutyname} -> Session #{tyname}
          create#{namedProdName def}WithID rowID create =
            statement (rowID, create) $ Statement
              "INSERT INTO #{namedProdTableName def} (id, #{fieldNames}) VALUES ($1, #{fieldParams}) RETURNING id, #{fieldNames};"
              (((\\(x, _) -> x) >$< Encode.param (Encode.nonNullable Encode.uuid))
                <>
               ((\\(_, x) -> x) >$< encode#{cutyname}))
              (Decode.singleRow decode#{tyname})
              True
        |]

    produceRead :: ReaderT Config FileM ()
    produceRead = do
      let fieldNames = Text.intercalate ", " $
            fmap (\f -> [i|\\"#{fieldColumnName f}\\"|]) (namedProdFields def)
              <>
            if namedProdIncludeTimestamps def
              then ["created_at", "updated_at"]
              else []
      p [__i|
          read#{namedProdName def} :: UUID -> Session #{tyname}
          read#{namedProdName def} rowID =
            statement rowID $ Statement
              "SELECT id, #{fieldNames} FROM #{namedProdTableName def} WHERE id = $1;"
              (Encode.param $ Encode.nonNullable Encode.uuid)
              (Decode.singleRow decode#{tyname})
              True
        |]

    produceUpdate :: ReaderT Config FileM ()
    produceUpdate = do
      let
        fieldNames = Text.intercalate ", " $
          fmap (\f -> [i|\\"#{fieldColumnName f}\\"|]) (namedProdFields def)
            <>
          if namedProdIncludeTimestamps def
            then ["created_at", "updated_at"]
            else []
        fieldUpdates = Text.intercalate ", " $
          fmap (\(n, f) -> [i|\\"#{fieldColumnName f}\\" = $#{n :: Int}|]) (zip [2..] $ namedProdFields def)
            <>
          if namedProdIncludeTimestamps def
            then ["updated_at = DEFAULT"]
            else []
      p [__i|
          update#{namedProdName def} :: UUID -> #{cutyname} -> Session #{tyname}
          update#{namedProdName def} rowID update =
            statement (rowID, update) $ Statement
              "UPDATE #{namedProdTableName def} SET #{fieldUpdates} WHERE id = $1 RETURNING id, #{fieldNames};"
              ((fst >$< Encode.param (Encode.nonNullable Encode.uuid)) <> (snd >$< encode#{cutyname}))
              (Decode.singleRow decode#{tyname})
              True
        |]

    produceDelete :: ReaderT Config FileM ()
    produceDelete = p
      [__i|
        delete#{namedProdName def} :: UUID -> Session ()
        delete#{namedProdName def} rowID =
          statement rowID $ Statement
            "DELETE FROM #{namedProdTableName def} WHERE id = $1;"
            (Encode.param $ Encode.nonNullable Encode.uuid)
            Decode.noResult
            True
      |]

    produceCodec :: ReaderT Config FileM ()
    produceCodec = do
      encoder <- encodeCUType
      decoder <- decodeType
      p [__i|
          encode#{cutyname} :: Encode.Params #{cutyname}
          encode#{cutyname} = #{encoder}

          decode#{tyname} :: Decode.Row #{tyname}
          decode#{tyname} = #{decoder}
        |]
      where
        encodeCUType :: ReaderT Config FileM Text
        encodeCUType = do
          colEncoders <- for (namedProdFields def) $ \field -> do
            fname <- maybePrefixField (namedProdName def) field
            colEncoder <- hasqlEncoder (fieldColumnType field) (fieldDefLoc field)
            pure [i| <> (view \##{fname} >$< Encode.param (#{colEncoder}))|]
          pure $! mconcat $ "mempty" : colEncoders

        decodeType :: ReaderT Config FileM Text
        decodeType = do
          let timestampFields = if namedProdIncludeTimestamps def
                then [ " <*> Decode.column (Decode.nonNullable Decode.timestamptz)"
                     , " <*> Decode.column (Decode.nonNullable Decode.timestamptz)"
                     ]
                else []
          colDecoders <- for (namedProdFields def) $ \field -> do
            colDecoder <- hasqlDecoder (fieldColumnType field) (fieldDefLoc field)
            pure [i| <*> Decode.column (#{colDecoder})|]
          pure $! mconcat $
            [i|pure #{tyname}|]
            : " <*> Decode.column (Decode.nonNullable Decode.uuid)"
            : colDecoders
            <> timestampFields

    tyname :: Identifier
    tyname = namedProdName def

    cutyname :: Identifier
    cutyname = [i|#{namedProdName def}CU|]

produceDataDef' :: DataDef' -> ReaderT Config FileM ()
produceDataDef' def = do
  case def of
    Sum sum -> produceSum sum
    AnonymousProduct aprod -> produceAnonProd aprod
    NamedProduct nprod -> produceNamedProd nprod

produceDataDefs :: [DataDef] -> ReaderT Config ProducerM ()
produceDataDefs defs = do
  cfg <- Reader.ask
  if cfgOutputIndividualFiles cfg
    then for_ defs $ \(DataDef name body) ->
      lift $ case parseRelFile [i|#{name}.hs|] of
        Nothing -> error [i|invalid file name: #{name}|]
        Just filename -> withFile filename $ do
          runReaderT (producePreamble name name) cfg
          runReaderT (produceDataDef' body) cfg
          mark "manual section"
    else lift $ withFile [relfile|Queries.hs|] $ do
      runReaderT (producePreamble "Queries" "Types") cfg
      for_ defs $ \(DataDef _ body) -> do
        runReaderT (produceDataDef' body) cfg
        mark "manual section"

hasqlEncoder :: TypeInfo -> SrcLoc -> ReaderT Config FileM Text
hasqlEncoder ty loc = case ty of
  FlatType t -> (\e -> [i|(Encode.nonNullable #{e})|]) <$> flatnameEncoder t loc
  MaybeType t -> (\e -> [i|(Encode.nullable #{e})|]) <$> flatnameEncoder t loc
  where
    flatnameEncoder :: Text -> SrcLoc -> ReaderT Config FileM Text
    flatnameEncoder ty loc = case Text.toUpper ty of
      "BOOLEAN"          -> pure "Encode.bool"
      "INT2"             -> pure "Encode.int2"
      "INT4"             -> pure "Encode.int4"
      "INT8"             -> pure "Encode.int8"
      "FLOAT4"           -> pure "Encode.float4"
      "FLOAT8"           -> pure "Encode.float8"
      "DOUBLE PRECISION" -> pure "Encode.float8"
      "NUMERIC"          -> pure "Encode.numeric"
      "CHAR"             -> pure "Encode.char"
      "TEXT"             -> pure "Encode.text"
      "BYTEA"            -> pure "Encode.bytea"
      "DATE"             -> pure "Encode.date"
      "TIMESTAMP"        -> pure "Encode.timestamp"
      "TIMESTAMPTZ"      -> pure "Encode.timestamptz"
      "TIME"             -> pure "Encode.time"
      "TIMETZ"           -> pure "Encode.timetz"
      "INTERVAL"         -> pure "Encode.interval"
      "UUID"             -> pure "Encode.uuid"
      "INET"             -> pure "Encode.inet"
      "JSON"             -> pure "Encode.json"
      "JSONB"            -> pure "Encode.jsonb"
      _other             ->
        lift $ errorAt loc [i|unknown PG type: `#{ty}'|]

hasqlDecoder :: TypeInfo -> SrcLoc -> ReaderT Config FileM Text
hasqlDecoder ty loc = case ty of
  FlatType t -> (\d -> [i|(Decode.nonNullable #{d})|]) <$> flatnameDecoder t loc
  MaybeType t -> (\d -> [i|(Decode.nullable #{d})|]) <$> flatnameDecoder t loc
  where
    flatnameDecoder :: Text -> SrcLoc -> ReaderT Config FileM Text
    flatnameDecoder ty loc = case Text.toUpper ty of
      "BOOLEAN"          -> pure "Decode.bool"
      "INT2"             -> pure "Decode.int2"
      "INT4"             -> pure "Decode.int4"
      "INT8"             -> pure "Decode.int8"
      "FLOAT4"           -> pure "Decode.float4"
      "FLOAT8"           -> pure "Decode.float8"
      "DOUBLE PRECISION" -> pure "Decode.float8"
      "NUMERIC"          -> pure "Decode.numeric"
      "CHAR"             -> pure "Decode.char"
      "TEXT"             -> pure "Decode.text"
      "BYTEA"            -> pure "Decode.bytea"
      "DATE"             -> pure "Decode.date"
      "TIMESTAMP"        -> pure "Decode.timestamp"
      "TIMESTAMPTZ"      -> pure "Decode.timestamptz"
      "TIME"             -> pure "Decode.time"
      "TIMETZ"           -> pure "Decode.timetz"
      "INTERVAL"         -> pure "Decode.interval"
      "UUID"             -> pure "Decode.uuid"
      "INET"             -> pure "Decode.inet"
      "JSON"             -> pure "Decode.json"
      "JSONB"            -> pure "Decode.jsonb"
      _other             ->
        lift $ errorAt loc [i|unknown PG type: `#{ty}'|]

quasiquotedFragment :: Text -> Text -> Text
quasiquotedFragment quasiquoter body =
  mconcat ["[", quasiquoter, "|\n", body, "|]"]

-- |
-- Potentially prefix the given field, based on the `Config' value.
maybePrefixField :: Identifier -> NamedField -> ReaderT Config FileM Identifier
maybePrefixField typename field = do
  shouldPrefix <- Reader.asks cfgOutputPrefixedFields
  if not shouldPrefix
    then pure $ fieldName field
    else
      let mprefixed = do
            (tyHead, tyRest) <- Text.uncons typename
            (fieldHead, fieldRest) <- Text.uncons $ fieldName field
            pure [i|#{Char.toLower tyHead}#{tyRest}#{Char.toUpper fieldHead}#{fieldRest}|]
      in case mprefixed of
           Just prefixed ->
             pure prefixed
           Nothing ->
             lift $ errorAt (fieldDefLoc field) "Bad field definition. Is the field name empty?"

-- |
-- Function to produce the text of a function that pulls a specific field from
-- a record with anonymous fields.
indexedFn :: Identifier -> Int -> Int -> Text
indexedFn cutyname numParams n =
  let cuparams :: Text = foldMap
        (\i -> if i == n then " x" else " _")
        [1..numParams]
  in [i|\\(#{cutyname}#{cuparams}) -> x|]

-- |
-- Function to produce the text of a function that pulls a specific field from
-- a tuple.
indexedTupleFn :: Int -> Int -> Text
indexedTupleFn numParams n =
  let tupleFields :: Text = Text.intercalate ", " $ flip fmap [1..numParams] $
        \i -> if i == n then "x" else "_"
  in [i|\\(#{tupleFields}) -> x|]

p :: ByteString -> ReaderT Config FileM ()
p str = do
  lift (produce str)
  lift (produce "\n")

whenAsks :: Monad m => (cfg -> Bool) -> ReaderT cfg m () -> ReaderT cfg m ()
whenAsks pred body = do
  flag <- Reader.asks pred
  when flag body

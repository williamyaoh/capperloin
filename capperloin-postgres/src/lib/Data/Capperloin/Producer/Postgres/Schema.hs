{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE QuasiQuotes         #-}
{-# LANGUAGE ScopedTypeVariables #-}

-- |
-- Output SQL schemas for defined types.

module Data.Capperloin.Producer.Postgres.Schema where

import Control.Monad ( when )
import Data.ByteString ( ByteString )
import Data.Capperloin.Defs ( Identifier, TypeInfo(..) )
import Data.Capperloin.Producer ( Producer(..), ProducerM, FileM, relfile, version, withFile, produce, mark )
import Data.Capperloin.Producer.Postgres.Matcher
  ( AnonymousField(..)
  , NamedField(..)
  , SumVariant(..)
  , SumDef(..)
  , AnonymousProductDef(..)
  , NamedProductDef(..)
  , DataDef'(..)
  , DataDef(..)
  , matchDataDefs
  )
import Data.Foldable ( for_ )
import Data.Function ( (&) )
import Data.Functor ( (<&>) )
import Data.String.Interpolate ( i, __i, iii )
import Data.Text ( Text )

import qualified Data.Text as Text

-- TODO: This module doesn't support any kind of typevars on datatypes yet.
-- In order to fix that, we'll likely need to modify Capperloin such that
-- it's even possible to distinguish between normal types and type variables.

producer :: Producer
producer =
  Producer
    { prodName = "capperloin-postgres"
    , prodVersion = version 0 1 0 [] []
    , prodMatcher = matchDataDefs
    , prodCommentF = ("-- " <>)
    , prodBody = produceDataDefs
    }

-- |
-- Output the boring module header.
producePreamble :: FileM ()
producePreamble = do
  mark "beginning of file"
  p [__i|
      CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

      CREATE TYPE unit AS enum('()');
    |]

produceSum :: SumDef -> FileM ()
produceSum def = do
  -- Generate the base table, with a tag for each variant
  p [i|CREATE TABLE #{sumTableName def}|]
  p [i|  ( id UUID NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4()|]
  for_ (sumVariants def) $ \variant ->
    p [i|  , "#{tagCol variant}" unit NULL|]
  when (sumIncludeTimestamps def) $ do
    p [i|  , created_at TIMESTAMPTZ NOT NULL DEFAULT clock_timestamp()|]
    p [i|  , updated_at TIMESTAMPTZ NOT NULL DEFAULT clock_timestamp()|]
  p [i|  );|]

  -- Generate the individual variant tables
  for_ (sumVariants def) $ \variant -> do
    p [i|CREATE TABLE #{variantTableName variant}|]
    p [i|  ( id UUID NOT NULL|]
    p [i|  , __tag__ unit NOT NULL|]
    for_ (variantFields variant) $ \field -> do
      p [i|  , "#{anonFieldColumnName field}" #{outputColumn $ anonFieldColumnType field}|]
    p [i|  , PRIMARY KEY ( id, __tag__ )|]
    p [i|  );|]

  -- Create deferred FK constraints from the main table to the variant tables
  for_ (sumVariants def) $ \variant -> do
    p [i|ALTER TABLE #{sumTableName def}|]
    p [i|  ADD CONSTRAINT "#{sumTableName def}__#{tagCol variant}"|]
    p [i|      FOREIGN KEY (id, "#{tagCol variant}")|]
    p [i|      REFERENCES #{variantTableName variant} (id, __tag__)|]
    p [i|      DEFERRABLE INITIALLY DEFERRED;|]

  -- Create unique constraints on each (id, tag) pair to ensure fast lookups
  for_ (sumVariants def) $ \variant -> do
    p [i|ALTER TABLE #{sumTableName def}|]
    p [i|  ADD CONSTRAINT "#{sumTableName def}__unique_#{tagCol variant}"|]
    p [i|  UNIQUE (id, "#{tagCol variant}");|]

  -- Create a CHECK constraint to ensure that only one tag is set at a time on the main table
  p [i|ALTER TABLE #{sumTableName def}|]
  p [i|  ADD CONSTRAINT "#{sumTableName def}__exactly_one_tag"|]
  p [i|  CHECK #{tagChecks};|]

  -- Create non-deferred FK constraints from the variant tables to the main table
  for_ (sumVariants def) $ \variant -> do
    p [i|ALTER TABLE #{variantTableName variant}|]
    p [i|  ADD CONSTRAINT "#{variantTableName variant}__#{sumTableName def}"|]
    p [i|  FOREIGN KEY (id, __tag__)|]
    p [i|  REFERENCES #{sumTableName def} (id, "#{tagCol variant}")|]
    p [i|  ON DELETE CASCADE;|]
  where
    tagCol :: SumVariant -> Identifier
    tagCol = Text.toLower . variantName

    tagCheck :: (Identifier, [Identifier]) -> Text
    tagCheck (var, vars) = vars
      <&> (\var' -> if var == var' then [i|"#{var'}" IS NOT NULL|] else [i|"#{var'}" IS NULL|])
       &  Text.intercalate " AND "
       &  (\t -> [i|(#{t})|])

    tagChecks :: Text
    tagChecks =
      [ (tagCol variant, fmap tagCol $ sumVariants def) | variant <- sumVariants def ]
        <&> tagCheck
         &  Text.intercalate " OR "
         &  (\t -> [i|(#{t})|])

produceAnonProd :: AnonymousProductDef -> FileM ()
produceAnonProd def = p
  [iii|
    CREATE TABLE #{anonProdTableName def}
      ( id UUID NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(), #{sqlFields} );
  |]
  where
    sqlFields :: Text
    sqlFields =
      let timestampFields = if anonProdIncludeTimestamps def
            then [ "created_at TIMESTAMPTZ NOT NULL DEFAULT clock_timestamp()"
                 , "updated_at TIMESTAMPTZ NOT NULL DEFAULT clock_timestamp()"
                 ]
            else []
      in Text.intercalate ", " $ (<> timestampFields) $ flip fmap (anonProdFields def) $
            \field -> [i|"#{anonFieldColumnName field}" #{outputColumn $ anonFieldColumnType field}|]

produceNamedProd :: NamedProductDef -> FileM ()
produceNamedProd def = p
  [iii|
    CREATE TABLE #{namedProdTableName def}
      ( id UUID NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(), #{sqlFields} );
  |]
  where
    sqlFields :: Text
    sqlFields =
      let timestampFields = if namedProdIncludeTimestamps def
            then [ "created_at TIMESTAMPTZ NOT NULL DEFAULT clock_timestamp()"
                 , "updated_at TIMESTAMPTZ NOT NULL DEFAULT clock_timestamp()"
                 ]
            else []
      in Text.intercalate ", " $ (<> timestampFields) $ flip fmap (namedProdFields def) $
           \field -> [i|"#{fieldColumnName field}" #{outputColumn $ fieldColumnType field}|]

produceDataDef' :: DataDef' -> FileM ()
produceDataDef' def = do
  case def of
    Sum sum -> produceSum sum
    AnonymousProduct aprod -> produceAnonProd aprod
    NamedProduct nprod -> produceNamedProd nprod

produceDataDefs :: [DataDef] -> ProducerM ()
produceDataDefs defs = withFile [relfile|schema.sql|] $ do
  producePreamble
  for_ defs $ \(DataDef _ body) -> produceDataDef' body
  mark "end of file"

outputColumn :: TypeInfo -> Text
outputColumn (FlatType t) = [i|#{t} NOT NULL|]
outputColumn (MaybeType t) = [i|#{t} NULL|]

p :: ByteString -> FileM ()
p str = produce str *> produce "\n"

{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE QuasiQuotes         #-}
{-# LANGUAGE ScopedTypeVariables #-}

-- |
-- Basically a carbon copy of @Data.Capperloin.Producer.Haskell@, but with
-- some DB-specific extensions. In particular:
--
-- * Types get an extra "id" field added to them, of type UUID
-- * For easy @INSERT@ and @UPDATE@s, an extra type called @<Type>CU@ is
--   output which doesn't contain the "id" or timestamp fields. The CU stands
--   for the Create-Update of CRUD.

module Data.Capperloin.Producer.Postgres.Types where

import Control.Monad ( when )
import Control.Monad.Trans.Class ( lift )
import Control.Monad.Trans.Reader ( ReaderT, runReaderT )
import Data.ByteString ( ByteString )
import Data.Capperloin.Defs ( Identifier, TypeInfo(..) )
import Data.Capperloin.Producer ( Producer(..), ProducerM, FileM, relfile, version, withFile, produce, mark, errorAt )
import Data.Capperloin.Producer.Postgres.Matcher
  ( AnonymousField(..)
  , NamedField(..)
  , SumVariant(..)
  , SumDef(..)
  , AnonymousProductDef(..)
  , NamedProductDef(..)
  , DataDef'(..)
  , DataDef(..)
  , matchDataDefs
  )
import Data.Foldable ( for_ )
import Data.String.Interpolate ( i, __i'L )
import Data.Text ( Text )
import Path ( parseRelFile )

import qualified Control.Monad.Trans.Reader as Reader
import qualified Data.Char as Char
import qualified Data.Text as Text

data Config = Config
  { -- |
    -- Whether to output each type into a separate file. If disabled, a single
    -- file called @Types.hs@ containing all types will be produced.
    cfgOutputIndividualFiles :: Bool
  , -- |
    -- Whether to prefix named record fields with the name of the type.
    -- The intention when not using this is to enable @-XDuplicateRecordFields@
    -- and access fields using generic lenses, to make data access more succinct.
    -- Regardless of whether you're using this, don't prefix the field names in
    -- your Capperloin definitions.
    cfgOutputPrefixedFields :: Bool
  , -- |
    -- The prefix of your modules, like @Data.MyTypes@. Don't add a trailing
    -- period.
    cfgModulePrefix :: Maybe Text
  , -- |
    -- Any extra stuff to stick in the outputted module imports.
    cfgExtraImports :: Maybe Text
  }
  deriving Show

defaultConfig :: Config
defaultConfig = Config
  { cfgOutputIndividualFiles = True
  , cfgOutputPrefixedFields = True
  , cfgModulePrefix = Nothing
  , cfgExtraImports = Nothing
  }

producer :: Config -> Producer
producer cfg =
  Producer
    { prodName = "capperloin-postgres"
    , prodVersion = version 0 1 0 [] []
    , prodMatcher = matchDataDefs
    , prodCommentF = ("-- " <>)
    , prodBody = flip runReaderT cfg . produceDataDefs
    }

-- |
-- Output the boring module header.
producePreamble :: Text -> ReaderT Config FileM ()
producePreamble fname = do
  p [i|{-\# OPTIONS -fno-warn-unused-imports \#-}|]
  whenAsks cfgOutputIndividualFiles $ p
    [__i'L|
      {-\# LANGUAGE DeriveGeneric           \#-}
      {-\# LANGUAGE DuplicateRecordFields   \#-}
    |]
  lift $ mark "module pragmas"

  moduleName <- maybe fname (\prefix -> [i|#{prefix}.#{fname}|])
    <$> Reader.asks cfgModulePrefix
  p [__i'L|
      module #{moduleName} where
    |]

  p [i|import Data.UUID ( UUID )|]
  whenAsks cfgOutputIndividualFiles $ p
    [__i'L|
      import GHC.Generics ( Generic )
    |]

  mextraImports <- Reader.asks cfgExtraImports
  case mextraImports of
    Nothing -> pure ()
    Just extraImports -> p [i|#{extraImports}|]

  lift $ mark "imports"

produceSum :: SumDef -> ReaderT Config FileM ()
produceSum def = do
  let cuTypeName :: Text = [i|#{sumName def}CU|]

  -- Output normal type with ID
  let
    baseFields =
      [ NamedField "id" "UUID" "id" (FlatType "UUID") (sumSrcLoc def)
      , NamedField "body" [i|#{cuTypeName} #{Text.intercalate " " $ sumTypevars def}|] "body" (FlatType [i|#{cuTypeName} #{Text.intercalate " " $ sumTypevars def}|]) (sumSrcLoc def)
      ]
    fullFields = if sumIncludeTimestamps def
      then baseFields <>
           [ NamedField "createdAt" "UTCTime" "created_at" (FlatType "TIMESTAMPTZ") (sumSrcLoc def)
           , NamedField "updatedAt" "UTCTime" "updated_at" (FlatType "TIMESTAMPTZ") (sumSrcLoc def)
           ]
      else baseFields
  p [i|data #{sumName def} #{Text.intercalate " " $ sumTypevars def} =|]
  p [i|  #{sumName def}|]
  -- Technically we're not taking in the constructor name as part of the
  -- definition tree, which could cause collisions. Probably okay right now.
  outputFields (sumName def) fullFields
  whenAsks cfgOutputIndividualFiles $
    p [i|  deriving Generic|]

  -- Output CU type, which doubles as the actual sum to case on
  p [i|data #{cuTypeName} #{Text.intercalate " " $ sumTypevars def}|]
  case sumVariants def of
    [] -> pure ()
    (SumVariant varname varfields _:rest) -> do
      p [i|  = #{varname} #{Text.intercalate " " $ fmap (outputType . anonFieldType) varfields}|]
      for_ rest $ \(SumVariant varname varfields _) ->
        p [i|  | #{varname} #{Text.intercalate " " $ fmap (outputType . anonFieldType) varfields}|]
  whenAsks cfgOutputIndividualFiles $
    p [i|  deriving Generic|]

produceAnonProd :: AnonymousProductDef -> ReaderT Config FileM ()
produceAnonProd def = do
  let
    cuTypeName :: Text = [i|#{anonProdName def}CU|]
    cuTypeConstructor :: Text = [i|#{anonProdConstructor def}CU|]
    fullFields = if anonProdIncludeTimestamps def
      then fmap (outputType . anonFieldType) (anonProdFields def) <> ["UTCTime", "UTCTime"]
      else fmap (outputType . anonFieldType) (anonProdFields def)

  -- Output normal type with ID
  p [i|data #{anonProdName def} #{Text.intercalate " " $ anonProdTypevars def} =|]
  p [i|  #{anonProdConstructor def} UUID #{Text.intercalate " " fullFields}|]
  whenAsks cfgOutputIndividualFiles $
    p [i|  deriving Generic|]

  -- Output CU type
  p [i|data #{cuTypeName} #{Text.intercalate " " $ anonProdTypevars def} =|]
  p [i|  #{cuTypeConstructor} #{Text.intercalate " " $ fmap (outputType . anonFieldType) $ anonProdFields def}|]
  whenAsks cfgOutputIndividualFiles $
    p [i|  deriving Generic|]

produceNamedProd :: NamedProductDef -> ReaderT Config FileM ()
produceNamedProd def = do
  let
    cuTypeName :: Text = [i|#{namedProdName def}CU|]
    cuTypeConstructor :: Text = [i|#{namedProdConstructor def}CU|]

  -- Output normal type with ID
  let
    fullFields = if namedProdIncludeTimestamps def
      then NamedField "id" "UUID" "id" (FlatType "UUID") (namedProdSrcLoc def) :
           namedProdFields def <>
           [ NamedField "createdAt" "UTCTime" "created_at" (FlatType "TIMESTAMPTZ") (namedProdSrcLoc def)
           , NamedField "updatedAt" "UTCTime" "updated_at" (FlatType "TIMESTAMPTZ") (namedProdSrcLoc def)
           ]
      else NamedField "id" "UUID" "id" (FlatType "UUID") (namedProdSrcLoc def) : namedProdFields def
  p [i|data #{namedProdName def} #{Text.intercalate " " $ namedProdTypevars def} =|]
  p [i|  #{namedProdConstructor def}|]
  outputFields (namedProdName def) fullFields
  whenAsks cfgOutputIndividualFiles $
    p [i|  deriving Generic|]

  -- Output CU type
  p [i|data #{cuTypeName} #{Text.intercalate " " $ namedProdTypevars def} =|]
  p [i|  #{cuTypeConstructor}|]
  outputFields cuTypeName (namedProdFields def)
  whenAsks cfgOutputIndividualFiles $
    p [i|  deriving Generic|]

produceDataDef :: DataDef' -> ReaderT Config FileM ()
produceDataDef def = do
  case def of
    Sum sum -> produceSum sum
    AnonymousProduct aprod -> produceAnonProd aprod
    NamedProduct nprod -> produceNamedProd nprod

produceDataDefs :: [DataDef] -> ReaderT Config ProducerM ()
produceDataDefs defs = do
  cfg <- Reader.ask
  if cfgOutputIndividualFiles cfg
    then for_ defs $ \(DataDef name body) ->
      lift $ case parseRelFile [i|#{name}.hs|] of
        Nothing -> error [i|invalid file name: #{name}|]
        Just filename -> withFile filename $ do
          runReaderT (producePreamble name) cfg
          runReaderT (produceDataDef body) cfg
          mark "manual section"
    else lift $ withFile [relfile|Types.hs|] $ do
      runReaderT (producePreamble "Types") cfg
      for_ defs $ \(DataDef _ body) -> do
        runReaderT (produceDataDef body) cfg
        mark "manual section"

-- |
-- Produce the { .. } delimited list of fields for a record with named fields,
-- properly indented.
outputFields :: Identifier -> [NamedField] -> ReaderT Config FileM ()
outputFields typename fields = case fields of
  [] -> pure ()
  (field:fs) -> do
    fullname <- maybePrefixField typename field
    p [i|    { #{fullname} :: #{outputType $ fieldType field}|]
    for_ fs $ \field -> do
      fullname <- maybePrefixField typename field
      p [i|    , #{fullname} :: #{outputType $ fieldType field}|]
    p [i|    }|]

-- |
-- Potentially prefix the given field, based on the `Config' value.
maybePrefixField :: Identifier -> NamedField -> ReaderT Config FileM Identifier
maybePrefixField typename field = do
  shouldPrefix <- Reader.asks cfgOutputPrefixedFields
  if not shouldPrefix
    then pure $ fieldName field
    else
      let mprefixed = do
            (tyHead, tyRest) <- Text.uncons typename
            (fieldHead, fieldRest) <- Text.uncons $ fieldName field
            pure [i|#{Char.toLower tyHead}#{tyRest}#{Char.toUpper fieldHead}#{fieldRest}|]
      in case mprefixed of
           Just prefixed ->
             pure prefixed
           Nothing ->
             lift $ errorAt (fieldDefLoc field) "Bad field definition. Is the field name empty?"

outputType :: TypeInfo -> Text
outputType (FlatType t) = t
outputType (MaybeType t) = [i|(Maybe #{t})|]

p :: ByteString -> ReaderT Config FileM ()
p str = do
  lift (produce str)
  lift (produce "\n")

whenAsks :: Monad m => (cfg -> Bool) -> ReaderT cfg m () -> ReaderT cfg m ()
whenAsks pred body = do
  flag <- Reader.asks pred
  when flag body

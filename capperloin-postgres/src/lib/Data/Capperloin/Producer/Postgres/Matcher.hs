{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE OverloadedStrings #-}

-- |
-- The types and matchers for writing other producers in this package.

module Data.Capperloin.Producer.Postgres.Matcher where

import Data.Maybe ( isJust )
import Data.Capperloin.Defs ( Identifier, SrcLoc, TypeInfo(..), capperloinNS, haskellNS, haskellContentNS )
import Data.Capperloin.Defs.Postgres ( postgresNS, postgresContentNS )
import Data.Capperloin.Matcher
import Data.String.Interpolate ( i )

data AnonymousField = AnonymousField
  { anonFieldType :: TypeInfo
  , anonFieldColumnName :: Identifier
  , anonFieldColumnType :: TypeInfo
  , anonFieldDefLoc :: SrcLoc
  }

data NamedField = NamedField
  { fieldName :: Identifier
  , fieldType :: TypeInfo
  , fieldColumnName :: Identifier
  , fieldColumnType :: TypeInfo
  , fieldDefLoc :: SrcLoc
  }

data SumVariant = SumVariant
  { variantName :: Identifier
  , variantFields :: [AnonymousField]
  , variantTableName :: Identifier
  }

data SumDef = SumDef
  { sumName :: Identifier
  , sumTypevars :: [Identifier]
  , sumVariants :: [SumVariant]
  , sumTableName :: Identifier
  , sumIncludeTimestamps :: Bool
  , sumSrcLoc :: SrcLoc
  }

data AnonymousProductDef = AnonymousProductDef
  { anonProdName :: Identifier
  , anonProdTypevars :: [Identifier]
  , anonProdConstructor :: Identifier
  , anonProdFields :: [AnonymousField]
  , anonProdTableName :: Identifier
  , anonProdIncludeTimestamps :: Bool
  }

data NamedProductDef = NamedProductDef
  { namedProdName :: Identifier
  , namedProdTypevars :: [Identifier]
  , namedProdConstructor :: Identifier
  , namedProdFields :: [NamedField]
  , namedProdTableName :: Identifier
  , namedProdIncludeTimestamps :: Bool
  , namedProdSrcLoc :: SrcLoc
  }

data DataDef'
  = Sum SumDef
  | AnonymousProduct AnonymousProductDef
  | NamedProduct NamedProductDef

data DataDef = DataDef
  { defName :: Identifier
  , defBody :: DataDef'
  }

hs :: Identifier -> DefsMatcher a -> ChildMatcher a
hs = exact haskellNS

pg :: Identifier -> DefsMatcher a -> ChildMatcher a
pg = exact postgresNS

nmHS :: (Identifier -> DefsMatcher a) -> ChildMatcher a
nmHS = namedNS haskellNS

nmHSC :: (Identifier -> DefsMatcher a) -> ChildMatcher a
nmHSC = namedNS haskellContentNS

nmPG :: (Identifier -> DefsMatcher a) -> ChildMatcher a
nmPG = namedNS postgresNS

nmPGC :: (Identifier -> DefsMatcher a) -> ChildMatcher a
nmPGC = namedNS postgresContentNS

type_ :: (TypeInfo -> DefsMatcher a) -> ChildMatcher a
type_ body = choiceChild
  [ hs "field" $ exists $ nmHSC $ \typename ->
      body (FlatType typename)
  , hs "field" $ exists $ hs "maybe" $ exists $ nmHSC $ \typename ->
      body (MaybeType typename)
  ]

matchTable :: DefsMatcher Identifier
matchTable = exists $ pg "table" $
  exists $ nmPG $ \name -> pure name

matchColumn :: DefsMatcher (Identifier, TypeInfo)
matchColumn = exists $ pg "column" $ do
  colname <- exists $ pg "colname" $
    exists $ nmPG $ \colname -> pure colname
  coltype <- exists $ pg "coltype" $ exists $ choiceChild
    [ nmPGC $ \tyname -> pure (FlatType tyname)
    , pg "maybe" $ exists $ nmPGC $ \tyname -> pure (MaybeType tyname)
    ]
  pure $! (colname, coltype)

matchIncludeTimestamps :: DefsMatcher Bool
matchIncludeTimestamps = fmap isJust $ try $ exists $
  pg "timestamps" $ pure ()

matchSum :: (Identifier, [Identifier]) -> DefsMatcher SumDef
matchSum (name, typevars) = do
  sumLoc <- srcLoc
  tableName <- matchTable
  includeTimestamps <- matchIncludeTimestamps
  variants <- exists $ hs "variants" $
    forAll $ hs "variant" $
      exists $ nmHS $ \varname -> do
        varTable <- matchTable
        fields <- exists $ hs "fields" $
          forAll $ type_ $ \fieldTy -> do
            loc <- srcLoc
            (colname, coltype) <- matchColumn
            pure $! AnonymousField fieldTy colname coltype loc
        pure $! SumVariant varname fields varTable
  pure $! SumDef name typevars variants tableName includeTimestamps sumLoc

matchAnonProd :: (Identifier, [Identifier]) -> DefsMatcher AnonymousProductDef
matchAnonProd (name, typevars) =
  exists $ nmHS $ \constructor -> do
    tableName <- matchTable
    includeTimestamps <- matchIncludeTimestamps
    exists $ hs "fields" $ do
      fields <- forAll $ type_ $ \fieldTy -> do
        loc <- srcLoc
        (colname, coltype) <- matchColumn
        pure $! AnonymousField fieldTy colname coltype loc
      pure $! AnonymousProductDef name typevars constructor fields tableName includeTimestamps

matchNamedProd :: (Identifier, [Identifier]) -> DefsMatcher NamedProductDef
matchNamedProd (name, typevars) = do
  prodLoc <- srcLoc
  exists $ nmHS $ \constructor -> do
    tableName <- matchTable
    includeTimestamps <- matchIncludeTimestamps
    exists $ hs "fields" $ do
      fields <- forAll $ hs "named" $
        exists $ nmHS $ \fieldName ->
          exists $ type_ $ \fieldType -> do
            loc <- srcLoc
            (colname, coltype) <- matchColumn
            pure $! NamedField fieldName fieldType colname coltype loc
      pure $! NamedProductDef name typevars constructor fields tableName includeTimestamps prodLoc

matchDataDef :: Identifier -> DefsMatcher DataDef
matchDataDef name = do
  typevars <- exists $ hs "typevars" $
    forAll $ nmHS $ \typevar -> pure typevar
  exists $ hs "definition" $ do
    body <- exists $ nmHS $ \deftype -> case deftype of
      "sum"               -> Sum <$> matchSum (name, typevars)
      "anonymous-product" -> AnonymousProduct <$> matchAnonProd (name, typevars)
      "product"           -> NamedProduct <$> matchNamedProd (name, typevars)
      _                   -> fail [i|unknown definition type: `#{deftype}'|]
    pure $! DataDef name body

matchDataDefs :: DefsMatcher [DataDef]
matchDataDefs = forAll $ namedNS capperloinNS $ \typename ->
  matchDataDef typename

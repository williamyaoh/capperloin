{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

module Data.Capperloin.Matcher.WebTyped.Haskell
  ( DataDef(..)
  , DataDefBody(..)
  , ProductDef(..)
  , SumDef(..)
  , SumVariant(..)
  , Field(..)
  , matchDataDefs, matchDataDef
  )
where

import Data.Maybe ( isJust )
import Data.Capperloin.Defs ( Identifier, SrcLoc, TypeInfo(..), capperloinNS, haskellNS, haskellContentNS )
import Data.Capperloin.Defs.WebTyped ( webtypedNS )
import Data.Capperloin.Matcher
import Data.Sequence ( Seq )
import Data.String.Interpolate ( i )

import qualified Data.Sequence as Seq

data Field = Field
  { fieldName :: Identifier
  , fieldType :: TypeInfo
  , fieldLoc :: SrcLoc
  }

data SumVariant = SumVariant
  { variantName :: Identifier
  , variantFields :: Seq Field
  , variantLoc :: SrcLoc
  }

data SumDef = SumDef
  { sumName :: Identifier
  , sumTypevars :: Seq Identifier
  , sumVariants :: Seq SumVariant
  , sumIncludeTimestamps :: Bool
  , sumLoc :: SrcLoc
  }

data ProductDef = ProductDef
  { productName :: Identifier
  , productTypevars :: Seq Identifier
  , productConstructor :: Identifier
  , productFields :: Seq Field
  , productIncludeTimestamps :: Bool
  , productLoc :: SrcLoc
  }

data DataDefBody
  = Sum SumDef
  | Product ProductDef

data DataDef = DataDef
  { defName :: Identifier
  , defBody :: DataDefBody
  , defLoc :: SrcLoc
  }

hs :: Identifier -> DefsMatcher a -> ChildMatcher a
hs = exact haskellNS

wt :: Identifier -> DefsMatcher a -> ChildMatcher a
wt = exact webtypedNS

nmHS :: (Identifier -> DefsMatcher a) -> ChildMatcher a
nmHS = namedNS haskellNS

nmHSC :: (Identifier -> DefsMatcher a) -> ChildMatcher a
nmHSC = namedNS haskellContentNS

type_ :: (TypeInfo -> DefsMatcher a) -> ChildMatcher a
type_ body = choiceChild
  [ hs "field" $ exists $ nmHSC $ \typename ->
      body (FlatType typename)
  , hs "field" $ exists $ hs "maybe" $ exists $ nmHSC $ \typename ->
      body (MaybeType typename)
  ]

matchIncludeTimestamps :: DefsMatcher Bool
matchIncludeTimestamps = fmap isJust $ try $ exists $
  wt "timestamps" $ pure ()

matchSum :: (Identifier, Seq Identifier) -> DefsMatcher SumDef
matchSum (name, typevars) = do
  sumLoc <- srcLoc
  includeTimestamps <- matchIncludeTimestamps
  variants <- exists $ hs "variants" $
    forAll $ hs "variant" $
      exists $ nmHS $ \varname -> do
        varloc <- srcLoc
        fields <- exists $ hs "fields" $
          forAll $ hs "named" $
            exists $ nmHS $ \fieldName ->
              exists $ type_ $ \ty -> do
                loc <- srcLoc
                pure $! Field fieldName ty loc
        pure $! SumVariant varname (Seq.fromList fields) varloc
  pure $! SumDef name typevars (Seq.fromList variants) includeTimestamps sumLoc

matchProd :: (Identifier, Seq Identifier) -> DefsMatcher ProductDef
matchProd (name, typevars) = do
  prodLoc <- srcLoc
  exists $ nmHS $ \constructor -> do
    includeTimestamps <- matchIncludeTimestamps
    fields <- exists $ hs "fields" $ do
      forAll $ hs "named" $
        exists $ nmHS $ \fieldName ->
          exists $ type_ $ \ty -> do
            loc <- srcLoc
            pure $! Field fieldName ty loc
    pure $! ProductDef name typevars constructor (Seq.fromList fields) includeTimestamps prodLoc

matchDataDef :: Identifier -> DefsMatcher DataDef
matchDataDef name = do
  loc <- srcLoc
  typevars <- fmap Seq.fromList $ exists $ hs "typevars" $
    forAll $ nmHS $ \typevar -> pure typevar
  exists $ hs "definition" $ do
    body <- exists $ nmHS $ \deftype -> case deftype of
      "sum"               -> Sum <$> matchSum (name, typevars)
      "product"           -> Product <$> matchProd (name, typevars)
      _                   -> fail [i|unknown definition type: `#{deftype}'|]
    pure $! DataDef name body loc

matchDataDefs :: DefsMatcher (Seq DataDef)
matchDataDefs = fmap Seq.fromList $ forAll $ namedNS capperloinNS $ \typename ->
  matchDataDef typename

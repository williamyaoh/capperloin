{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE QuasiQuotes         #-}
{-# LANGUAGE ScopedTypeVariables #-}

-- | Given that these types are meant to be used in a /web/ application,
-- | clearly we need some kind of JSON representation. The representation we
-- | use is designed to ease the manipulation of the JSON data at the term
-- | level, rather than using tricks like dynamic field names based on sum
-- | variants or anything like that.
-- |
-- | * Record types are simply turned into JSON objects in the obvious way,
-- |   with field names kebab-cased.
-- | * Sum types become an object like @{ "tag": "...", "body": "..." }@,
-- |   where @tag@ is the constructor of the sum variant, kebab-cased. The @body@
-- |   is a record holding the Fields... fields. If the sum constructor is nullary,
-- |   there is no @body@ field.
-- |
-- | Compiling the output requires the @deriving-aeson@ library.

module Data.Capperloin.Producer.WebTyped.DerivingAeson
  ( Config(..)
  , defaultConfig
  , producer
  )
where

import Control.Monad ( unless )
import Control.Monad.Trans.Class ( lift )
import Control.Monad.Trans.Reader ( ReaderT, runReaderT )
import Data.ByteString ( ByteString )
import Data.Capperloin.Producer ( Producer(..), ProducerM, FileM, version, withFile, produce, mark )
import Data.Capperloin.Matcher.WebTyped.Haskell
import Data.Foldable ( for_ )
import Data.Sequence ( Seq )
import Data.String.Interpolate ( i, __i )
import Data.Text ( Text )
import Path ( parseRelFile )

import qualified Control.Monad.Trans.Reader as Reader
import qualified Data.Sequence as Seq

data Config = Config
  { -- |
    -- The name of your module that should contain the ToJSON/FromJSON instances.
    cfgModuleName :: Text
  , -- |
    -- The prefix of your module, like @Data.MyTypes@. Don't add a trailing
    -- period.
    cfgModulePrefix :: Maybe Text
  , -- |
    -- The prefix of your types module.
    cfgTypesModulePrefix :: Maybe Text
  , -- |
    -- Any extra stuff to stick in the outputted module imports.
    cfgExtraImports :: Maybe Text
  }
  deriving Show

defaultConfig :: Config
defaultConfig = Config
  { cfgModuleName = "Serialize"
  , cfgModulePrefix = Nothing
  , cfgTypesModulePrefix = Nothing
  , cfgExtraImports = Nothing
  }

producer :: Config -> Producer
producer cfg =
  Producer
    { prodName = "capperloin-webtyped-haskell"
    , prodVersion = version 0 1 0 [] []
    , prodMatcher = matchDataDefs
    , prodCommentF = ("-- " <>)
    , prodBody = flip runReaderT cfg . produceDataDefs
    }

-- @deriving-aeson@ is overall pretty good, but it has some unfortunate quirks
-- that are holdovers from the TH-based deriving in the Aeson library itself,
-- which @deriving-aeson@ depends on.
--
-- Specifically, the way it handles sums is extremely unfortunate. By default
-- it outputs a { "tag": "...", "contents": "..." } style object, but does lots
-- of ugly special casing. The @contents@ field is an array, but *only* if
-- there's more than one argument to the sum constructor. Otherwise, it gets
-- unwrapped! Seemingly reasonable, but incredibly annoying when having to
-- write a codec in a different language. Plus, if the constructor is nullary,
-- you don't get an object at all! Instead you just get a string of the
-- constructor name. Not to mention that anonymous products just get turned
-- into arrays? Rather than getting treated the same way as sums. Look, I get
-- that it makes things shorter, but in terms of both parseability and
-- debuggability, it makes things seriously annoying.
--
-- We use the following modifiers for sums and products:
--
-- * For products:
--     FieldLabelModifier '[CamelToKebab],
--     ConstructorTagModifier '[CamelToKebab]
-- * For sums:
--     FieldLabelModifier '[CamelToKebab],
--     ConstructorTagModifier '[CamelToKebab],
--     TagSingleConstructors,
--     NoAllNullaryToStringTag,
--     SumTaggedObject "tag" "body"
--
-- Other than the CamelToKebab modifiers, which do the obvious things, the rest
-- deserve some explanation. Records just get turned into a JSON object with its
-- fields. The modifiers for sums are primarily aimed at correcting those
-- inconsistencies with the default deriving. With these modifiers, all variants,
-- whether nullary or not, get turned into a tagged object. Unfortunately, we
-- can't override the array unwrapping behavior, so these modifiers technically
-- still change the body based on whether there are multiple arguments in the
-- constructor or not. But for web-typed data, we only ever have at most one
-- argument in a sum constructor, which is a record, so not wrapping that object
-- in an array is exactly what we want.

-- | Output the boring module header.
producePreamble :: Text -> ReaderT Config FileM ()
producePreamble fname = do
  moduleName <- maybe fname (\prefix -> [i|#{prefix}.#{fname}|])
    <$> Reader.asks cfgModulePrefix
  p [__i|
      {-\# OPTIONS -Wno-orphans             \#-}
      {-\# OPTIONS -fno-warn-unused-imports \#-}
      {-\# LANGUAGE DerivingStrategies      \#-}
      {-\# LANGUAGE DerivingVia             \#-}
      {-\# LANGUAGE StandaloneDeriving      \#-}
      {-\# LANGUAGE DataKinds               \#-}

      module #{moduleName} where

      import Deriving.Aeson
    |]

  mextraImports <- Reader.asks cfgExtraImports
  case mextraImports of
    Nothing -> pure ()
    Just extraImports -> p [i|#{extraImports}|]
  lift $ mark "imports"

produceModifiers :: ReaderT Config FileM ()
produceModifiers =
  p [__i|
      type RecordModifiers =
        '[ FieldLabelModifier '[CamelToKebab]
         , ConstructorTagModifier '[CamelToKebab]
         ]
      type SumModifiers =
        '[ FieldLabelModifier '[CamelToKebab]
         , ConstructorTagModifier '[CamelToKebab]
         , TagSingleConstructors
         , NoAllNullaryToStringTag
         , SumTaggedObject "tag" "body"
         ]
    |]

produceSum :: SumDef -> ReaderT Config FileM ()
produceSum def = do
  let cutyname :: Text = [i|#{sumName def}CU|]
  for_ (sumVariants def) $ \variant ->
    unless (Seq.null $ variantFields variant) $ do
      let fieldsname :: Text = [i|#{variantName variant}Fields|]
      p [i|deriving via (CustomJSON RecordModifiers #{fieldsname}) instance ToJSON #{fieldsname}|]
      p [i|deriving via (CustomJSON RecordModifiers #{fieldsname}) instance FromJSON #{fieldsname}|]
  p [i|deriving via (CustomJSON RecordModifiers #{sumName def}) instance ToJSON #{sumName def}|]
  p [i|deriving via (CustomJSON RecordModifiers #{sumName def}) instance FromJSON #{sumName def}|]
  p [i|deriving via (CustomJSON SumModifiers #{cutyname}) instance ToJSON #{cutyname}|]
  p [i|deriving via (CustomJSON SumModifiers #{cutyname}) instance FromJSON #{cutyname}|]
  p ""

produceProd :: ProductDef -> ReaderT Config FileM ()
produceProd def = do
  let cutyname :: Text = [i|#{productName def}CU|]
  p [i|deriving via (CustomJSON RecordModifiers #{productName def}) instance ToJSON #{productName def}|]
  p [i|deriving via (CustomJSON RecordModifiers #{productName def}) instance FromJSON #{productName def}|]
  p [i|deriving via (CustomJSON RecordModifiers #{cutyname}) instance ToJSON #{cutyname}|]
  p [i|deriving via (CustomJSON RecordModifiers #{cutyname}) instance FromJSON #{cutyname}|]
  p ""

produceDataDefBody :: DataDefBody -> ReaderT Config FileM ()
produceDataDefBody def = do
  case def of
    Sum sum -> produceSum sum
    Product prod -> produceProd prod

produceDataDefs :: Seq DataDef -> ReaderT Config ProducerM ()
produceDataDefs defs = do
  cfg <- Reader.ask
  let moduleName = cfgModuleName cfg
  case parseRelFile [i|#{moduleName}.hs|] of
    Nothing -> error [i|invalid file name: #{moduleName}|]
    Just filename -> lift $ withFile filename $ do
      runReaderT (producePreamble moduleName) cfg
      for_ defs $ \def -> do
        let tyname = defName def
        let typesModuleName = maybe
              tyname
              (\prefix -> [i|#{prefix}.#{tyname}|])
              (cfgTypesModulePrefix cfg)
        produce [i|import #{typesModuleName}\n|]
      runReaderT produceModifiers cfg
      for_ defs $ \def -> runReaderT (produceDataDefBody (defBody def)) cfg
      mark "manual section"

p :: ByteString -> ReaderT Config FileM ()
p str = do
  lift (produce str)
  lift (produce "\n")

{-# LANGUAGE OverloadedStrings #-}

-- | Description: Functions for defining Postgres/Hasql information.

module Data.Capperloin.Defs.WebTyped.Hasql
  ( table
  , table_
  , column
  , column_
  , postgresNS
  , postgresContentNS
  )
where

import Data.Capperloin.Defs ( Namespace, DefM, Identifier, TypeInfo(..), node, defcapperloin )
import GHC.Stack ( HasCallStack )

postgresNS :: Namespace
postgresNS = "postgres"

postgresContentNS :: Namespace
postgresContentNS = "postgres-content"

table :: HasCallStack => Identifier -> DefM -> DefM
table name body = defcapperloin $
  node postgresNS "table" $
    node postgresNS name $
      body

table_ :: HasCallStack => Identifier -> DefM
table_ name = defcapperloin $
  table name $ pure ()

column :: HasCallStack => Identifier -> TypeInfo -> DefM -> DefM
column colname coltype body = defcapperloin $
  node postgresNS "column" $ do
    node postgresNS "colname" $ node postgresNS colname $ pure ()
    node postgresNS "coltype" $ case coltype of
      FlatType t -> node postgresContentNS t $ pure ()
      MaybeType t -> node postgresNS "maybe" $
        node postgresContentNS t $ pure ()
    body

column_ :: HasCallStack => Identifier -> TypeInfo -> DefM
column_ colname coltype = defcapperloin $
  column colname coltype $ pure ()

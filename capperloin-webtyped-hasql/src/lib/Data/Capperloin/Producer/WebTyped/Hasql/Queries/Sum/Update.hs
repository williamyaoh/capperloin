{-# LANGUAGE OverloadedLabels    #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE QuasiQuotes         #-}

module Data.Capperloin.Producer.WebTyped.Hasql.Queries.Sum.Update
  ( produceUpdateQueries )
where

import Data.ByteString ( ByteString )
import Data.Capperloin.Producer ( FileM, produce )
import Data.Capperloin.Matcher.WebTyped.Hasql ( Field(..) )
import Data.Capperloin.Producer.WebTyped.Hasql.Queries.Sum
  ( SumDBInfo(..)
  , FieldAssignment
  , variantJSONBExpr
  )
import Data.Capperloin.Producer.WebTyped.Hasql.Queries.Util ( betwixt, pre, mbseq, fieldsToParamsSelect, fieldsToParamsUpdate, fieldsToColumnList )
import Data.Function ( (&) )
import Data.Functor ( (<&>) )
import Data.Sequence ( Seq )
import Data.String.Interpolate ( i, iii, __i )
import Data.Text ( Text )
import Optics.Core ( (%) )
import Optics.Operators ( (^.) )

produceUpdateQueries :: SumDBInfo -> FileM ()
produceUpdateQueries sum = do
  produceUpdateByIDQuery sum

produceUpdateByIDQuery :: SumDBInfo -> FileM ()
produceUpdateByIDQuery sum = do
  let cases :: Seq Text = sum ^. #variants <&> \variant ->
        -- The point of the @id = id@ assignments is so that the query is still
        -- valid even if there are no other fields to change.
        let qry :: Text =
              [iii|
                "WITH base_entity AS
                  (UPDATE #{sum ^. #base % #table}
                    SET id = id
                        #{mbseq (sum ^. #base % #forUpdate) "" (", " <> fieldAssignmentsToUpdate (sum ^. #base % #forUpdate))}
                    WHERE id = $1
                    RETURNING id #{pre ", " $ fmap fieldColumnName (sum ^. #base % #forRead)})
                 UPDATE #{variant ^. #table}
                   SET id = base_entity.id
                       #{mbseq (variant ^. #fields) "" (", " <> fieldsToParamsUpdate 1 (variant ^. #fields))}
                   FROM base_entity
                   WHERE #{variant ^. #table}.id = $1
                     AND base_entity.id = #{variant ^. #table}.id
                   RETURNING base_entity.id,
                             row('#{variant ^. #table}', #{variantJSONBExpr (variant ^. #table) variant})
                             #{pre ", " $ fmap fieldColumnName (sum ^. #base % #forRead)};"
              |]
        in [iii|
             #{variant ^. #name} #{mbseq (variant ^. #fields) "" "args"} ->
               statement (rowID #{mbseq (variant ^. #fields) "" ", args"}) $ Statement
                 #{qry}
                 ((#{mbseq (variant ^. #fields) "" "fst >$<"} Encode.param (Encode.nonNullable Encode.uuid))
                   #{mbseq (variant ^. #fields) "" (" <> (snd >$< " <> variant ^. #encoder <> ")")})
                 (Decode.rowMaybe #{sum ^. #decoder})
                 True
           |]

  -- The query above is the "happy path" where the case of the update is the same
  -- as the case currently in the DB. If that's not the case and we get back no
  -- rows, we go down a slower path where we fetch out the current case and use
  -- that info to delete from the current subtable, then insert into the new
  -- subtable.

  let deleteExisting :: Text =
        [iii|
          (mconcat
            [ "DELETE FROM "
            , currtable
            , " WHERE ID = $1;"
            ])
        |]
  let retargetCases :: Seq Text = sum ^. #variants <&> \variant ->
        let qry :: Text =
              [iii|
                (mconcat
                  [ "WITH
                       base_entity AS
                         (UPDATE #{sum ^. #base % #table}
                           SET id = id,
                               \\"#{variant ^. #tagcol}\\" = '()', \\""
                  , currtagcol
                  , "\\" = NULL
                      #{mbseq (sum ^. #base % #forUpdate) "" (", " <> fieldAssignmentsToUpdate (sum ^. #base % #forUpdate))}
                      WHERE id = $1
                      RETURNING id #{pre ", " $ fmap fieldColumnName (sum ^. #base % #forRead)}),
                    sub_entity AS
                      (INSERT INTO #{variant ^. #table}
                       (id, __tag__ #{mbseq (variant ^. #fields) "" (", " <> fieldsToColumnList (variant ^. #fields))})
                       SELECT base_entity.id,
                              '()' AS __tag_
                              #{mbseq (variant ^. #fields) "" (", " <> fieldsToParamsSelect 1 (variant ^. #fields))}
                         FROM base_entity
                       RETURNING #{variant ^. #table}.*)
                    SELECT base_entity.id,
                           row('#{variant ^. #table}', #{variantJSONBExpr "sub_entity" variant})
                           #{pre ", " $ fmap fieldColumnName (sum ^. #base % #forRead)}
                      FROM base_entity
                      JOIN sub_entity ON base_entity.id = sub_entity.id;"
                  ])
              |]
        in [iii|
             #{variant ^. #name} #{mbseq (variant ^. #fields) "" "args"} ->
               statement (rowID #{mbseq (variant ^. #fields) "" ", args"}) $ Statement
                 #{qry}
                 ((#{mbseq (variant ^. #fields) "" "fst >$<"} Encode.param (Encode.nonNullable Encode.uuid))
                   #{mbseq (variant ^. #fields) "" (" <> (snd >$< " <> variant ^. #encoder <> ")")})
                 (Decode.singleRow #{sum ^. #decoder})
                 True
           |]

  p [__i|
      update#{sum ^. #name}ByID :: UUID -> #{sum ^. #cu} -> Transaction #{sum ^. #name}
      update#{sum ^. #name}ByID rowID create = do
        mentity <- case create of
          { #{betwixt "; " cases } }
        case mentity of
          Just entity -> pure $! entity
          Nothing -> do
            (currtable, currtagcol) <- statement rowID $ Statement
              "SELECT #{tagCase sum} AS currbranch FROM #{sum ^. #base % #table} WHERE id = $1;"
              (Encode.param $ Encode.nonNullable Encode.uuid)
              (Decode.singleRow $ Decode.column $ Decode.nonNullable $ Decode.composite $ (,)
                <$> Decode.field (Decode.nonNullable Decode.bytea)
                <*> Decode.field (Decode.nonNullable Decode.bytea))
              True
            statement rowID $ Statement
              #{deleteExisting}
              (Encode.param $ Encode.nonNullable Encode.uuid)
              Decode.noResult
              True
            case create of
              { #{betwixt "; " retargetCases} }
    |]
  p ""

-- | A giant @CASE ... END@ to fetch info about the current branch table/tag.
tagCase :: SumDBInfo -> Text
tagCase sum =
  let branches = betwixt " " $ sum ^. #variants <&> \variant ->
        [iii|
          WHEN #{sum ^. #base % #table}.\\"#{variant ^. #tagcol}\\" IS NOT NULL
            THEN row('#{variant ^. #table}' :: BYTEA, '#{variant ^. #tagcol}' :: BYTEA)
        |]
  in [i|CASE #{branches} END|]

fieldAssignmentsToUpdate :: Seq FieldAssignment -> Text
fieldAssignmentsToUpdate fields = fields
  & fmap (\fld -> [i|#{fld ^. #column} = #{fld ^. #value}|])
  & betwixt ", "

p :: ByteString -> FileM ()
p str = produce str *> produce "\n"

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

module Data.Capperloin.Producer.WebTyped.Hasql.Queries.Util
  ( fieldsToParamsSelect
  , fieldsToParamsUpdate
  , fieldsToColumnList
  , outputType
  , betwixt
  , pre
  , mbseq
  )
where

import Data.Capperloin.Defs ( TypeInfo(..) )
import Data.Capperloin.Matcher.WebTyped.Hasql ( Field(..) )
import Data.Foldable ( foldl' )
import Data.Function ( (&) )
import Data.Sequence ( Seq((:<|)) )
import Data.String.Interpolate ( i )
import Data.Text ( Text )
import Optics.Indexed.Core ( reindexed, iover, imapped )

import qualified Data.Sequence as Seq

-- | Convert a list of fields to a string like
-- |
-- | > $1 AS \"field1\", $2 AS \"field2\", $3 AS \"field3\" ...
-- |
-- | The default parameter offset is 0, producing the list as above.
fieldsToParamsSelect :: Int -> Seq Field -> Text
fieldsToParamsSelect offset fields = fields
  & iover
      (reindexed (\n -> n + offset + 1) imapped)
      (\ix fld -> [i|$#{ix} AS \\"#{fieldColumnName fld}\\"|])
  & betwixt ", "

-- | Convert a list of fields to a string like
-- |
-- | > \"field1\" = $1, \"field2\" = $2, \"field3\" = $3 ...
-- |
-- | The default parameter offset is 0, producing the list as above.
fieldsToParamsUpdate :: Int -> Seq Field -> Text
fieldsToParamsUpdate offset fields = fields
  & iover
      (reindexed (\n -> n + offset + 1) imapped)
      (\ix fld -> [i|\\"#{fieldColumnName fld}\\" = $#{ix}|])
  & betwixt ", "

-- | Convert a list of fields to a string like
-- |
-- | > \"field1\", \"field2\", \"field3\" ...
fieldsToColumnList :: Seq Field -> Text
fieldsToColumnList fields = fields
  & fmap (\fld -> [i|\\"#{fieldColumnName fld}\\"|])
  & betwixt ", "

outputType :: TypeInfo -> Text
outputType (FlatType t) = t
outputType (MaybeType t) = [i|(Maybe #{t})|]

-- | Add some text between each element.
betwixt :: Text -> Seq Text -> Text
betwixt sep elems = case elems of
  Seq.Empty -> ""
  x :<| xs -> foldl' (\l r -> l <> sep <> r) x xs

-- | Add some text before each element.
pre :: Text -> Seq Text -> Text
pre sep = foldMap (sep <>)

-- | Insert one bit of text if the sequence is empty, and a different one if not.
mbseq :: Seq a -> Text -> Text -> Text
mbseq s no yes = case s of
  Seq.Empty -> no
  _         -> yes

{-# LANGUAGE OverloadedLabels    #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE QuasiQuotes         #-}

module Data.Capperloin.Producer.WebTyped.Hasql.Queries.Sum.Create
  ( produceCreateQueries )
where

import Data.ByteString ( ByteString )
import Data.Capperloin.Producer ( FileM, produce )
import Data.Capperloin.Matcher.WebTyped.Hasql ( Field(..) )
import Data.Capperloin.Producer.WebTyped.Hasql.Queries.Sum
  ( SumDBInfo(..)
  , variantJSONBExpr
  )
import Data.Capperloin.Producer.WebTyped.Hasql.Queries.Util ( betwixt, pre, mbseq, fieldsToParamsSelect, fieldsToColumnList )
import Data.Functor ( (<&>) )
import Data.Sequence ( Seq )
import Data.String.Interpolate ( iii, __i )
import Data.Text ( Text )
import Optics.Core ( (%) )
import Optics.Operators ( (^.) )

-- In order to write these queries, we use a three-part CTE:
--
-- 1. Insert a row with the right tag into the base table
-- 2. Insert a row with the variant fields into the sub table, using the ID from the base
-- 3. Join the results of the two inserts together and return them
--
-- Because Postgres treats an entire CTE as all being part of one transaction,
-- even if no transaction block has been opened, this doesn't violate the foreign
-- key constraints on the sum tables.
--
-- You might rightly wonder why we need three steps instead of two; why can't
-- we just use a RETURNING clause on the second insert to get the columns we
-- need? Unfortunately, this doesn't work; as per the PG documentation on INSERT:
-- <https://www.postgresql.org/docs/current/sql-insert.html>
-- the RETURNING clause on an INSERT can __only__ return columns of the table
-- being inserted into. So there's no way to return the values from the CTE.

produceCreateQueries :: SumDBInfo -> FileM ()
produceCreateQueries sum = do
  produceCreateAutoIDQuery sum
  produceCreateWithIDQuery sum

produceCreateAutoIDQuery :: SumDBInfo -> FileM ()
produceCreateAutoIDQuery sum = do
  let cases :: Seq Text = sum ^. #variants <&> \variant ->
        let qry :: Text =
              [iii|
                "WITH
                   base_entity AS
                     (INSERT INTO #{sum ^. #base % #table}
                       ( id, \\"#{variant ^. #tagcol}\\"
                       #{pre ", " $ fmap (^. #column) (sum ^. #base % #forCreate)}
                       )
                       VALUES
                       ( DEFAULT, '()'
                       #{pre ", " $ fmap (^. #value) (sum ^. #base % #forCreate)}
                       )
                       RETURNING id #{pre ", " $ fmap fieldColumnName (sum ^. #base % #forRead)}),
                   sub_entity AS
                     (INSERT INTO #{variant ^. #table}
                      (id, __tag__ #{mbseq (variant ^. #fields) "" (", " <> fieldsToColumnList (variant ^. #fields))})
                      SELECT base_entity.id,
                             '()' AS __tag__
                             #{mbseq (variant ^. #fields) "" (", " <> fieldsToParamsSelect 0 (variant ^. #fields))}
                      FROM base_entity
                      RETURNING #{variant ^. #table}.*)
                 SELECT base_entity.id,
                        row('#{variant ^. #table}', #{variantJSONBExpr "sub_entity" variant})
                        #{pre ", " $ fmap fieldColumnName (sum ^. #base % #forRead)}
                   FROM base_entity
                   JOIN sub_entity ON sub_entity.id = base_entity.id;"
              |]
        in [iii|
             #{variant ^. #name} #{mbseq (variant ^. #fields) "" "args"} ->
               statement #{mbseq (variant ^. #fields) "()" "args"} $ Statement
                 #{qry}
                 #{mbseq (variant ^. #fields) "Encode.noParams" (variant ^. #encoder)}
                 (Decode.singleRow #{sum ^. #decoder})
                 True
           |]
  p [__i|
      create#{sum ^. #name} :: #{sum ^. #cu} -> Transaction #{sum ^. #name}
      create#{sum ^. #name} create = case create of
        { #{betwixt "; " cases} }
    |]
  p ""

produceCreateWithIDQuery :: SumDBInfo -> FileM ()
produceCreateWithIDQuery sum = do
  let cases :: Seq Text = sum ^. #variants <&> \variant ->
        let qry :: Text =
              [iii|
                "WITH
                   base_entity AS
                     (INSERT INTO #{sum ^. #base % #table}
                       ( id, \\"#{variant ^. #tagcol}\\"
                       #{pre ", " $ fmap (^. #column) (sum ^. #base % #forCreate)}
                       )
                       VALUES
                       ( $1, '()'
                       #{pre ", " $ fmap (^. #value) (sum ^. #base % #forCreate)}
                       )
                       RETURNING id #{pre ", " $ fmap fieldColumnName (sum ^. #base % #forRead)}),
                   sub_entity AS
                     (INSERT INTO #{variant ^. #table}
                      (id, __tag__ #{mbseq (variant ^. #fields) "" (", " <> fieldsToColumnList (variant ^. #fields))})
                      SELECT base_entity.id,
                             '()' AS __tag__
                             #{mbseq (variant ^. #fields) "" (", " <> fieldsToParamsSelect 1 (variant ^. #fields))}
                      FROM base_entity
                      RETURNING #{variant ^. #table}.*)
                 SELECT base_entity.id,
                        row('#{variant ^. #table}', #{variantJSONBExpr "sub_entity" variant})
                        #{pre ", " $ fmap fieldColumnName (sum ^. #base % #forRead)}
                   FROM base_entity
                   JOIN sub_entity ON sub_entity.id = base_entity.id;"
              |]
        in [iii|
             #{variant ^. #name} #{mbseq (variant ^. #fields) "" "args"} ->
               statement (rowID #{mbseq (variant ^. #fields) "" ", args"}) $ Statement
                 #{qry}
                 ((#{mbseq (variant ^. #fields) "" "fst >$<"} Encode.param (Encode.nonNullable Encode.uuid))
                   #{mbseq (variant ^. #fields) "" (" <> (snd >$< " <> variant ^. #encoder <> ")")})
                 (Decode.singleRow #{sum ^. #decoder})
                 True
           |]
  p [__i|
      create#{sum ^. #name}WithID :: UUID -> #{sum ^. #cu} -> Transaction #{sum ^. #name}
      create#{sum ^. #name}WithID rowID create = case create of
        { #{betwixt "; " cases } }
    |]
  p ""

p :: ByteString -> FileM ()
p str = produce str *> produce "\n"

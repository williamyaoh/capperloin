{-# LANGUAGE OverloadedLabels    #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE QuasiQuotes         #-}

module Data.Capperloin.Producer.WebTyped.Hasql.Queries.Sum.Delete
  ( produceDeleteQueries )
where

import Data.ByteString ( ByteString )
import Data.Capperloin.Producer ( FileM, produce )
import Data.Capperloin.Producer.WebTyped.Hasql.Queries.Sum ( SumDBInfo(..) )
import Data.Capperloin.Producer.WebTyped.Hasql.Queries.Util ( betwixt )
import Data.Functor ( (<&>) )
import Data.Sequence ( Seq((:<|)) )
import Data.String.Interpolate ( i, iii, __i )
import Data.Text ( Text )
import Optics.Core ( (%) )
import Optics.Operators ( (^.) )

produceDeleteQueries :: SumDBInfo -> FileM ()
produceDeleteQueries sum = do
  produceDeleteManyQuery sum
  produceDeleteByIDQuery sum

produceDeleteManyQuery :: SumDBInfo -> FileM ()
produceDeleteManyQuery sum = do
  -- DELETE CASCADE handles deleting from the subtables
  p [__i|
      delete#{sum ^. #name} :: RowsOf #{sum ^. #name} -> Transaction ()
      delete#{sum ^. #name} rows =
        let renderedRows = renderSQL rows
        in statement () $ Statement
             (mconcat
               [ "DELETE FROM #{sum ^. #base % #table} #{variantJoins sum} "
               , renderedRows
               , if ByteString.null renderedRows then " WHERE " else " AND "
               , "#{sum ^. #base % #table}.id = __base.id;"
               ])
             Encode.noParams
             Decode.noResult
             False
    |]
  p ""

produceDeleteByIDQuery :: SumDBInfo -> FileM ()
produceDeleteByIDQuery sum = do
  -- DELETE CASCADE handles deleting from the subtables
  p [__i|
      delete#{sum ^. #name}ByID :: UUID -> Transaction ()
      delete#{sum ^. #name}ByID rowID =
        statement rowID $ Statement
          "DELETE FROM #{sum ^. #base % #table} WHERE id = $1;"
          (Encode.param $ Encode.nonNullable Encode.uuid)
          Decode.noResult
          True
    |]
  p ""

-- | Joins in all the variant subtables. Unfortunately, we also have to use
-- | the base table itself in the USING clause, because otherwise its columns
-- | aren't in scope when joining in the variant tables.
-- | Whoever designed the SQL standard was a goddamn psychopath for making that
-- | the case.
variantJoins :: SumDBInfo -> Text
variantJoins sum = betwixt " " $
  [i|USING #{sum ^. #base % #table} AS __base|]
    :<| (<&>) (sum ^. #variants) (\variant ->
          [iii|
            LEFT JOIN #{variant ^. #table}
              ON __base.id = #{variant ^. #table}.id
              AND __base.\\"#{variant ^. #tagcol}\\" = #{variant ^. #table}.__tag__
          |])

p :: ByteString -> FileM ()
p str = produce str *> produce "\n"

{-# LANGUAGE OverloadedLabels    #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE QuasiQuotes         #-}

module Data.Capperloin.Producer.WebTyped.Hasql.Queries.Sum.Read
  ( produceReadQueries )
where

import Data.ByteString ( ByteString )
import Data.Capperloin.Producer ( FileM, produce )
import Data.Capperloin.Matcher.WebTyped.Hasql ( Field(..) )
import Data.Capperloin.Producer.WebTyped.Hasql.Queries.Sum
  ( SumDBInfo(..)
  , variantJSONBExpr
  )
import Data.Capperloin.Producer.WebTyped.Hasql.Queries.Util ( betwixt, pre )
import Data.Functor ( (<&>) )
import Data.String.Interpolate ( i, iii, __i )
import Data.Text ( Text )
import Optics.Core ( (%) )
import Optics.Operators ( (^.) )

produceReadQueries :: SumDBInfo -> FileM ()
produceReadQueries sum = do
  produceReadVectorQuery sum
  produceReadByIDQuery sum

produceReadVectorQuery :: SumDBInfo -> FileM ()
produceReadVectorQuery sum = do
  let qry :: Text =
        [iii|
          "SELECT #{sum ^. #base % #table}.id,
                  #{bodyCase sum} AS \\"payload\\"
                  #{pre ", " $ fmap fieldColumnName (sum ^. #base % #forRead)}
             FROM #{sum ^. #base % #table}
             #{variantJoins sum} "
        |]
  p [__i|
      read#{sum ^. #name} :: Query #{sum ^. #name} -> Transaction (Vector #{sum ^. #name})
      read#{sum ^. #name} qry =
        statement () $ Statement
          (#{qry} <> renderSQL qry)
          Encode.noParams
          (Decode.rowVector #{sum ^. #decoder})
          False
    |]
  p ""

produceReadByIDQuery :: SumDBInfo -> FileM ()
produceReadByIDQuery sum = do
  let qry :: Text =
        [iii|
          "SELECT #{sum ^. #base % #table}.id,
                  #{bodyCase sum} AS \\"payload\\"
                  #{pre ", " $ fmap fieldColumnName (sum ^. #base % #forRead)}
             FROM #{sum ^. #base % #table}
             #{variantJoins sum}
             WHERE #{sum ^. #base % #table}.id = $1;"
        |]
  p [__i|
      read#{sum ^. #name}ByID :: UUID -> Transaction #{sum ^. #name}
      read#{sum ^. #name}ByID rowID =
        statement rowID $ Statement
          #{qry}
          (Encode.param $ Encode.nonNullable Encode.uuid)
          (Decode.singleRow #{sum ^. #decoder})
          True
    |]
  p ""

-- | A giant @CASE ... END@ to select the sum body based on the tag.
bodyCase :: SumDBInfo -> Text
bodyCase sum =
  let branches = betwixt " " $ sum ^. #variants <&> \variant ->
        [iii|
          WHEN #{sum ^. #base % #table}.\\"#{variant ^. #tagcol}\\" IS NOT NULL
            THEN row('#{variant ^. #table}', #{variantJSONBExpr (variant ^. #table) variant})
        |]
  in [i|CASE #{branches} END|]

-- | Joins in all the variant subtables.
variantJoins :: SumDBInfo -> Text
variantJoins sum =
  betwixt " " $ sum ^. #variants <&> \variant ->
    [iii|
      LEFT JOIN #{variant ^. #table}
        ON #{sum ^. #base % #table}.id = #{variant ^. #table}.id
        AND #{sum ^. #base % #table}.\\"#{variant ^. #tagcol}\\"
          = #{variant ^. #table}.__tag__
    |]

p :: ByteString -> FileM ()
p str = produce str *> produce "\n"

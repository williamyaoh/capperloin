{-# LANGUAGE OverloadedLabels  #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

-- | Generate the relevant instances and functions for @quibble-core@ usage.

module Data.Capperloin.Producer.WebTyped.Hasql.Queries.Sum.Quibble
  ( produceQuibbleCode )
where

import Control.Monad ( unless )
import Data.ByteString ( ByteString )
import Data.Capperloin.Producer ( FileM, produce )
import Data.Capperloin.Matcher.WebTyped.Hasql ( Field(..) )
import Data.Capperloin.Producer.WebTyped.Hasql.Queries.Sum ( SumDBInfo(..) )
import Data.Capperloin.Producer.WebTyped.Hasql.Queries.Util ( outputType, mbseq )
import Data.Foldable ( for_ )
import Data.String.Interpolate ( __i )
import Optics.Core ( (%) )
import Optics.Operators ( (^.) )

import qualified Data.Sequence as Seq

produceQuibbleCode :: SumDBInfo -> FileM ()
produceQuibbleCode sum = do
  produceBaseCode sum
  produceVariantCode sum

-- | Produce the instances for querying on the base fields.
produceBaseCode :: SumDBInfo -> FileM ()
produceBaseCode sum = do
  p [__i|
      instance HasTable #{sum ^. #name} "#{sum ^. #base % #table}"
      instance HasColumn #{sum ^. #name} "id" UUID
    |]
  for_ (sum ^. #base % #forRead) $ \field ->
    p [__i|
        instance HasColumn #{sum ^. #name} "#{fieldColumnName field}" #{outputType $ fieldType field}
      |]
  p ""

-- | Produce the instances for the fields of each variant, as well as functions
-- | to allow querying on the variant tables.
produceVariantCode :: SumDBInfo -> FileM ()
produceVariantCode sum =
  for_ (sum ^. #variants) $ \variant -> do
    p [__i|
        _#{variant ^. #name} :: Expr #{mbseq (variant ^. #fields) (sum ^. #name) (variant ^. #body)} Bool -> Expr #{sum ^. #name} Bool
        _#{variant ^. #name} =
          sumBranch "#{sum ^. #base % #table}" "\\"#{variant ^. #tagcol}\\""
      |]
    p ""
    unless (Seq.null $ variant ^. #fields) $ do
      p [__i|
          instance HasTable #{variant ^. #body} "#{variant ^. #table}"
        |]
      for_ (variant ^. #fields) $ \field ->
        p [__i|
            instance HasColumn #{variant ^. #body} "#{fieldColumnName field}" #{outputType $ fieldType field}
          |]
      p ""

p :: ByteString -> FileM ()
p str = produce str *> produce "\n"

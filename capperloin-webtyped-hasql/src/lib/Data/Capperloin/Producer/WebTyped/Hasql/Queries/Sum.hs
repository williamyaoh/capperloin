{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedLabels      #-}
{-# LANGUAGE OverloadedLists       #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE QuasiQuotes           #-}
{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE UndecidableInstances  #-}


module Data.Capperloin.Producer.WebTyped.Hasql.Queries.Sum
  ( SumDBInfo(..)
  , SumBaseInfo(..)
  , SumVariantInfo(..)
  , FieldAssignment(..)
  , sumToInfo
  , variantJSONBExpr
  )
where

import Data.Capperloin.Defs ( SrcLoc )
import Data.Capperloin.Matcher.WebTyped.Hasql ( Field(..), SumVariant(..), SumDef(..) )
import Data.Sequence ( Seq )
import Data.String.Interpolate ( i )
import Data.Text ( Text )
import Optics.Operators ( (^.) )
import Optics.TH ( makeFieldLabelsNoPrefix )

import qualified Data.Text as Text

-- | As an example, say we had some sum type @Foo@, which is specified to
-- | include timestamps. Most of the fields in this constituent data get set
-- | in the obvious way. The interesting part is in the sequence of fields inside
-- | `SumBaseInfo'. @forRead@ would be set to the @created_at@ and @updated_at@
-- | fields, since those need to be read out in addition to the id and sum body.
-- | @forCreate@ would be set to @created_at@ and @updated_at@ as well, but
-- | would include the extra information about what values to use. @forUpdate@
-- | would be set to just @updated_at@, since we obviously don't want to touch
-- | the @created_at@ timestamp. All this gives the query producers enough info
-- | to produce the right queries.
data SumDBInfo = SumDBInfo
  { name :: Text
  , cu :: Text
  , id :: Field
  , base :: SumBaseInfo
  , variants :: Seq SumVariantInfo
  , decoder :: Text
  }

data SumBaseInfo = SumBaseInfo
  { table :: Text
  , -- | Sequence of fields that should be read out of the base table. Note that
    -- | this should not include the @id@ field. The full field is used so that
    -- | we have enough information to write decoders.
    forRead :: Seq Field
  , -- | Sequence of fields to set in the base table on an @INSERT@, including
    -- | their SQL values. Should not include the @id@ field.
    forCreate :: Seq FieldAssignment
  , -- | Sequence of fields to set in the base table on an @UPDATE@, including
    -- | their new SQL values. Should not include the @id@ field.
    forUpdate :: Seq FieldAssignment
  }

data SumVariantInfo = SumVariantInfo
  { name :: Text
  , table :: Text
  , -- | The name of the column in the base table which holds the tag for this
    -- | this table. Not quoted, so be careful with it.
    tagcol :: Text
  , fields :: Seq Field
  , -- | The name of the @-Fields@ datatype for this variant.
    body :: Text
  , -- | The name of the encoder for this variant's @-Fields@ datatype.
    encoder :: Text
  }

data FieldAssignment = FieldAssignment
  { column :: Text
  , value :: Text
  }

makeFieldLabelsNoPrefix ''SumDBInfo
makeFieldLabelsNoPrefix ''SumBaseInfo
makeFieldLabelsNoPrefix ''SumVariantInfo
makeFieldLabelsNoPrefix ''FieldAssignment

sumToInfo :: SumDef -> SumDBInfo
sumToInfo sum = SumDBInfo
  { name = sumName sum
  , cu = [i|#{sumName sum}CU|]
  , id = idField $ sumLoc sum
  , base = SumBaseInfo
    { table = sumTableName sum
    , forRead = if sumIncludeTimestamps sum
        then [createdAtField (sumLoc sum), updatedAtField (sumLoc sum)]
        else []
    , forCreate = if sumIncludeTimestamps sum
        then [FieldAssignment "created_at" "DEFAULT", FieldAssignment "updated_at" "DEFAULT"]
        else []
    , forUpdate = if sumIncludeTimestamps sum
        then [FieldAssignment "updated_at" "DEFAULT"]
        else []
    }
  , variants = fmap variantToInfo $ sumVariants sum
  , decoder = [i|decode#{sumName sum}|]
  }

variantToInfo :: SumVariant -> SumVariantInfo
variantToInfo variant = SumVariantInfo
  { name = variantName variant
  , table = variantTableName variant
  , tagcol = Text.toLower $ variantName variant
  , fields = variantFields variant
  , body = [i|#{variantName variant}Fields|]
  , encoder = [i|encode#{variantName variant}Fields|]
  }

idField :: SrcLoc -> Field
idField loc = Field
  { fieldName = "id"
  , fieldType = "UUID"
  , fieldColumnName = "id"
  , fieldColumnType = "UUID"
  , fieldLoc = loc
  }

createdAtField :: SrcLoc -> Field
createdAtField loc = Field
  { fieldName = "createdAt"
  , fieldType = "UTCTime"
  , fieldColumnName = "created_at"
  , fieldColumnType = "TIMESTAMPTZ"
  , fieldLoc = loc
  }

updatedAtField :: SrcLoc -> Field
updatedAtField loc = Field
  { fieldName = "updatedAt"
  , fieldType = "UTCTime"
  , fieldColumnName = "updated_at"
  , fieldColumnType = "TIMESTAMPTZ"
  , fieldLoc = loc
  }

-- | A SQL expression to get all the fields of the variant as a JSON blob.
-- | We have to take in the name of the table to pull fields from, since
-- | @INSERT@ requires a slightly different return.
variantJSONBExpr :: Text -> SumVariantInfo -> Text
variantJSONBExpr tbl variant =
  [i|to_jsonb(row(#{tbl}.*) :: #{variant ^. #table})|]

{-# LANGUAGE OverloadedLists     #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE QuasiQuotes         #-}
{-# LANGUAGE ScopedTypeVariables #-}

-- |
-- Output SQL schemas for defined types.

module Data.Capperloin.Producer.WebTyped.Hasql.Schema
  ( producer )
where

import Control.Monad ( when )
import Data.ByteString ( ByteString )
import Data.Capperloin.Defs ( Identifier, TypeInfo(..) )
import Data.Capperloin.Producer ( Producer(..), ProducerM, FileM, relfile, version, withFile, produce, mark )
import Data.Capperloin.Matcher.WebTyped.Hasql
  ( Field(..)
  , SumVariant(..)
  , SumDef(..)
  , ProductDef(..)
  , DataDefBody(..)
  , DataDef(..)
  , matchDataDefs
  )
import Data.Foldable ( for_, foldl' )
import Data.Function ( (&) )
import Data.Functor ( (<&>) )
import Data.Sequence ( Seq((:<|)) )
import Data.String.Interpolate ( i, __i, iii )
import Data.Text ( Text )

import qualified Data.Sequence as Seq
import qualified Data.Text as Text

-- TODO: This module doesn't support any kind of typevars on datatypes yet.
-- In order to fix that, we'll likely need to modify Capperloin such that
-- it's even possible to distinguish between normal types and type variables.

producer :: Producer
producer =
  Producer
    { prodName = "capperloin-webtyped-hasql"
    , prodVersion = version 0 1 0 [] []
    , prodMatcher = matchDataDefs
    , prodCommentF = ("-- " <>)
    , prodBody = produceDataDefs
    }

-- |
-- Output the boring module header.
producePreamble :: FileM ()
producePreamble = do
  mark "beginning of file"
  p [__i|
      CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

      CREATE TYPE unit AS enum('()');
    |]

produceSum :: SumDef -> FileM ()
produceSum def = do
  -- Generate the base table, with a tag for each variant
  p [i|CREATE TABLE #{sumTableName def}|]
  p [i|  ( id UUID NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4()|]
  for_ (sumVariants def) $ \variant ->
    p [i|  , "#{tagCol variant}" unit NULL|]
  when (sumIncludeTimestamps def) $ do
    p [i|  , created_at TIMESTAMPTZ NOT NULL DEFAULT clock_timestamp()|]
    p [i|  , updated_at TIMESTAMPTZ NOT NULL DEFAULT clock_timestamp()|]
  p [i|  );|]

  -- Generate the individual variant tables
  for_ (sumVariants def) $ \variant -> do
    p [i|CREATE TABLE #{variantTableName variant}|]
    p [i|  ( id UUID NOT NULL|]
    p [i|  , __tag__ unit NOT NULL|]
    for_ (variantFields variant) $ \field -> do
      p [i|  , "#{fieldColumnName field}" #{outputColumn $ fieldColumnType field}|]
    p [i|  , PRIMARY KEY ( id, __tag__ )|]
    p [i|  );|]

  -- Create deferred FK constraints from the main table to the variant tables
  for_ (sumVariants def) $ \variant -> do
    p [i|ALTER TABLE #{sumTableName def}|]
    p [i|  ADD CONSTRAINT "#{sumTableName def}__#{tagCol variant}"|]
    p [i|      FOREIGN KEY (id, "#{tagCol variant}")|]
    p [i|      REFERENCES #{variantTableName variant} (id, __tag__)|]
    p [i|      DEFERRABLE INITIALLY DEFERRED;|]

  -- Create unique constraints on each (id, tag) pair to ensure fast lookups
  for_ (sumVariants def) $ \variant -> do
    p [i|ALTER TABLE #{sumTableName def}|]
    p [i|  ADD CONSTRAINT "#{sumTableName def}__unique_#{tagCol variant}"|]
    p [i|  UNIQUE (id, "#{tagCol variant}");|]

  -- Create a CHECK constraint to ensure that only one tag is set at a time on the main table
  p [i|ALTER TABLE #{sumTableName def}|]
  p [i|  ADD CONSTRAINT "#{sumTableName def}__exactly_one_tag"|]
  p [i|  CHECK #{tagChecks};|]

  -- Create non-deferred FK constraints from the variant tables to the main table
  for_ (sumVariants def) $ \variant -> do
    p [i|ALTER TABLE #{variantTableName variant}|]
    p [i|  ADD CONSTRAINT "#{variantTableName variant}__#{sumTableName def}"|]
    p [i|  FOREIGN KEY (id, __tag__)|]
    p [i|  REFERENCES #{sumTableName def} (id, "#{tagCol variant}")|]
    p [i|  ON DELETE CASCADE;|]
  where
    tagCol :: SumVariant -> Identifier
    tagCol = Text.toLower . variantName

    tagCheck :: (Identifier, Seq Identifier) -> Text
    tagCheck (var, vars) = vars
      <&> (\var' -> if var == var' then [i|"#{var'}" IS NOT NULL|] else [i|"#{var'}" IS NULL|])
       &  intercalate " AND "
       &  (\t -> [i|(#{t})|])

    tagChecks :: Text
    tagChecks =
      fmap (\variant -> (tagCol variant, fmap tagCol (sumVariants def))) (sumVariants def)
        <&> tagCheck
         &  intercalate " OR "
         &  (\t -> [i|(#{t})|])

produceProd :: ProductDef -> FileM ()
produceProd def = p
  [iii|
    CREATE TABLE #{productTableName def}
      ( id UUID NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(), #{sqlFields} );
  |]
  where
    sqlFields :: Text
    sqlFields =
      let timestampFields = if productIncludeTimestamps def
            then [ "created_at TIMESTAMPTZ NOT NULL DEFAULT clock_timestamp()"
                 , "updated_at TIMESTAMPTZ NOT NULL DEFAULT clock_timestamp()"
                 ]
            else []
      in intercalate ", " $ (<> timestampFields) $ flip fmap (productFields def) $
           \field -> [i|"#{fieldColumnName field}" #{outputColumn $ fieldColumnType field}|]

produceDataDefBody :: DataDefBody -> FileM ()
produceDataDefBody def = do
  case def of
    Sum sum -> produceSum sum
    Product prod -> produceProd prod

produceDataDefs :: Seq DataDef -> ProducerM ()
produceDataDefs defs = withFile [relfile|schema.sql|] $ do
  producePreamble
  for_ defs $ \(DataDef _ body _) -> produceDataDefBody body
  mark "end of file"

outputColumn :: TypeInfo -> Text
outputColumn (FlatType t) = [i|#{t} NOT NULL|]
outputColumn (MaybeType t) = [i|#{t} NULL|]

p :: ByteString -> FileM ()
p str = produce str *> produce "\n"

intercalate :: Text -> Seq Text -> Text
intercalate sep elems = case elems of
  Seq.Empty -> ""
  x :<| xs -> foldl' (\l r -> l <> sep <> r) x xs
